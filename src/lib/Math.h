// Copyright (c) 2023 Vladimir Nejedly, vladimir.nejedly@gmail.com
// Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license
// https://creativecommons.org/licenses/by-nc-sa/4.0/
// This software is provided "as is", without any warranty or condition.

#pragma once

// Phi constant
constexpr float Pi = 3.14159265358979323846f;

// Square of a number
template <typename T> T Square(T number) { return number * number; }