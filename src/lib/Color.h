// Copyright (c) 2023 Vladimir Nejedly, vladimir.nejedly@gmail.com
// Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license
// https://creativecommons.org/licenses/by-nc-sa/4.0/
// This software is provided "as is", without any warranty or condition.

#pragma once

#include "lvgl.h"

//! 16-bit color with extra alpha byte, to be used with type LV_IMG_CF_TRUE_COLOR_ALPHA
struct ColorAlpha
{
  //! Color
  lv_color_t color;
  //! Alpha
  unsigned char alpha;
};