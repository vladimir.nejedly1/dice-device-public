// Copyright (c) 2023 Vladimir Nejedly, vladimir.nejedly@gmail.com
// Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license
// https://creativecommons.org/licenses/by-nc-sa/4.0/
// This software is provided "as is", without any warranty or condition.

#pragma once

#include <cmath>
#include "Vector2.h"
#include "Math.h"

//! Class that helps to set ideal layout of circles on screen
//! The circles are packed the way so that distance to neighbour is the same as distance to screen border
class CirclePacking
{
  //! Number of circles
  int _nCircles;
  //! Diameter of the screen in pixels, can be smaller than real screen to have a padding at the border
  int _screenWidth;
  //! Calculated radius of best-fit inner circle on which are circles placed
  float _radius;
public:
  //! Calculate optimal circle size, touching border and neighbour
  static int OptimalCircleSize(int nCircles, int screenWidth = 480)
  {
    // Special case, return whole screen area
    if (nCircles == 1)
      return screenWidth;
      
    // Optimal size touching neighbour and border
    const float sinAlphaHalf = sin(Pi / nCircles);
    return (int)(2.0f * sinAlphaHalf / (sinAlphaHalf + 1.0f) * screenWidth / 2);
  }
  //! Constructor
  CirclePacking(int nCircles, int circleSize, int screenWidth = 480) :
    _nCircles(nCircles),
    _screenWidth(screenWidth)
  {
    float dNorm = (float)circleSize / (_screenWidth / 2); // Circle diameter normalized (from interval <0, 1>)
    _radius = (1.0f + dNorm * 0.5f) / (1.0f + sin(Pi / _nCircles) * 2.0f);
  }
  //! Return screen position of circle of given index
  Vector2I GetPosition(int circleIndex)
  {
    // Special case, we want the circle to be in the center
    if (_nCircles == 1)
      return Vector2I(0, 0);

    // If there are 2 circles, we want to be located horizontally. On the contrary, with more circles we want them to start at the top
    float angleOffset;
    if (_nCircles == 2)
    {
      angleOffset = -Pi;
    }
    else
    {
      angleOffset = -Pi * 0.5f;
    }

    // Calculate circle position
    float angle = (float)circleIndex / _nCircles * 2.0f * Pi + angleOffset;
    return Vector2I(
      (int)(cos(angle) * _radius * _screenWidth / 2),
      (int)(sin(angle) * _radius * _screenWidth / 2)
    );
  }
};
