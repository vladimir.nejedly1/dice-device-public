// Copyright (c) 2023 Vladimir Nejedly, vladimir.nejedly@gmail.com
// Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license
// https://creativecommons.org/licenses/by-nc-sa/4.0/
// This software is provided "as is", without any warranty or condition.

#pragma once

#include <cmath>
#include "Math.h"

//! 2D vector
template <typename T>
struct Vector2
{
  //! Data
  T x, y;

  //! Constructor
  Vector2() {}
  //! Constructor - initialization
  constexpr Vector2(T x, T y) : x(x), y(y) {}
  //! Method compares vector elements with specified tolerance
  bool Equals(const Vector2& other) const;
  //! Dot product
  constexpr T Dot(const Vector2& v) const {return x * v.x + y * v.y;}
   //! Cross product
  constexpr T Cross(const Vector2& v) const {return x * v.y - y * v.x;}
  //! Square size of vector
  constexpr T Size2() const {return x * x + y * y;}
  //! Size of vector
  T Size() const {return (T)sqrt(Size2());}
  //! Return normalized vector
  Vector2 Normalized() const {T invSize = 1 / Size(); return *this * invSize;}
  //! Normalize vector
  void Normalize() {T invSize = 1 / Size(); *this *= invSize;}
  //! Squared distance of two vectors
  T Distance2(const Vector2<T>& v) const;
  //! Distance of two vectors
  T Distance(const Vector2<T>& v) const {return sqrt(Distance2(v));}
  //! Get angle between 2 vectors, result is in interval <-Pi, Pi>
  float AngleTo(const Vector2<T>& v) const;

  //!{ Operators
  constexpr Vector2 operator+(const Vector2& v) const {return Vector2(x + v.x, y + v.y);}
  constexpr Vector2 operator-(const Vector2& v) const {return Vector2(x - v.x, y - v.y);}
  constexpr Vector2 operator*(const Vector2& v) const {return Vector2(x * v.x, y * v.y);}
  constexpr Vector2 operator/(const Vector2& v) const {return Vector2(x / v.x, y / v.y);}
  constexpr Vector2 operator*(T s) const {return Vector2(x*s, y*s);}
  constexpr Vector2 operator/(T s) const {return Vector2(x/s, y/s);}
  constexpr Vector2& operator+=(T s) { *this = *this + s; return *this; }
  constexpr Vector2 operator+(T s) const { return Vector2(x + s, y + s); }
  constexpr Vector2& operator-=(T s) { *this = *this + -s; return *this; }
  constexpr Vector2 operator-(T s) const { return *this + -s; }
  constexpr Vector2& operator+=(const Vector2& v) {x += v.x; y += v.y; return *this;}
  constexpr Vector2& operator-=(const Vector2& v) {x -= v.x; y -= v.y; return *this;}
  constexpr Vector2& operator*=(const Vector2& v) {x *= v.x; y *= v.y; return *this;}
  constexpr Vector2& operator/=(const Vector2& v) {x /= v.x; y /= v.y; return *this;}
  constexpr Vector2& operator*=(T s) {x *= s; y *= s; return *this;}
  constexpr Vector2& operator/=(T s) {x /= s; y /= s; return *this;}
  constexpr Vector2 operator-() const {return Vector2(-x, -y);}
  constexpr bool operator==(const Vector2& v) const {return x == v.x && y == v.y;}
  constexpr bool operator!=(const Vector2& v) const {return x != v.x || y != v.y;}
  //!}
};

template <typename T>
T Vector2<T>::Distance2(const Vector2<T>& v) const
{
  const T xx = x - v.x;
  const T yy = y - v.y;
  return (xx * xx) + (yy * yy);
}

template <typename T>
float Vector2<T>::AngleTo(const Vector2<T>& v) const
{
  // Angle difference between 2 vectors
  float angle = atan2(x, y) - atan2(v.x, v.y);

  // Make angle difference to be between <-Pi, Pi>
  angle += Pi; // Shift interval <-Pi, Pi> to <0, 2*Pi>
  const float Pi2 = Pi * 2.0f;
  angle = angle - Pi2 * std::floor(angle / Pi2); // Wrap angle in interval <0, 2*Pi>
  angle -= Pi; // Shift interval back to <-Pi, Pi>

  return angle;
}

template <typename T>
bool Vector2<T>::Equals(const Vector2<T>& other) const
{
  return (x == other.x) && (y == other.y);
}

//! 2D float vector
typedef Vector2<float> Vector2F;

//! 2D integer vector
typedef Vector2<int> Vector2I;


