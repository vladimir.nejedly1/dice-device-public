// Copyright (c) 2023 Vladimir Nejedly, vladimir.nejedly@gmail.com
// Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license
// https://creativecommons.org/licenses/by-nc-sa/4.0/
// This software is provided "as is", without any warranty or condition.

#pragma once

#include "freertos\FreeRTOS.h"
#include "freertos\semphr.h"

//! Synchronization object
class Mutex
{
private:

  //! Mutex
  SemaphoreHandle_t _mutex;

public:

  //! Constructor, creating mutex
  Mutex() { _mutex = xSemaphoreCreateMutex(); }
  //! Destructor, releasing mutex
  ~Mutex() { vSemaphoreDelete(_mutex); }
  //! Locking
  void Lock() { xSemaphoreTake(_mutex, portMAX_DELAY); }
  //! Unlocking
  void Unlock() { xSemaphoreGive(_mutex); }
};

//! Lock object passed as an argument
class ScopeLock
{
private:

  //! Object to lock
  Mutex* _mutex;

public:

  //! Constructor, locking object
  explicit ScopeLock(Mutex& mutex) : _mutex(&mutex)
  {
    _mutex->Lock();
  }

  //! Destructor, releasing lock
  ~ScopeLock()
  {
    _mutex->Unlock();
  }
};
