// Copyright (c) 2023 Vladimir Nejedly, vladimir.nejedly@gmail.com
// Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license
// https://creativecommons.org/licenses/by-nc-sa/4.0/
// This software is provided "as is", without any warranty or condition.

#include <memory>
#include "Arduino.h"
#include "lvgl.h"
#include "..\..\menu\ddApp.h"
#include "..\..\lib\Vector2.h"
#include "stdlib.h"

LV_IMG_DECLARE(waitClock);
LV_IMG_DECLARE(stories);
LV_IMG_DECLARE(storiesSet0);
LV_IMG_DECLARE(storiesSet1);
LV_IMG_DECLARE(storiesSet2);

//! Canvas buffer - helper class for reading encoded images, rendering them to correct format
class CanvasBuffer
{
  //! Canvas buffer
  lv_color_t* _buffer;
  //! Data source of buffer
  const lv_img_dsc_t* _src;
  //! Flag to determine if we use alpha on canvas or not
  bool _useAlpha;

public:

  //! Constructor
  CanvasBuffer(const lv_img_dsc_t* src, bool useAlpha = false) : _src(src), _useAlpha(useAlpha)
  {
    _buffer = (lv_color_t *)heap_caps_malloc(src->header.w * src->header.h * (sizeof(lv_color_t) + (useAlpha ? 1 : 0)), MALLOC_CAP_SPIRAM);
  }

  //! Destructor
  ~CanvasBuffer()
  {
    heap_caps_free(_buffer);
  }

  //! Prepare canvas buffer for canvas object
  void PrepareCanvasBuffer(lv_obj_t* canvas)
  {
    // Associate object with buffer
    lv_canvas_set_buffer(canvas, _buffer, _src->header.w, _src->header.h, _useAlpha ? LV_IMG_CF_TRUE_COLOR_ALPHA : LV_IMG_CF_TRUE_COLOR);

    // Prepare for rendering, background is empty including alpha
    lv_canvas_fill_bg(canvas, LV_COLOR_MAKE(0, 0, 0), LV_OPA_TRANSP);

    // Simple draw descriptor
    lv_draw_img_dsc_t drawDscSimple;
    lv_draw_img_dsc_init(&drawDscSimple);
    if (_useAlpha)
      drawDscSimple.recolor_opa = LV_OPA_COVER; // This ensures that target alpha is updated

    // Draw compas border on canvas
    lv_canvas_draw_img(canvas, 0, 0, _src, &drawDscSimple);
  }

  //! Reuse already prepared canvas buffer (suitable for sharing canvas data)
  void ReuseCanvasBuffer(lv_obj_t* canvas)
  {
    // Associate object with buffer
    lv_canvas_set_buffer(canvas, _buffer, _src->header.w, _src->header.h, _useAlpha ? LV_IMG_CF_TRUE_COLOR_ALPHA : LV_IMG_CF_TRUE_COLOR);
  }
};

class AppStories : public DDApp
{
  //! Buffer associated with canvas split to 3 sets, one for 3 dice
  std::shared_ptr<CanvasBuffer> _canvasBuffer[3];
  //! Individual dice with one image
  std::vector<lv_obj_t*> _dice;
  //! Dice currently touched
  int _touchedDice;
  //! X offset from the last touch
  int _offsetX;
  //! Y offset from the last touch
  int _offsetY;

public:
  //! Constructor
  AppStories(lv_obj_t* background) : DDApp(background), _touchedDice(-1), _offsetX(0), _offsetY(0)
  {
    // Show clock
    lv_obj_t* clock = lv_img_create(lv_scr_act());
    lv_img_set_src(clock, &waitClock);
    lv_obj_align(clock, LV_ALIGN_CENTER, 0, 0);
    lv_img_set_antialias(clock, false);

    // Refresh background
    _lv_disp_refr_timer(NULL);

    // Go through 3 stories sets
    const lv_img_dsc_t* storiesSet[] = {&storiesSet0, &storiesSet1, &storiesSet2};
    const int setNum = 3;
    for (int s = 0; s < setNum; s++)
    {
      // Canvas buffer with stories
      _canvasBuffer[s] = std::shared_ptr<CanvasBuffer>(new CanvasBuffer(storiesSet[s], true));

      // Create dice for each dice in a set
      srand(int(millis() % INT_MAX));
      const int diceInSetNum = 3;
      for (int i = 0; i < diceInSetNum; i++)
      {
        float angle = (float)(s * diceInSetNum + i) / (setNum * diceInSetNum) * Pi * 2.0f;
        float x = cos(angle);
        float y = sin(angle);
        lv_obj_t* d = lv_canvas_create(lv_scr_act());
        if (i == 0)
          _canvasBuffer[s]->PrepareCanvasBuffer(d);
        else
          _canvasBuffer[s]->ReuseCanvasBuffer(d);
        lv_obj_align(d, LV_ALIGN_CENTER, x * 180, y * 180);
        lv_obj_set_size(d, 96, 96);
        lv_img_set_offset_x(d, (rand() % 6) * 96);
        lv_img_set_offset_y(d, i * 96);
        _dice.push_back(d);
      }
    }

    // Hide clock
    lv_obj_del(clock);    
  }

  //! Destructor
  ~AppStories()
  {
    for (auto d : _dice)
      lv_obj_del(d);
  }

  //! Touch detection, invoked in a fast pace
  virtual void Touch(int tp, const Vector2I& pos)
  {
    // Move to center-aligned coordinates
    Vector2I cPos = pos - 240;

    // If touching screen....
    if (tp > 0)
    {
      // If dice was not selected yet
      if (_touchedDice < 0)
      {
        // Find closest dice to touch point
        _touchedDice = -1;
        int closestCubeDistance = 10000;
        for (int i = 0; i < _dice.size(); i++)
        {
          // Vector from touch point to button
          Vector2I vec(lv_obj_get_x_aligned(_dice[i]) - cPos.x, lv_obj_get_y_aligned(_dice[i]) - cPos.y);
          int distance = vec.Size();
          if (distance < closestCubeDistance)
          {
            _touchedDice = i;
            closestCubeDistance = distance;    
          }
        }

        // Remember offset from touch point to cube center
        _offsetX = lv_obj_get_x_aligned(_dice[_touchedDice]) - cPos.x;
        _offsetY = lv_obj_get_y_aligned(_dice[_touchedDice]) - cPos.y;
      }
      else
      {
        // Get new position. Do not allow moving dice outside of the screen
        Vector2I newPos = cPos + Vector2I(_offsetX, _offsetY);
        if (newPos.Size() > 240)
        {
          newPos.x = newPos.x * 240 / newPos.Size();
          newPos.y = newPos.y * 240 / newPos.Size();
        }

        // Update touched dice position
        lv_obj_set_x(_dice[_touchedDice], newPos.x);
        lv_obj_set_y(_dice[_touchedDice], newPos.y);
      }
    }
    else
    {
      // User is not touching anything
      _touchedDice = -1;
    }
  }  

  //! Simulation step, invoked every rendering frame
  virtual void Tick(int tp, bool pressed, bool depressed, const Vector2I& pos)
  {
  }
};

//! Application creator
class StoriesCreator : public DDAppCreator
{
public:
  //! Implementation of DDAppCreator
  virtual std::string Name() const {return "Stories";}
  //! Implementation of DDAppCreator
  virtual const lv_img_dsc_t* Icon() const {return &stories;}
  //! Implementation of DDAppCreator
  virtual std::shared_ptr<DDApp> Create(lv_obj_t* background) {return std::make_shared<AppStories>(background);}
};

//! Creator instance
StoriesCreator sc;