// Copyright (c) 2023 Vladimir Nejedly, vladimir.nejedly@gmail.com
// Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license
// https://creativecommons.org/licenses/by-nc-sa/4.0/
// This software is provided "as is", without any warranty or condition.

#include <memory>
#include "Arduino.h"
#include "lvgl.h"
#include "..\..\menu\pin_config.h"
#include "..\..\menu\ddApp.h"
#include "..\..\lib\Common.h"
#include "stdlib.h"

LV_IMG_DECLARE(gaudi);
LV_IMG_DECLARE(egypt);
LV_IMG_DECLARE(rome);
LV_IMG_DECLARE(newyork);
LV_IMG_DECLARE(future);

const int BlockWidth = 128;
const int BlockHeight = 32;

//! Class representing one block
class Block
{
  //! Left cut of block
  int _cutMin;
  //! Right cut of block
  int _cutMax;
  //! Color
  lv_color_t _color;
  //! Buffer holding block surface
  lv_color_t* _canvasBuffer;
  //! Canvas object
  lv_obj_t* _canvas;

private:

  //! Private copy constructor, the object is difficult to copy
  Block(const Block& obj) {}
  //! Private assignment, the object is difficult to copy
  Block& operator=(const Block& obj) { return *this; }

public:

  //! Constructor
  Block(int x, int y, int cutMin, int cutMax, lv_color_t color) : _cutMin(cutMin), _cutMax(cutMax), _color(color)
  {
    // Canvas object representing block
    const int width = cutMax - cutMin;
    _canvasBuffer = (lv_color_t *)heap_caps_malloc(width * BlockHeight * sizeof(lv_color_t), MALLOC_CAP_SPIRAM);
    _canvas = lv_canvas_create(lv_scr_act());
    lv_canvas_set_buffer(_canvas, _canvasBuffer, width, BlockHeight, LV_IMG_CF_TRUE_COLOR);
    lv_obj_align(_canvas, LV_ALIGN_CENTER, 0, 0);

    // Fill with color
    lv_canvas_fill_bg(_canvas, color, LV_OPA_COVER);

    // Block position
    SetPos(x, y);
  }

  //! Move constructor, so that structure can be in std::vector
  Block(Block&& obj) :
    _cutMin(obj._cutMin),
    _cutMax(obj._cutMax),
    _color(obj._color),
    _canvasBuffer(obj._canvasBuffer),
    _canvas(obj._canvas)
  {
    // Invalidate input buffers
    obj._canvasBuffer = nullptr;
    obj._canvas = nullptr;
  }

  //! Destructor
  ~Block()
  {
    if (_canvas)
      lv_obj_del(_canvas);
    if (_canvasBuffer)
      heap_caps_free(_canvasBuffer);
  }

  //! Setting up the position
  void SetPos(int x, int y)
  {
    int originalCenter = (0 + BlockWidth) / 2;
    int currentCenter = (_cutMin + _cutMax) / 2;
    lv_obj_set_pos(_canvas, x - originalCenter + currentCenter, y);
  }

  // Getting X coordinate
  int GetPosX() const
  {
    int originalCenter = (0 + BlockWidth) / 2;
    int currentCenter = (_cutMin + _cutMax) / 2;
    return lv_obj_get_x_aligned(_canvas) + originalCenter - currentCenter;
  }

  // Getting Y coordinate
  int GetPosY() const
  {
    return lv_obj_get_y_aligned(_canvas);
  }

  // Getting color
  lv_color_t GetColor() const
  {
    return _color;
  }

  //! Size of the block
  int Size() const {return _cutMax - _cutMin;}

  //!{ Wrappers
  int CutMin() const {return _cutMin;}
  int CutMax() const {return _cutMax;}
  //!}
};

//! Class representing one particle
class ParticleBlock
{
  //! Color
  lv_color_t _color;
  //! Buffer holding block surface
  lv_color_t* _canvasBuffer;
  //! Canvas object
  lv_obj_t* _canvas;
  //! Velocity
  float _velocityX;
  float _velocityY;
  //! Angular velocity
  float _aVelocity;
  //! Number of frames particle is going to live
  int _lifeTime = 50;

private:

  //! Private copy constructor, the object is difficult to copy
  ParticleBlock(const ParticleBlock& obj) {}
  //! Private assignment, the object is difficult to copy
  ParticleBlock& operator=(const ParticleBlock& obj) { return *this; }

public:

  //! Constructor
  ParticleBlock(int x, int y, int width, lv_color_t color, float velocityX, float velocityY, float aVelocity) :
    _color(color),
    _velocityX(velocityX),
    _velocityY(velocityY),
    _aVelocity(aVelocity)
  {
    // Canvas object representing block
    _canvasBuffer = (lv_color_t *)heap_caps_malloc(width * BlockHeight * sizeof(lv_color_t), MALLOC_CAP_SPIRAM);
    _canvas = lv_canvas_create(lv_scr_act());
    lv_canvas_set_buffer(_canvas, _canvasBuffer, width, BlockHeight, LV_IMG_CF_TRUE_COLOR);
    lv_obj_align(_canvas, LV_ALIGN_CENTER, 0, 0);

    // Fill with color
    lv_canvas_fill_bg(_canvas, color, LV_OPA_COVER);

    // Block position
    lv_obj_set_pos(_canvas, x, y);
  }

  //! Move constructor, so that structure can be in std::vector
  ParticleBlock(ParticleBlock&& obj) :
    _color(obj._color),
    _canvasBuffer(obj._canvasBuffer),
    _canvas(obj._canvas),
    _velocityX(obj._velocityX),
    _velocityY(obj._velocityY),
    _aVelocity(obj._aVelocity),
    _lifeTime(obj._lifeTime)
  {
    // Invalidate input buffers
    obj._canvasBuffer = nullptr;
    obj._canvas = nullptr;
  }

  //! Destructor
  ~ParticleBlock()
  {
    if (_canvas)
      lv_obj_del(_canvas);
    if (_canvasBuffer)
      heap_caps_free(_canvasBuffer);
  }

  //! Setting up the position
  void SetPos(int x, int y)
  {
    lv_obj_set_pos(_canvas, x, y);
  }

  // Getting X coordinate
  int GetPosX() const
  {
    return lv_obj_get_x_aligned(_canvas);
  }

  // Getting Y coordinate
  int GetPosY() const
  {
    return lv_obj_get_y_aligned(_canvas);
  }  

  // Simulation
  bool Simulate()
  {
    // Apply gravity
    _velocityX += 0.0f;
    _velocityY += 0.3f;

    // Update pos
    lv_obj_set_pos(_canvas,
      lv_obj_get_x_aligned(_canvas) + _velocityX,
      lv_obj_get_y_aligned(_canvas) + _velocityY);

    // Update angle
    const float avel = (int)(_aVelocity * 10);
    lv_img_set_angle(_canvas, (int)(lv_img_get_angle(_canvas) + avel));

    // Decrease life time
    _lifeTime -= 1;

    // Return if particle still lives
    if (_lifeTime > 0)
        return true;
    else
        return false;  
  }
};

//! Canvas buffer - helper class for reading encoded images, rendering them to correct format
class CanvasBuffer
{
  //! Canvas buffer
  lv_color_t* _buffer;
  //! Data source of buffer
  const lv_img_dsc_t* _src;
  //! Flag to determine if we use alpha on canvas or not
  bool _useAlpha;

public:

  //! Constructor
  CanvasBuffer(const lv_img_dsc_t* src, bool useAlpha = false) : _src(src), _useAlpha(useAlpha)
  {
    _buffer = (lv_color_t *)heap_caps_malloc(src->header.w * src->header.h * (sizeof(lv_color_t) + (useAlpha ? 1 : 0)), MALLOC_CAP_SPIRAM);
  }

  //! Destructor
  ~CanvasBuffer()
  {
    heap_caps_free(_buffer);
  }

  //! Prepare canvas buffer for canvas object
  void PrepareCanvasBuffer(lv_obj_t* canvas)
  {
    // Associate object with buffer
    lv_canvas_set_buffer(canvas, _buffer, _src->header.w, _src->header.h, _useAlpha ? LV_IMG_CF_TRUE_COLOR_ALPHA : LV_IMG_CF_TRUE_COLOR);

    // Prepare for rendering, background is empty including alpha
    lv_canvas_fill_bg(canvas, LV_COLOR_MAKE(0, 0, 0), LV_OPA_TRANSP);

    // Simple draw descriptor
    lv_draw_img_dsc_t drawDscSimple;
    lv_draw_img_dsc_init(&drawDscSimple);
    if (_useAlpha)
      drawDscSimple.recolor_opa = LV_OPA_COVER; // This ensures that target alpha is updated

    // Draw compas border on canvas
    lv_canvas_draw_img(canvas, 0, 0, _src, &drawDscSimple);
  }

  //! Reuse already prepared canvas buffer (suitable for sharing canvas data)
  void ReuseCanvasBuffer(lv_obj_t* canvas)
  {
    // Associate object with buffer
    lv_canvas_set_buffer(canvas, _buffer, _src->header.w, _src->header.h, _useAlpha ? LV_IMG_CF_TRUE_COLOR_ALPHA : LV_IMG_CF_TRUE_COLOR);
  }
};

//! Class representing a label with shadow
class Label
{
  //! Big text style
  lv_style_t _styleTextBig;
  //! Status text
  lv_obj_t* _statusTextShadow;
  //! Status text
  lv_obj_t* _statusText;

public:

  //! Constructor
  Label(lv_color_t color, int offsetX, int offsetY)
  {
    // Big text style initialization
    lv_style_init(&_styleTextBig);
    lv_style_set_text_font(&_styleTextBig, &lv_font_montserrat_32);

    // Shadow
    _statusTextShadow = lv_label_create(lv_scr_act());
    lv_obj_add_style(_statusTextShadow, &_styleTextBig, 0);
    lv_obj_set_style_text_color(_statusTextShadow, LV_COLOR_MAKE(0, 0, 0), 0);
    lv_obj_align(_statusTextShadow, LV_ALIGN_CENTER, offsetX + 3, offsetY + 3);

    // Regular text
    _statusText = lv_label_create(lv_scr_act());
    lv_obj_add_style(_statusText, &_styleTextBig, 0);
    lv_obj_set_style_text_color(_statusText, color, 0);
    lv_obj_align(_statusText, LV_ALIGN_CENTER, offsetX, offsetY);

    //lv_label_set_text_fmt(_statusText, "Item: %"LV_PRIu32, i);
    lv_label_set_text(_statusTextShadow, "Tap to Start");
    lv_label_set_text(_statusText, "Tap to Start");
  }

  //! Destructor
  ~Label()
  {
    lv_obj_del(_statusText);
    lv_obj_del(_statusTextShadow);
  }

  //! Updating the text
  void SetText(const char* text)
  {
    lv_label_set_text(_statusTextShadow, text);
    lv_label_set_text(_statusText, text);
  }
};

enum AppStatus
{
  AS_Welcome,
  AS_Playing,
  AS_GameOver,
};

//! Gaudi app
class AppGaudi : public DDApp
{
  //! Application status (started, game over, etc)
  AppStatus _status = AS_Welcome;
  //! Status text
  std::unique_ptr<Label> _statusText;
  //! Score text
  std::unique_ptr<Label> _score;
  //! Blocks for each row
  std::vector<Block> _blocks;
  //! New block object
  std::unique_ptr<Block> _newBlock;
  //! Blocks for each row
  std::vector<std::shared_ptr<ParticleBlock>> _particles;
  //! Direction of new block movement,  1 or -1
  int _newBlockDirection = 1;
  //! Number of pixels skipped every frame
  int _newBlockVelocity = 4;
  //! View offset
  int _blockViewOffset;
  //! State flag
  bool _gameOverFingerReleased;
  //! Buffer associated with canvas
  lv_color_t* _canvasBuffer;
  //! Canvas object
  lv_obj_t* _canvas;
  //! Age is not set at the beginning
  int _currentAge = -1;

protected:

  //! Convert layer index to Y coordinate
  int LayerToY(int layer) const
  {
    return 240 - (layer - _blockViewOffset)  * BlockHeight - 40;
  }

  //! Get new layer Y coordinate
  int NewBlockY() const
  {
    return LayerToY(_blocks.size());
  }

  //! Update view offset, move with existing blocks
  void UpdateViewOffset()
  {
    const int blockThreshold = 6;
    int diff = 0;
    if (_blocks.size() > blockThreshold)
    {
      int newOffset = _blocks.size() - blockThreshold;
      diff = newOffset - _blockViewOffset;
      _blockViewOffset = newOffset;
      for (int i = 0; i < _blocks.size(); i++)
      {
        _blocks[i].SetPos(_blocks[i].GetPosX(), LayerToY(i));
      }
    }

    // Update particles
    if (diff != 0)
    {
      for (int i = 0; i < _particles.size(); i++)
      {
        _particles[i]->SetPos(_particles[i]->GetPosX(), _particles[i]->GetPosY() + diff * BlockHeight);
      }
    }
  }

  //! Update Age
  void UpdateAge(int age)
  {
    // Lazy update of age
    if (age != _currentAge)
    {
      // Age images
      const lv_img_dsc_t* ageImage[] = {&egypt, &rome, &newyork, &future};

      // Simple draw descriptor
      lv_draw_img_dsc_t drawDscSimple;
      lv_draw_img_dsc_init(&drawDscSimple);

      // Draw compas border on canvas
      age = std::max(std::min(age, lenof(ageImage) - 1), 0);
      lv_canvas_draw_img(_canvas, 0, 0, ageImage[age], &drawDscSimple);

      // Update the current age
      _currentAge = age;
    }
  }

  //! Update scrore based on current number of blocks
  void UpdateScore()
  {
    char score[256];
    sprintf(score, "%d", _blocks.size());
    _score->SetText(score);
  }

  // Get random color from background
  lv_color_t GetRandomBackgroundColor() const
  {
    // Read color from background at random position
    lv_color_t color = _canvasBuffer[rand() % (EXAMPLE_LCD_H_RES * EXAMPLE_LCD_V_RES)];

    // Return Complementary color
    lv_color_hsv_t colorHSV = lv_color_to_hsv(color);
    colorHSV.h = (colorHSV.h + 180) % 360;
    colorHSV.s = 100;
    return lv_color_hsv_to_rgb(colorHSV.h, colorHSV.s, colorHSV.v);
  }

public:

  //! Constructor
  AppGaudi(lv_obj_t* background) :
    DDApp(background)
  {
    // Hide background
    lv_obj_add_flag(_background, LV_OBJ_FLAG_HIDDEN);

    // Canvas representing background
    _canvasBuffer = (lv_color_t *)heap_caps_malloc(EXAMPLE_LCD_H_RES * EXAMPLE_LCD_V_RES * sizeof(lv_color_t), MALLOC_CAP_SPIRAM);
    _canvas = lv_canvas_create(lv_scr_act());
    lv_canvas_set_buffer(_canvas, _canvasBuffer, EXAMPLE_LCD_H_RES, EXAMPLE_LCD_V_RES, LV_IMG_CF_TRUE_COLOR);

    // Status text
    _statusText = (std::unique_ptr<Label>) new Label(lv_color_white(), 0, 0);

    // Score text
    _score = (std::unique_ptr<Label>) new Label(lv_color_white(), 0, -210);

    // Prepare for a new game
    CleanUp();
  }

  //! Destructor
  ~AppGaudi()
  {
    // Delete background
    lv_obj_del(_canvas);
    heap_caps_free(_canvasBuffer);

    // Show background
    lv_obj_clear_flag(_background, LV_OBJ_FLAG_HIDDEN);
  }

  //! Prepare for next game start
  void CleanUp()
  {
    // We start with Egypt
    UpdateAge(0);

    // Prepare game
    _blocks.clear();
    _blockViewOffset = 0;
    _blocks.push_back(Block(0, NewBlockY(), 0, 128, GetRandomBackgroundColor()));
    _newBlock = (std::unique_ptr<Block>) new Block(-240 - BlockWidth/2, NewBlockY(), _blocks[0].CutMin(), _blocks[0].CutMax(), GetRandomBackgroundColor());
    _newBlockDirection = 1;
    _particles.clear();
    _gameOverFingerReleased = false;

    // Status update
    _status = AS_Welcome;    
    _statusText->SetText("Tap to Start");

    // No score at the beginning
    _score->SetText("");
  }

  //! Simulation step, invoked every rendering frame
  virtual void Tick(int tp, bool pressed, bool depressed, const Vector2I& pos)
  {
    // Welcome screen
    if (_status == AS_Welcome)
    {
      // Show information
      //lib.text.DrawText(surface, (0.01, -0.01), "Tap to Start", self._font, '#000000')
      //lib.text.DrawText(surface, (0, 0), "Tap to Start", self._font, '#00FF99')

      // If pressing, start playing
      if (pressed) 
      {
        // Go to playing mode
        _status = AS_Playing;
        _statusText->SetText("");
        UpdateScore();
      }
    }

    // Playing
    else if (_status == AS_Playing)
    {
      // Try to place a block
      if (pressed)
      {
        // Align block to be placed with the block underneath - cut side, place particle of corresponding size and color
        int leftOverhang = + (_blocks.back().GetPosX() - _blocks.back().CutMin()) - (_newBlock->GetPosX() - _newBlock->CutMin());
        Serial.printf("%d\n", leftOverhang);
        if (leftOverhang > 0)
        {
          int pbWidth = std::min(leftOverhang, _newBlock->Size());
          _particles.push_back(std::make_shared<ParticleBlock>(
            _newBlock->GetPosX() - BlockWidth / 2 + _newBlock->CutMin() + pbWidth / 2,
            _newBlock->GetPosY(),
            pbWidth,
            _newBlock->GetColor(),
            -2, 0,
            -3));
        }
        int rightOverhang = - (_blocks.back().GetPosX() - _blocks.back().CutMin()) + (_newBlock->GetPosX() - _newBlock->CutMin());
        if (rightOverhang > 0)
        {
          int pbWidth = std::min(rightOverhang, _newBlock->Size());
          _particles.push_back(std::make_shared<ParticleBlock>(
            _newBlock->GetPosX() - BlockWidth / 2 + _newBlock->CutMax() - pbWidth / 2,
            _newBlock->GetPosY(),
            pbWidth,
            _newBlock->GetColor(),
            +2, 0,
            +3));
        }

        // Detect game over or place a block
        if (leftOverhang >= _newBlock->Size() || rightOverhang >= _newBlock->Size())
        {
          // Remove block (it was converted to particle)
          _newBlock.reset(nullptr);

          // Game over screen
          _status = AS_GameOver;
          _statusText->SetText("Game Over");
        }

        else
        {
          // Place block
          _blocks.push_back(Block(_newBlock->GetPosX(), NewBlockY(),
            _newBlock->CutMin() + ((leftOverhang > 0) ? leftOverhang : 0),
            _newBlock->CutMax() - ((rightOverhang > 0) ? rightOverhang : 0),
            _newBlock->GetColor()));

          // Update view offset
          UpdateViewOffset();

          // Update age
          UpdateAge(_blocks.size() / 10);

          // Update score
          UpdateScore();

          // New block parameters
          lv_color_t color = GetRandomBackgroundColor();
          int cutMin = _blocks.back().CutMin();
          int cutMax = _blocks.back().CutMax();

          // Spawn a new block
          if (_newBlockDirection > 0)
          {
            _newBlock = (std::unique_ptr<Block>) new Block(+240 + BlockWidth/2, NewBlockY(), cutMin, cutMax, color);
            _newBlockDirection = -1;
          }
          else
          {
            _newBlock = (std::unique_ptr<Block>) new Block(-240 - BlockWidth/2, NewBlockY(), cutMin, cutMax, color);
            _newBlockDirection = +1;
          }
        }
      }

      // Simulate block
      if (_newBlock)
      {
        // Do simulation
        _newBlock->SetPos(_newBlock->GetPosX() + _newBlockVelocity * _newBlockDirection, _newBlock->GetPosY());

        // When block crossed screen, game over
        if ((_newBlock->GetPosX() > 240 + BlockWidth/2) || (_newBlock->GetPosX() < -240 - BlockWidth/2))
        {
          _status = AS_GameOver;
          _statusText->SetText("Game Over");
        }
      }

      // Simulate particles
      for (int i = 0; i < _particles.size(); i++)
      {
        if (!_particles[i]->Simulate())
          _particles.erase(_particles.begin() + i);
      }

      // Show score
      //lib.text.DrawText(surface, (0.81, -0.01), str(len(self._blocks)), self._font, '#000000')
      //lib.text.DrawText(surface, (0.8, 0), str(len(self._blocks)), self._font, '#9400FF')
    }

    // Game Over screen
    else if (_status == 2)
    {
      // Simulate particles
      for (int i = 0; i < _particles.size(); i++)
      {
        if (!_particles[i]->Simulate())
          _particles.erase(_particles.begin() + i);
      }

      // Show information
      //lib.text.DrawText(surface, (0.01, -0.01), "Game Over", self._font, '#000000')
      //lib.text.DrawText(surface, (0, 0), "Game Over", self._font, '#FF007F')

      // Show score
      //lib.text.DrawText(surface, (0.81, -0.01), str(len(self._blocks)), self._font, '#000000')
      //lib.text.DrawText(surface, (0.8, 0), str(len(self._blocks)), self._font, '#FFFFFF')

      // Detect the user had finger off during game over screen, only then he can tap to continue.
      // Without this check this screen would be skipped too quickly
      if (_gameOverFingerReleased)
      {
        // If tap was detected, go to welcome screen
        if (pressed)
        {
          // Clean-up tower status
          CleanUp();
        }
      }
      else
      {
        // Detect finger untouch status
        //if touch[2] == False:
        //    self._gameOverFingerReleased = True
      }
    }
  }
};

//! Application creator
class GaudiCreator : public DDAppCreator
{
public:
  //! Implementation of DDAppCreator
  virtual std::string Name() const {return "Gaudi";}
  //! Implementation of DDAppCreator
  virtual const lv_img_dsc_t* Icon() const {return &gaudi;}
  //! Implementation of DDAppCreator
  virtual std::shared_ptr<DDApp> Create(lv_obj_t* background) {return std::make_shared<AppGaudi>(background);}
};

//! Creator instance
GaudiCreator gc;