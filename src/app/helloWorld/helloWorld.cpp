#include <memory>
#include "Arduino.h"
#include "lvgl.h"
#include "..\..\menu\ddApp.h"
#include "stdlib.h"

//! Hello World Application
class AppHelloWorld : public DDApp
{
  //! Hello World text to be displayed in the center of the screen
  lv_obj_t* _text;

public:

  //! Constructor
  AppHelloWorld(lv_obj_t* background) : DDApp(background)
  {
    _text = lv_label_create(lv_scr_act());
    lv_obj_align(_text, LV_ALIGN_CENTER, 0, 0);
    lv_label_set_text(_text, "Hello World!");
  }

  //! Destructor
  ~AppHelloWorld()
  {
    lv_obj_del(_text);
  }

  //! Simulation step, invoked every rendering frame
  void Tick(int tp, bool pressed, bool depressed, const Vector2I& pos)
  {
    if (pressed)
      lv_label_set_text(_text, "Screen Was Touched");
  }
};

//! Application creator
class HelloWorldCreator : public DDAppCreator
{
public:
  //! Implementation of DDAppCreator
  virtual std::string Name() const {return "HelloWorld";}
  //! Implementation of DDAppCreator
  virtual std::shared_ptr<DDApp> Create(lv_obj_t* background) {return std::make_shared<AppHelloWorld>(background);}
};

//! Creator instance
//HelloWorldCreator hwc;