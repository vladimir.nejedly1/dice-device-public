// Copyright (c) 2023 Vladimir Nejedly, vladimir.nejedly@gmail.com
// Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license
// https://creativecommons.org/licenses/by-nc-sa/4.0/
// This software is provided "as is", without any warranty or condition.

#include <memory>
#include "Arduino.h"
#include "lvgl.h"
#include "..\..\menu\pin_config.h"
#include "..\..\menu\ddApp.h"
#include "..\..\lib\Vector2.h"
#include "..\..\lib\Color.h"
#include "stdlib.h"

struct Time
{
  //! Variable representing a time, in seconds
  float _time;
  //! Get minute from time
  float GetMinutes() const { return _time / 60.0f; }
  //! Update minutes, keep seconds
  void UpdateMinutes(int minutes) { _time = minutes * 60 + GetSeconds(); }
  //! Get second from time
  float GetSeconds() const { return fmod(_time, 60.0f); }
  //! Update seconds, keep minutes
  void UpdateSeconds(float seconds)
  {
    // Remove original seconds and replace them with new
    _time = std::floor(GetMinutes()) * 60.0f + seconds;

    // Normalize value, stick to interval <0, MaxSeconds) even for negative numbers
    const float MaxSeconds = 60.0f * 60.0f;
    _time = _time - MaxSeconds * std::floor(_time / MaxSeconds);
  }
  //! Get Minute arrow angle in radians
  float GetMinutesAngle() const
  {
    return GetMinutes() / 60.0f * Pi * 2.0f;
  }
  //! Set minute based on angle in radians, keep seconds
  void UpdateMinutesAngle(float angle)
  {
    UpdateMinutes((int)std::floor(angle / (Pi * 2.0f) * 60.0f));
  }
  //! Get Second arrow angle in radians
  float GetSecondsAngle() const
  {
    return GetSeconds() / 60.0f * Pi * 2.0f;
  }
  //! Set seconds based on angle in radians, keep minutes, rounded
  void UpdateSecondsAngle(float angle)
  {
    UpdateSeconds(std::floor(angle / (Pi * 2.0f) * 60.0f));
  }
};

enum TimerState
{
  TS_Paused,
  TS_CountDown,
  TS_CountUp,
};

enum EditArrow
{
  EA_Seconds,
  EA_None = -1,
};

//! Class representing an arrow (both min and sec)
class Arrow
{
  //! Buffer associated with arrow
  ColorAlpha* _arrowBuffer;
  //! Arrow objects
  lv_obj_t* _arrow;
  //! Position of the center of the arrow
  int _offset;

public:

  //! Constructor
  Arrow(const lv_color_t& color, int width, int start, int end, bool triangle)
  {
    // Canvas representing arrow
    const int ArrowWidth = width;
    const int ArrowHeight = end - start;
    _arrowBuffer = (ColorAlpha *)heap_caps_malloc(ArrowWidth * ArrowHeight * sizeof(ColorAlpha), MALLOC_CAP_SPIRAM);
    _arrow = lv_canvas_create(lv_scr_act());
    _offset = (start + end) / 2;
    lv_canvas_set_buffer(_arrow, _arrowBuffer, ArrowWidth, ArrowHeight, LV_IMG_CF_TRUE_COLOR_ALPHA);

    // Clear the canvas
    lv_canvas_fill_bg(_arrow, LV_COLOR_MAKE(0, 0, 0), LV_OPA_TRANSP);

    // Various shapes
    if (triangle)
    {
      // Triangle
      lv_draw_rect_dsc_t rect_dsc;
      lv_draw_rect_dsc_init(&rect_dsc);
      rect_dsc.bg_color = color;
      lv_point_t p[3];
      p[0].x = ArrowWidth / 2;
      p[0].y = ArrowHeight;
      p[1].x = 0;
      p[1].y = 0;
      p[2].x = ArrowWidth;
      p[2].y = 0;
      lv_canvas_draw_polygon(_arrow, p, 3, &rect_dsc);      
    }
    else
    {
      // Rounded tip of arrow
      lv_draw_arc_dsc_t arc_dsc;
      lv_draw_arc_dsc_init(&arc_dsc);
      arc_dsc.color = color;
      arc_dsc.width = ArrowWidth / 2;
      lv_canvas_draw_arc(_arrow, ArrowWidth / 2, ArrowWidth / 2, ArrowWidth / 2, 0, 360, &arc_dsc);

      // Arrow body
      lv_draw_rect_dsc_t rect_dsc;
      lv_draw_rect_dsc_init(&rect_dsc);
      rect_dsc.bg_color = color;
      lv_canvas_draw_rect(_arrow, 0, ArrowWidth / 2, ArrowWidth, ArrowHeight - ArrowWidth / 2, &rect_dsc);
    }

    // Set object properties
    lv_obj_align(_arrow, LV_ALIGN_CENTER, 0, 0);
    lv_img_set_antialias(_arrow, false);
  }

  //! Destructor
  ~Arrow()
  {
    lv_obj_del(_arrow);
    heap_caps_free(_arrowBuffer);
  }

  //! Set angle of an arrow
  void SetAngle(float angle)
  {
    lv_obj_set_pos(_arrow,
      +sin(angle) * _offset,
      -cos(angle) * _offset);
    lv_img_set_angle(_arrow, (int)(angle / (Pi * 2) * 3600) % 3600);
  }

  //! Show / Hide arrow
  void Show(bool show)
  {
    if (show)
      lv_obj_clear_flag(_arrow, LV_OBJ_FLAG_HIDDEN);
    else
      lv_obj_add_flag(_arrow, LV_OBJ_FLAG_HIDDEN);
  }
};

//! Object that flashes background as an alarm
class Alarm
{
  //! Object representing a background
  lv_obj_t* _back;

  //! Alarm time
  float _time = 1.5f;

public:

  //! Constructor
  Alarm()
  {
    // Create a new object that covers the entire screen
    _back = lv_obj_create(lv_scr_act());
    lv_obj_set_size(_back, LV_HOR_RES, LV_VER_RES);

    // Set the background color of the object
    lv_obj_set_style_bg_color(_back, LV_COLOR_MAKE(254, 87, 34), 0);
  }

  //! Destructor
  ~Alarm()
  {
    // Destroy object
    lv_obj_del(_back);

    // Make sure we end with light enabled
    digitalWrite(EXAMPLE_PIN_NUM_BK_LIGHT, EXAMPLE_LCD_BK_LIGHT_ON_LEVEL);
  }

  //! Simulation of alarm, return false if it is not active anymore and we want to destroy it
  bool Tick(float deltaT)
  {
    // Progress time
    _time -= deltaT;

    // Detect the alarm should end
    if (_time < 0.0f)
      return false;

    // Flash
    int status = (int)(_time * 10.0f);
    if (status & 1)
      digitalWrite(EXAMPLE_PIN_NUM_BK_LIGHT, EXAMPLE_LCD_BK_LIGHT_ON_LEVEL);
    else
      digitalWrite(EXAMPLE_PIN_NUM_BK_LIGHT, EXAMPLE_LCD_BK_LIGHT_OFF_LEVEL);

    return true;
  }

};

const int InnerCircleRadius = 140;

LV_FONT_DECLARE(DejaVuSans_64);

//! Timer application
class AppTimer : public DDApp
{
  //! Timer state
  TimerState _state = TS_Paused;
  //! Counter representing a time
  Time _counter = {0.0f};
  //! Start time for countdown
  Time _counterTarget = {0.0f};

  //! Arrow we are dragging
  EditArrow _editArrow = EA_None;
  //! Counter at the start of editing
  Time _editCounter = {0.0f};
  //! Current rotation angle
  float _editAngle = 0.0f;
  //! Last touched point
  Vector2I _lastTouchedPoint = Vector2I(0, 0);


  //! Buffer associated with canvas
  lv_color_t* _canvasBuffer;
  //! Canvas object
  lv_obj_t* _canvas;

  //! Arrow representing a minute
  std::unique_ptr<Arrow> _arrowMin;
  //! Arrow representing a second
  std::unique_ptr<Arrow> _arrowSec;

  //! Arrow representing a minute target for countdown
  std::unique_ptr<Arrow> _arrowTargetMin;
  //! Arrow representing a second target for countdown
  std::unique_ptr<Arrow> _arrowTargetSec;

  // Object representing alarm
  std::unique_ptr<Alarm> _alarm;

  lv_style_t _styleTextBig;
  lv_obj_t* _labelTime;


  



public:
  //! Constructor
  AppTimer(lv_obj_t* background) : DDApp(background)
  {
    // Hide background
    lv_obj_add_flag(_background, LV_OBJ_FLAG_HIDDEN);







    // Canvas representing background
    _canvasBuffer = (lv_color_t *)heap_caps_malloc(EXAMPLE_LCD_H_RES * EXAMPLE_LCD_V_RES * sizeof(lv_color_t), MALLOC_CAP_SPIRAM);
    _canvas = lv_canvas_create(lv_scr_act());
    lv_canvas_set_buffer(_canvas, _canvasBuffer, EXAMPLE_LCD_H_RES, EXAMPLE_LCD_V_RES, LV_IMG_CF_TRUE_COLOR);

    {
      // Background color
      lv_canvas_fill_bg(_canvas, LV_COLOR_MAKE(39, 31, 28), LV_OPA_COVER);

      // Inner circle
      lv_draw_arc_dsc_t da;
      lv_draw_arc_dsc_init(&da);
      da.color = LV_COLOR_MAKE(0, 0, 0);
      da.width = 3;
      lv_canvas_draw_arc(_canvas, 240, 240, InnerCircleRadius, 0, 360, &da);

      // Border
      lv_draw_line_dsc_t dl;
      lv_draw_line_dsc_init(&dl);

      const int textWidth = 40;
      lv_draw_label_dsc_t dd;
      lv_draw_label_dsc_init(&dd);
      dd.font = &lv_font_montserrat_20;
      dd.ofs_x = -textWidth/2;
      dd.ofs_y = -20/2;
      dd.color = LV_COLOR_MAKE(255, 255, 255);
      dd.align = LV_TEXT_ALIGN_CENTER;

      for (int i = 0; i < 60; i++)
      {
        // Thick vs thin lines
        int lineEnd;
        if ((i % 5) == 0)
        {
          dl.width = 4;
          dl.color = LV_COLOR_MAKE(255, 255, 255);
          lineEnd = 240;
        }
        else
        {
          dl.width = 2;
          dl.color = LV_COLOR_MAKE(128, 128, 128);
          lineEnd = 235;
        }

        float arad = i / 60.0f * Pi * 2.0f;
        lv_point_t p[2];
        const int lineStart = 210;
        p[0].x = 240 + sin(arad) * lineStart;
        p[0].y = 240 - cos(arad) * lineStart;
        p[1].x = 240 + sin(arad) * lineEnd;
        p[1].y = 240 - cos(arad) * lineEnd;
        lv_canvas_draw_line(_canvas, p, 2, &dl);

         
        // Draw numbers
        if ((i % 5) == 0)
        {
          const int numberStart = 190;
          char text[3];
          sprintf(text, "%d", i);
          lv_canvas_draw_text(_canvas,
            240 + sin(arad) * numberStart,
            240 - cos(arad) * numberStart,
            textWidth, &dd, text);

        }

      }



    }





    // Big text style initialization
    lv_style_init(&_styleTextBig);
    lv_style_set_text_font(&_styleTextBig, &DejaVuSans_64);
    lv_style_set_text_color(&_styleTextBig, LV_COLOR_MAKE(254, 87, 34));
    
    // Create label to represent a time
    _labelTime = lv_label_create(lv_scr_act());
    lv_obj_add_style(_labelTime, &_styleTextBig, 0);
    lv_obj_align(_labelTime, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_align(_labelTime, LV_TEXT_ALIGN_CENTER, 0);

    _arrowMin = (std::unique_ptr<Arrow>) new Arrow(LV_COLOR_MAKE(255, 255, 255), 18, InnerCircleRadius, 200, false);
    _arrowSec = (std::unique_ptr<Arrow>) new Arrow(LV_COLOR_MAKE(254, 87, 34), 10, InnerCircleRadius, 220, false);

    _arrowTargetMin = (std::unique_ptr<Arrow>) new Arrow(LV_COLOR_MAKE(255, 255, 255),  23, 225, 241, true);
    _arrowTargetSec = (std::unique_ptr<Arrow>) new Arrow(LV_COLOR_MAKE(254, 87, 34), 23, 225, 241, true);
    _arrowTargetMin->Show(false);
    _arrowTargetSec->Show(false);



    

    

  }

  //! Destructor
  ~AppTimer()
  {
    lv_obj_del(_labelTime);

    lv_obj_del(_canvas);
    heap_caps_free(_canvasBuffer);


    // Show background
    lv_obj_clear_flag(_background, LV_OBJ_FLAG_HIDDEN);
  }

  //! Touch detection, invoked in a fast pace
  virtual void Touch(int tp, const Vector2I& pos)
  {
    // New touch point
    if (tp)
    {
      // In case the vector makes sense, update the rotation angle and velocity based on previous and new touch
      // Also check there was a non-zero time difference from the previous record (in case of multiple touches it can be zero),
      // otherwise it would lead to zero division
      Vector2I cPos = pos - 240;
      if (cPos.Size() > 0)
      {
        // If we just start with editing, select what to edit
        if ((_lastTouchedPoint.Size() <= 0) && (cPos.Size() > InnerCircleRadius))
        {
          _editArrow = EA_Seconds;
          _editCounter = _counter;
          _editAngle = _counter.GetSecondsAngle();
        }

        // Do editing
        if (_editArrow != EA_None)
        {
          // If previous touch point make sense, get delta and update angle
          if (_lastTouchedPoint.Size() > 0)
          {
            float angleDelta = _lastTouchedPoint.AngleTo(cPos);
            _editAngle = _editAngle + angleDelta;

            _counter = _editCounter;
            _counter.UpdateSecondsAngle(_editAngle);

            _counterTarget = _counter;

            // Update Target arrows
            _arrowTargetMin->SetAngle(_counterTarget.GetMinutesAngle());
            _arrowTargetSec->SetAngle(_counterTarget.GetSecondsAngle());
            bool show = _counterTarget._time > 0.0f;
            _arrowTargetMin->Show(show);
            _arrowTargetSec->Show(show);

            // Stop timer when editing
            _state = TS_Paused;
          }
        }
      }

      // Update _lastTouchedPoint to new positon
      _lastTouchedPoint = cPos;
    }
    else
    {
      // User is not touching, set invalid value
      _lastTouchedPoint = Vector2I(0, 0);
      _editArrow = EA_None;
    }

  }  

  //! Simulation step, invoked every rendering frame
  virtual void Tick(int tp, bool pressed, bool depressed, const Vector2I& pos)
  {
    // Calculate time since the last call
    static unsigned long lastTime = 0;
    const unsigned long currTime = micros();
    float deltaT = (lastTime > 0) ? (float)((double)(currTime - lastTime) / (1000.0 * 1000.0)) : 1.0f / 45.0f;
    lastTime = currTime;

    // Simulate Alarm if it is running
    if (_alarm.get())
    {
      if (_alarm->Tick(deltaT))
        return;
      else
        _alarm = nullptr;
    }


    Vector2I cPos = pos - 240;
    bool pausePressed = pressed && cPos.Size() <= InnerCircleRadius;

    // Detect double press
    bool pauseDoublePressed = false;
    if (pausePressed)
    {
      static unsigned long lastPausePressed = 0;
      unsigned long pressTime = millis();
      if (pressTime - lastPausePressed < 200)
        pauseDoublePressed = true;
      lastPausePressed = pressTime;
    }

    if (pauseDoublePressed)
    {
      _counter = _counterTarget;
      _state = TS_Paused;
    }
    else if (_state == TS_Paused)
    {
      if (pausePressed)
      {
        if (_counterTarget._time > 0.0f)
        {
          _state = TS_CountDown;
        }
        else
        {
          _state = TS_CountUp;
        }
      }
    }
    else if (_state == TS_CountDown)
    {
      if (pausePressed)
      {
        _state = TS_Paused;
      }
      else
      {
        _counter._time -= deltaT;
        if (_counter._time <= 0.0f)
        {
          _counter = _counterTarget;
          _state = TS_Paused;
          _alarm = (std::unique_ptr<Alarm>) new Alarm();
        }
      }
    }
    else if (_state == TS_CountUp)
    {
      if (pausePressed)
      {
        _state = TS_Paused;
      }
      else
      {
        _counter._time += deltaT;
      }
    }

    char timeText[32];
    sprintf(timeText, "%02d:%02d.%d", (int)std::floor(_counter._time / 60.0f) % 60, (int)std::floor(_counter._time) % 60, (int)std::floor(_counter._time * 10.0f) % 10);
    lv_label_set_text(_labelTime, timeText);


    _arrowMin->SetAngle(_counter.GetMinutesAngle());
    _arrowSec->SetAngle(_counter.GetSecondsAngle());



  }
};

LV_IMG_DECLARE(timer);

//! Application creator
class TimerCreator : public DDAppCreator
{
public:
  //! Implementation of DDAppCreator
  virtual std::string Name() const {return "Timer";}
  //! Implementation of DDAppCreator
  virtual const lv_img_dsc_t* Icon() const {return &timer;}
  //! Implementation of DDAppCreator
  virtual std::shared_ptr<DDApp> Create(lv_obj_t* background) {return std::make_shared<AppTimer>(background);}
};

//! Creator instance
TimerCreator timc;