// Copyright (c) 2023 Vladimir Nejedly, vladimir.nejedly@gmail.com
// Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license
// https://creativecommons.org/licenses/by-nc-sa/4.0/
// This software is provided "as is", without any warranty or condition.

#include "Arduino.h"
#include "lvgl.h"
#include "..\..\menu\pin_config.h"
#include "..\..\menu\ddApp.h"
#include "..\..\lib\Vector2.h"
#include <memory>

enum LorcanaTheme
{
  LC_Bright,
  LC_Dark,
};

//! Parameters configured by the menu
struct Parameters
{
  //! Status of application - if it has started or is still in the menu
  bool _started = false;
  //! Background Theme
  LorcanaTheme _lt;
};

//! Menu dialog
class MenuLorcana
{
  //! Parameters stored in the application
  Parameters& _parameters;

  //!{ Styles
  lv_style_t _styleShadow;
  lv_style_t _styleTextMedium;
  lv_style_t _styleTextBig;
  lv_style_t _styleSelection;
  lv_style_t _styleSelectionItems;
  //!}

  //!{ Widgets
  lv_obj_t* _btnGo;
  lv_obj_t* _btnType;
  //!}

  //!{ Events
  static void eventGo(lv_event_t* e)
  {
    Parameters* parameters = (Parameters*) e->user_data;
    lv_event_code_t code = lv_event_get_code(e);
    if (code == LV_EVENT_CLICKED)
    {
      parameters->_started = true;
    }
  }
  static void eventType(lv_event_t* e)
  {
    Parameters* parameters = (Parameters*) e->user_data;
    lv_obj_t* obj = lv_event_get_target(e);
    uint32_t id = lv_btnmatrix_get_selected_btn(obj);
    if (id == 0)
      parameters->_lt = LC_Bright;
    else
      parameters->_lt = LC_Dark;
  }
  //!}

public:

  //! Constructor
  MenuLorcana(Parameters& parameters) : _parameters(parameters)
  {
    // Shadow style initialization
    lv_style_init(&_styleShadow);
    lv_style_set_shadow_width(&_styleShadow, 50);
    lv_style_set_shadow_color(&_styleShadow, LV_COLOR_MAKE(0, 0, 0));
    lv_style_set_border_opa(&_styleShadow, LV_OPA_COVER);

    // Medium text style initialization
    lv_style_init(&_styleTextMedium);
    lv_style_set_text_font(&_styleTextMedium, &lv_font_montserrat_20);

    // Big text style initialization
    lv_style_init(&_styleTextBig);
    lv_style_set_text_font(&_styleTextBig, &lv_font_montserrat_32);

    // Selection, to be used by button matrix object
    lv_style_init(&_styleSelection);
    lv_style_set_pad_all(&_styleSelection, 0);
    lv_style_set_pad_gap(&_styleSelection, 0);
    lv_style_set_clip_corner(&_styleSelection, true);
    lv_style_set_border_width(&_styleSelection, 0);

    // Selection items, to be used by button matrix object for items
    lv_style_init(&_styleSelectionItems);
    lv_style_set_border_side(&_styleSelectionItems, LV_BORDER_SIDE_INTERNAL);
    lv_style_set_border_width(&_styleSelectionItems, 0);
    lv_style_set_radius(&_styleSelectionItems, 0);

    // Go button
    _btnGo = lv_btn_create(lv_scr_act());
    lv_obj_add_style(_btnGo, &_styleShadow, 0);
    lv_obj_add_style(_btnGo, &_styleTextBig, 0);
    lv_obj_align(_btnGo, LV_ALIGN_CENTER, 0, 180);
    lv_obj_set_ext_click_area(_btnGo, 50);
    lv_obj_t* labelGo = lv_label_create(_btnGo);
    lv_label_set_text(labelGo, "Go!");
    lv_obj_add_event_cb(_btnGo, eventGo, LV_EVENT_ALL, &_parameters);

    // Single/double button matrix
    _btnType = lv_btnmatrix_create(lv_scr_act());
    lv_obj_add_style(_btnType, &_styleShadow, 0);
    lv_obj_add_style(_btnType, &_styleTextMedium, 0);
    lv_obj_add_style(_btnType, &_styleSelection, 0);
    lv_obj_add_style(_btnType, &_styleSelectionItems, LV_PART_ITEMS);
    lv_obj_align(_btnType, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_size(_btnType, 96, 100);
    static const char * mapType[] = {"Light", "\n", "Dark", ""};
    lv_btnmatrix_set_map(_btnType, mapType);
    lv_btnmatrix_set_btn_ctrl_all(_btnType, LV_BTNMATRIX_CTRL_CHECKABLE);
    lv_btnmatrix_set_one_checked(_btnType, true);
    lv_btnmatrix_set_btn_ctrl(_btnType, 0, LV_BTNMATRIX_CTRL_CHECKED);
    lv_obj_add_event_cb(_btnType, eventType, LV_EVENT_VALUE_CHANGED, &_parameters);
  }

  //! Destructor
  ~MenuLorcana()
  {
    lv_obj_del(_btnType);
    lv_obj_del(_btnGo);
  }
};

LV_IMG_DECLARE(adPlus);
LV_IMG_DECLARE(adMinus);
LV_IMG_DECLARE(adScoreFrame);
LV_FONT_DECLARE(DejaVuSans_48);

//! One counter drawing
class Counter
{
  //! Button representing +
  lv_obj_t* _btnPlus;
  //! Transformed button pos
  Vector2I _btnPlusPos;
  //! Button representing -
  lv_obj_t* _btnMinus;
  //! Transformed button pos
  Vector2I _btnMinusPos;  
  //! Image representing score frame
  lv_obj_t* _scoreFrame;
  //! Score text style
  lv_style_t _styleTextBig;
  //! Label representing the score
  lv_obj_t* _labelScore;
  //! Current score
  int _scoreNum = 0;

public:

  //! Constructor
  Counter(int angle)
  {
    static lv_style_t style_def;
    lv_style_init(&style_def);
    lv_style_set_text_color(&style_def, lv_color_white());

    // Add a shadow
    lv_style_set_radius(&style_def, 64);
    lv_style_set_shadow_width(&style_def, 50);
    lv_style_set_shadow_color(&style_def, LV_COLOR_MAKE(64, 64, 64));
    lv_style_set_border_opa(&style_def, LV_OPA_COVER);

    // Darken the button when pressed and make it wider
    static lv_style_t style_pr;
    lv_style_init(&style_pr);
    lv_style_set_img_recolor_opa(&style_pr, LV_OPA_30);
    lv_style_set_shadow_width(&style_pr, 25);

    // Plus button
    _btnPlus = lv_img_create(lv_scr_act());
    lv_img_set_src(_btnPlus, &adPlus);
    lv_obj_align(_btnPlus, LV_ALIGN_CENTER, 120, 90);
    lv_img_set_antialias(_btnPlus, false);

    // Rotate
    lv_obj_update_layout(_btnPlus);
    lv_obj_set_style_transform_pivot_x(_btnPlus, lv_obj_get_width(_btnPlus)  / 2 - lv_obj_get_x_aligned(_btnPlus), 0);
    lv_obj_set_style_transform_pivot_y(_btnPlus, lv_obj_get_height(_btnPlus) / 2 - lv_obj_get_y_aligned(_btnPlus), 0);
    lv_obj_set_style_transform_angle(_btnPlus, angle, 0);

    // Remember position
    lv_point_t pointPlus;
    pointPlus.x = lv_obj_get_x(_btnPlus) + lv_obj_get_width(_btnPlus)  / 2;
    pointPlus.y = lv_obj_get_y(_btnPlus) + lv_obj_get_height(_btnPlus) / 2;
    lv_obj_transform_point(_btnPlus, &pointPlus, false, false);
    _btnPlusPos = Vector2I(pointPlus.x, pointPlus.y);

    // Minus button
    _btnMinus = lv_img_create(lv_scr_act());
    lv_img_set_src(_btnMinus, &adMinus);
    lv_obj_align(_btnMinus, LV_ALIGN_CENTER, -120, 90);
    lv_img_set_antialias(_btnMinus, false);

    // Rotate
    lv_obj_update_layout(_btnMinus);
    lv_obj_set_style_transform_pivot_x(_btnMinus, lv_obj_get_width(_btnMinus)  / 2 - lv_obj_get_x_aligned(_btnMinus), 0);
    lv_obj_set_style_transform_pivot_y(_btnMinus, lv_obj_get_height(_btnMinus) / 2 - lv_obj_get_y_aligned(_btnMinus), 0);
    lv_obj_set_style_transform_angle(_btnMinus, angle, 0);

    // Remember position
    lv_point_t pointMinus;
    pointMinus.x = lv_obj_get_x(_btnMinus) + lv_obj_get_width(_btnMinus)  / 2;
    pointMinus.y = lv_obj_get_y(_btnMinus) + lv_obj_get_height(_btnMinus) / 2;
    lv_obj_transform_point(_btnMinus, &pointMinus, false, false);
    _btnMinusPos = Vector2I(pointMinus.x, pointMinus.y);

    // Frame of the score
    _scoreFrame = lv_img_create(lv_scr_act());
    lv_img_set_src(_scoreFrame, &adScoreFrame);
    lv_obj_align(_scoreFrame, LV_ALIGN_CENTER, 0, 120);
    lv_img_set_antialias(_scoreFrame, false);

    // Rotate
    lv_obj_update_layout(_scoreFrame);
    lv_obj_set_style_transform_pivot_x(_scoreFrame, lv_obj_get_width(_scoreFrame)  / 2 - lv_obj_get_x_aligned(_scoreFrame), 0);
    lv_obj_set_style_transform_pivot_y(_scoreFrame, lv_obj_get_height(_scoreFrame) / 2 - lv_obj_get_y_aligned(_scoreFrame), 0);
    lv_obj_set_style_transform_angle(_scoreFrame, angle, 0);

    // Big text style initialization
    lv_style_init(&_styleTextBig);
    lv_style_set_text_font(&_styleTextBig, &DejaVuSans_48);
    lv_style_set_text_color(&_styleTextBig, LV_COLOR_MAKE(0, 0, 0));
    
    // Create label to represent a time
    const int ScoreWidth = 64;
    const int ScoreHeight = 64;
    _labelScore = lv_label_create(lv_scr_act());
    lv_obj_add_style(_labelScore, &_styleTextBig, 0);
    lv_obj_set_width(_labelScore, ScoreWidth);
    lv_obj_set_height(_labelScore, ScoreHeight);
    lv_obj_align(_labelScore, LV_ALIGN_CENTER, 0, 150);
    lv_obj_set_style_text_align(_labelScore, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_set_style_text_letter_space(_labelScore, -5, 0);
    lv_obj_set_style_transform_pivot_x(_labelScore, ScoreWidth / 2 - lv_obj_get_x_aligned(_labelScore), 0);
    lv_obj_set_style_transform_pivot_y(_labelScore, ScoreHeight / 2 - lv_obj_get_y_aligned(_labelScore), 0);
    lv_obj_set_style_transform_angle(_labelScore, angle, 0);

    UpdateScore(0);
  }

  //! Destructor
  ~Counter()
  {
    lv_obj_del(_labelScore);
    lv_obj_del(_scoreFrame);
    lv_obj_del(_btnMinus);
    lv_obj_del(_btnPlus);
  }

  void UpdateScore(int delta)
  {
    _scoreNum = std::max(std::min(_scoreNum + delta, 20), 0);
    char score[4];
    sprintf(score, "%d", _scoreNum);
    lv_label_set_text(_labelScore, score);
  }

  void Tick(int tp, bool pressed, bool depressed, const Vector2I& pos)
  {
    if (pressed)
    {
      if (_btnPlusPos.Distance(pos) < 90)
        UpdateScore(+1);
      if (_btnMinusPos.Distance(pos) < 90)
        UpdateScore(-1);
    }
  }
};

LV_IMG_DECLARE(adThemeLight);
LV_IMG_DECLARE(adThemeDark);
LV_IMG_DECLARE(adLine);

//! Dice application
class AppLorcana : public DDApp
{
  //! Parameters of the application, set by initial Menu dialog, used by application
  Parameters _parameters;
  //! Menu dialog
  std::shared_ptr<MenuLorcana> _menu;
  //! Buffer associated with canvas
  lv_color_t* _canvasBuffer = nullptr;
  //! Canvas object
  lv_obj_t* _canvas = nullptr;

  //!{ Counters
  std::shared_ptr<Counter> _counter1;
  std::shared_ptr<Counter> _counter2;
  //!}

public:

  //! Constructor
  AppLorcana(lv_obj_t* background) :
    DDApp(background)
  {
    // Application starts with dialog where user can adjust parameters
    _menu = std::shared_ptr<MenuLorcana>(new MenuLorcana(_parameters));
  }

  //! Destructor
  ~AppLorcana()
  {
    if (_canvas)
      lv_obj_del(_canvas);
    if (_canvasBuffer)
      heap_caps_free(_canvasBuffer);

    // Show background
    lv_obj_clear_flag(_background, LV_OBJ_FLAG_HIDDEN);
  }

  //! Simulation step
  virtual void Tick(int tp, bool pressed, bool depressed, const Vector2I& pos)
  {
    // If application has started (means we are not in the dialog anymore), run
    if (_parameters._started)
    {
      // If dialog still exists, initialize run, destroy the dialog
      if (_menu != nullptr)
      {
        // Destroy the menu
        _menu.reset();

        // Hide background
        lv_obj_add_flag(_background, LV_OBJ_FLAG_HIDDEN);

        // Canvas representing background
        _canvasBuffer = (lv_color_t *)heap_caps_malloc(EXAMPLE_LCD_H_RES * EXAMPLE_LCD_V_RES * sizeof(lv_color_t), MALLOC_CAP_SPIRAM);
        _canvas = lv_canvas_create(lv_scr_act());
        lv_canvas_set_buffer(_canvas, _canvasBuffer, EXAMPLE_LCD_H_RES, EXAMPLE_LCD_V_RES, LV_IMG_CF_TRUE_COLOR);

        // Simple draw descriptor
        lv_draw_img_dsc_t drawDscSimple;
        lv_draw_img_dsc_init(&drawDscSimple);

        // Draw compas border on canvas
        if (_parameters._lt == LC_Dark)
          lv_canvas_draw_img(_canvas, 0, 0, &adThemeDark, &drawDscSimple);
        else
          lv_canvas_draw_img(_canvas, 0, 0, &adThemeLight, &drawDscSimple);
    
        // Add bar object
        lv_canvas_draw_img(_canvas, 16, 224, &adLine, &drawDscSimple);

        // Create counter (s)
        _counter1 = std::shared_ptr<Counter>(new Counter(0));
        _counter2 = std::shared_ptr<Counter>(new Counter(1800));
      }

      // Simulate counters
      if (_counter1)
        _counter1->Tick(tp, pressed, depressed, pos);
      if (_counter2)
        _counter2->Tick(tp, pressed, depressed, pos);

    }
  }
};

LV_IMG_DECLARE(lorcana);

//! Application creator
class LorcanaCreator : public DDAppCreator
{
public:
  //! Implementation of DDAppCreator
  virtual std::string Name() const {return "Lorcana";}
  //! Implementation of DDAppCreator
  virtual const lv_img_dsc_t* Icon() const {return &lorcana;}
  //! Implementation of DDAppCreator
  virtual std::shared_ptr<DDApp> Create(lv_obj_t* background) {return std::make_shared<AppLorcana>(background);}
};

//! Creator instance
LorcanaCreator lc;

//! Automated application registering
//#define REGISTRY(appName)
