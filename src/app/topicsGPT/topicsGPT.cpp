// Copyright (c) 2023 Vladimir Nejedly, vladimir.nejedly@gmail.com
// Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license
// https://creativecommons.org/licenses/by-nc-sa/4.0/
// This software is provided "as is", without any warranty or condition.

#include <memory>
#include "Arduino.h"
#include "lvgl.h"
#include "..\..\menu\ddApp.h"
#include "..\..\lib\Common.h"
#include "..\..\lib\Math.h"
#include "stdlib.h"

// Source: ChatGPT
static const char * Topics[] =
{
"How do you define happiness in your life?",
"If you could have dinner with any historical figure, who would it be and why?",
"In what ways do you strive to make a positive impact on the world?",
"What does true friendship mean to you?",
"How do you balance work, life, and personal pursuits?",
"If you could change one thing about the world, what would it be?",
"What motivates you to get out of bed in the morning?",
"What are your core values, and how do they guide your decisions?",
"If you could offer one piece of advice to your younger self, what would it be?",
"What are the most important qualities in a romantic partner for you?",
"How do you approach decision-making in your life?",
"What is your greatest fear, and how do you overcome it?",
"How do you prioritize self-care in your daily routine?",
"What do you think is the secret to maintaining long-lasting relationships?",
"What do you believe is the key to effective communication?",
"How do you approach personal and professional development?",
"What is your philosophy on dealing with criticism?",
"How do you foster a sense of curiosity and lifelong learning?",
"What legacy do you hope to leave behind?",
"What are the most important lessons you've learned from your failures?",
"What qualities do you admire most in the people you surround yourself with?",
"How do you define personal success and fulfillment?",
"What is the most courageous decision you've ever made?",
"How do you approach finding a sense of purpose in your work?",
"How do you prioritize and manage your time effectively?",
"How do you maintain a positive mindset in challenging times?",
"What do you believe is the key to building trust in relationships?",
"How do you foster a sense of belonging in your community?",
"How do you handle disagreements and conflicts in your relationships?",
"How do you stay grounded and connected to your values in a fast-paced world?",
"What is the most meaningful compliment you've ever received?",
"How do you approach decision-making when faced with uncertainty?",
"In what ways do you contribute to the well-being of the planet?",
"What role does laughter play in your life, and how do you find joy?",
"How do you approach setting and achieving meaningful goals?",
"What are the most important qualities you look for in a mentor?",
"What impact do you hope to have on the world around you?",
"How do you define success in your relationships?",
"How do you navigate the tension between ambition and contentment?",
"What are the most important values you want to pass on to future generations?",
"How do you approach finding a sense of purpose in your hobbies and passions?",
"What is the most meaningful gift you've ever received?",
"What is your philosophy on giving and receiving constructive feedback?",
"How do you define a healthy and fulfilling work environment?",
"How do you approach decision-making when faced with moral dilemmas?",
"What are your favorite ways to recharge and find inspiration?",
"What does it mean to be a good friend, and how do you nurture friendships?",
"How do you approach finding meaning and fulfillment in your career?",
"Do you prefer watching movies at home or at the theater?",
"Who's your favorite actor/actress and why?",
"What's your favorite type of music to listen to when you need to relax?",
"Have you ever been to a live concert or music festival? Which one was your favorite?",
"Do you prefer reading the book or watching the movie adaptation?",
"Have you ever met a celebrity? Who was it and what was it like?",
"What's the most memorable live performance you've ever seen?",
"If you could only listen to one artist for the rest of your life, who would it be?",
"What's your favorite type of game to play? Board games, video games, or something else?",
"Have you ever attended a sporting event in person? Which one and did you enjoy it?",
"Do you enjoy going to museums or art galleries? Which one is your favorite?",
"What's the last book you read that you couldn't put down?",
"Do you have a favorite comedian or type of comedy that always makes you laugh?",
"What's the last theater production you saw? Did you enjoy it?",
"Have you ever been to a themed restaurant? What was the theme and did you like it?",
"Do you prefer watching series on streaming platforms or on regular TV?",
"What's your favorite musical? Have you ever seen it live?",
"Do you enjoy going to amusement parks or water parks? Which one is your favorite?",
"What's the most memorable concert you've ever been to?",
"Have you ever participated in a talent show or open mic night? How did it go?",
"What's your favorite podcast to listen to? Why do you like it?",
"Have you ever taken part in an escape room game? Was it challenging or easy?",
"What's your favorite type of dance to watch or do?",
"Do you enjoy attending film festivals? Which one is your favorite?",
"What's the last musical performance you saw? Did you enjoy it?",
"Do you prefer classic movies or modern ones?",
"What's your favorite video game of all time? Why do you like it so much?",
"Do you enjoy attending stand-up comedy shows? Who's your favorite comedian?",
"What's your favorite type of art to appreciate? Painting, sculpture, or something else?",
"Do you enjoy attending comic book conventions or cosplay events?",
"What's the last album you listened to from start to finish? Did you enjoy it?",
"Do you have a favorite YouTube channel or content creator?",
"What's your favorite musical instrument? Can you play it?",
"What's the most unusual thing you've ever eaten?",
"Do you enjoy attending poetry readings or spoken word events?",
"Do you have a favorite TV show theme song?",
"Who's your favorite celebrity and why?",
"If you could have any role in a movie or TV show, what would it be?",
"Do you play any musical instruments or enjoy singing?",
"What's the last concert or music festival you attended?",
"Have you read any good books or seen any good plays lately?",
"What's the most interesting place you've ever visited?",
"What's your favorite type of vacation?",
"Do you prefer traveling alone or with a group?",
"Have you ever traveled internationally? If so, where did you go?",
"What's your favorite travel destination?",
"Do you prefer to stay in hotels or vacation rentals?",
"Have you ever been on a cruise? If so, where did you go?",
"What's your favorite road trip you've ever taken?",
"What's the most beautiful natural wonder you've ever seen?",
"Do you like to try new foods while traveling?",
"What's the scariest travel experience you've ever had?",
"Do you prefer to travel by plane, train, or car?",
"What's the longest distance you've ever traveled?",
"What's your favorite travel souvenir you've ever purchased?",
"Do you prefer to travel to urban or rural areas?",
"Have you ever visited a place that exceeded your expectations?",
"What's the most adventurous thing you've ever done while traveling?",
"What's the most relaxing vacation you've ever had?",
"Do you prefer to plan every detail of your trip or go with the flow?",
"What's the most underrated travel destination you've ever been to?",
"Have you ever had a travel mishap? What happened?",
"Do you collect anything while traveling?",
"What's the most interesting cultural experience you've ever had while traveling?",
"What's the most unique hotel you've ever stayed in?",
"Have you ever traveled for work? Where did you go?",
"Do you have any travel hacks or tips you can share?",
"What's the most stunning architecture you've ever seen while traveling?",
"What's your favorite travel-related book or movie?",
"What's the most beautiful beach you've ever been to?",
"Do you prefer to travel domestically or internationally?",
"What's the most memorable wildlife experience you've had while traveling?",
"Have you ever traveled to a place that was completely different from your expectations?",
"Do you prefer to travel to places with a rich history or modern attractions?",
"What's the most unique mode of transportation you've ever used while traveling?",
"What's the most breathtaking view you've ever seen while traveling?",
"Do you prefer to travel to warm or cold destinations?",
"What's the most amazing cultural monument you've ever seen while traveling?",
"What's the most adventurous meal you've ever tried while traveling?",
"Do you prefer to travel solo or with a partner/family?",
"What's the most unusual activity you've ever done while traveling?",
"What's the most interesting local tradition you've encountered while traveling?",
"Do you prefer to travel to places off the beaten path or popular tourist destinations?",
"What's the most beautiful park or garden you've ever visited while traveling?",
"What's the most amazing museum or gallery you've ever visited while traveling?",
"Do you prefer to travel to places with a bustling nightlife or quiet atmosphere?",
"What's the most unusual accommodation you've ever stayed in while traveling?",
"Have you ever been lost while traveling? How did you handle it?",
"What's the most beautiful place you've ever visited?",
"What's your favorite hobby or interest and how did you get started?",
"Do you have any creative hobbies like painting or writing?",
"Do you prefer indoor or outdoor activities for your hobbies?",
"Have you ever tried a new hobby and ended up loving it?",
"What's the most challenging hobby you've ever tried?",
"Do you collect anything interesting or unique?",
"What's your favorite genre of music and why?",
"Do you play any musical instruments?",
"Have you ever been to a concert or music festival? Which one was your favorite?",
"What's your favorite type of cuisine to cook or bake?",
"Do you enjoy watching or playing sports?",
"Have you ever tried an extreme sport like skydiving or bungee jumping?",
"Do you enjoy photography or videography as a hobby?",
"Have you ever participated in a dance competition or taken dance lessons?",
"Do you enjoy playing video games or board games?",
"Do you have any hobbies related to fitness or exercise?",
"Have you ever participated in a marathon or other endurance event?",
"What's your favorite type of art to admire or create?",
"Have you ever tried any DIY projects or home improvement tasks?",
"Do you enjoy reading? What's your favorite book or genre?",
"What's the most unusual hobby or interest you've ever heard of?",
"Do you enjoy gardening or caring for plants?",
"Do you have any hobbies related to technology or coding?",
"Have you ever built anything interesting or unique with your hands?",
"Have you ever done any acting or performing on stage?",
"What's your favorite type of fashion or style to wear?",
"Do you enjoy collecting vintage or antique items?",
"Have you ever taken a language class or learned a new language on your own?",
"Do you enjoy meditation or other mindfulness practices?",
"What's your favorite type of animal and do you have any related hobbies?",
"What's your favorite type of board game or card game to play?",
"What's your favorite type of puzzle to solve, like Sudoku or crossword?",
"Do you enjoy attending or participating in wine tastings or other beverage events?",
"What's a hobby or interest that you've always wanted to try but haven't had the chance to yet?",
"What inspired you to pursue your current career path?",
"What do you think are the most important qualities for success in your industry?",
"Have you ever made a career change? If so, what prompted it?",
"What's the most challenging project you've worked on, and what did you learn from it?",
"What's the most rewarding aspect of your job?",
"Do you prefer working independently or in a team?",
"What's the most innovative idea you've seen come out of your industry in recent years?",
"What's your biggest strength in the workplace?",
"What's the best career advice you've ever received?",
"What are your long-term career goals?",
"What's the most exciting project you've ever worked on?",
"What's the biggest challenge facing your industry right now?",
"What do you think is the key to a successful team?",
"What's the most important thing you look for in a manager or supervisor?",
"What's the most important thing you've learned about communication in the workplace?",
"What is your dream job, and what steps are you taking to make it a reality?",
"What are some of the biggest challenges you face in your job?",
"What are your favorite productivity tools or techniques?",
"What do you think sets successful people apart in your field?",
"What are some of the most rewarding aspects of your job?",
"What was the best piece of advice your parents ever gave you?",
"What was the most memorable moment from your childhood?",
"How do you balance your personal life and your family life?",
"What qualities do you think are important in a romantic partner?",
"What's the best way to handle a break-up?",
"What's the most important quality in a friendship?",
"What's the most important quality in a sibling?",
"What's the most important quality in a step-parent?",
"Do you believe in the idea of 'opposites attract' or do you think it's better to have similar interests and personalities in a relationship?",
"What's your favorite app on your phone, and why?",
"Do you think that technology has made our lives easier or more complicated?",
"What do you think is the next big thing in technology?",
"Have you ever had a bad experience with technology? What happened?",
"What's your favorite gadget, and why?",
"What do you think is the biggest challenge facing the tech industry today?",
"Do you think that robots will eventually take over most jobs?",
"What's your opinion on artificial intelligence?",
"What do you think is the most important invention in the past decade?",
"Do you think that social media has more positive or negative effects on society?",
"What's your favorite video game, and why?",
"Do you think that virtual reality will become a popular form of entertainment?",
"Have you ever experienced a technology blackout? How did you handle it?",
"What's your opinion on self-driving cars?",
"Do you think that technology is making us lazier?",
"What do you think is the most overrated piece of technology?",
"Do you think that technology is improving education?",
"What's your favorite streaming service, and why?",
"Do you think that technology is making us more or less creative?",
"What do you think is the most important technological advancement in the past 50 years?",
"Do you think that technology is improving healthcare?",
"What do you think is the best thing about the internet?",
"Do you think that technology is making us more or less intelligent?",
"What's your favorite piece of technology from your childhood?",
"What's your opinion on cryptocurrency?",
"What do you think is the most useful social media platform?",
"Do you think that technology is improving the environment?",
"What's your opinion on 3D printing?",
"What do you think is the most important thing to consider when buying a new TV?",
"Do you think that technology is improving transportation?",
"Do you think that technology is improving democracy?",
"What's your opinion on the use of technology in the workplace?",
"What's your favorite cuisine?",
"Do you prefer cooking or eating out?",
"What's the best meal you've ever had?",
"Have you ever tried any exotic foods?",
"Do you like spicy food?",
"What's your favorite type of dessert?",
"Do you like to experiment with new recipes?",
"Do you have any favorite local restaurants?",
"What's your favorite type of pizza topping?",
"What's your favorite type of cuisine to eat for breakfast?",
"Do you have any favorite food-related traditions?",
"Do you like to drink coffee or tea?",
"What's your favorite type of cuisine for a picnic?",
"Do you have any favorite food-related TV shows or movies?",
"What's your favorite type of alcohol?",
"Do you like to try different types of beer or wine?",
"What's the most unique or unusual food you've ever tasted?",
"Have you ever tried a new restaurant solely based on online reviews or recommendations?",
"Do you enjoy cooking or baking, and if so, what's your signature dish?",
"What's the best meal you've ever had while traveling?",
"Do you like trying new and exotic foods or do you stick to what you know and like?",
"What's your favorite type of drink, alcoholic or non-alcoholic?",
"Do you have a favorite type of wine or beer, and why do you like it?",
"Do you prefer coffee or tea, and how do you like to have it prepared?",
"Have you ever tried brewing your own beer or making your own wine?",
"What's your favorite type of cuisine to cook at home?",
"What's your opinion on organic or locally sourced food?",
"What's your favorite fast food restaurant or guilty pleasure food?",
"What's your favorite holiday or special occasion food?",
"Have you ever attended a food festival or culinary event?",
"If you could only eat one type of cuisine for the rest of your life, what would it be?",
"What was the most challenging experience you've ever faced?",
"What's the craziest thing you've ever done?",
"Have you ever had a near-death experience?",
"What's the most memorable trip you've ever taken?",
"What's a skill you've always wanted to learn?",
"Have you ever experienced culture shock?",
"What was your first job, and what did you learn from it?",
"What's a dream you've always had but haven't pursued yet?",
"Have you ever had a life-changing event that altered your perspective on something?",
"What's a moment in your life that you wish you could relive?",
"What's a childhood memory that still brings a smile to your face?",
"Have you ever been lost in a foreign country?",
"What's the best gift you've ever received?",
"What's the most beautiful place you've ever been to?",
"Have you ever had a run-in with the law?",
"What's a decision you've made that you regret?",
"What's the biggest risk you've ever taken?",
"What's a funny story from your childhood?",
"Have you ever overcome a fear, and how did you do it?",
"What's the most beautiful thing you've ever witnessed?",
"What's a time you had to stand up for yourself or someone else?",
"What's a song or album that holds a special meaning to you?",
"Have you ever had to start over from scratch?",
"What's the most memorable party or event you've attended?",
"What's the most significant change you've made in your life?",
"What's a talent or skill you wish you had?",
"What's a trait you admire in other people?",
"What's a place you've always wanted to visit?",
"What was the biggest challenge you faced when becoming a parent?",
"What is one thing you wish you knew before becoming a parent?",
"What is the most rewarding thing about being a parent?",
"What is your proudest moment as a parent?",
"What is your favorite activity to do with your child?",
"What is one parenting tip you would give to new parents?",
"What is your favorite thing about being a parent?",
"What is your favorite family tradition?",
"What is one thing you hope your child learns from you?",
"What is your biggest fear as a parent?",
"What is one thing you want to teach your child about the world?",
"What is the funniest parenting moment you've experienced?",
"How do you deal with your child's milestones and growing up?",
"What is your favorite thing to do for your child's birthday?",
"What is the most important lesson you want your child to learn?",
"What is one way you involve your child in making decisions for the family?",
"How do you help your child develop independence?",
"What is your favorite way to spend quality time with your child?",
"How do you celebrate achievements and milestones with your child?",
"What is the most rewarding thing about raising a child?",
"What is one way you encourage creativity and imagination in your child?",
"What is your biggest hope for your child's future?",
"What are some of your favorite family traditions?",
"What do you think is the biggest challenge facing young people today?",
"What advice would you give to your younger self?",
"What was your favorite childhood memory?",
"What are your aspirations for the future?",
"What role do you think youth can play in bringing about positive change in the world?",
"What's the most exciting thing happening in your life right now?",
"What do you think is the most important skill for young people to have?",
"What are some things you would like to see changed in the world?",
"What do you think is the biggest difference between your generation and your parents' generation?",
"What are some of the biggest changes you've seen in the world during your lifetime?",
"What do you think is the most important factor in a young person's success?",
"What are some of the things you're most grateful for in your life?",
"What was your favorite subject in school, and why?",
"If you could choose any career, what would it be and why?",
"What are some popular trends among young people that you find interesting?",
"What is something that you're looking forward to in the next few months?",
"What do you think is the most important issue facing young people in your community?",
"What are some things you and your friends like to do for fun?",
"What is your favorite weekend destination?",
"What is your favourite movie?",
"Do you enjoy watching movies or reading books more than once?",
"What is your favourite TV show?",
};

LV_IMG_DECLARE(stepBack);

//! Topic text width will be aligned to this value, rest of the text will go to next line
const int TopicTextWidth = 400;

const Vector2I BackBtnPos(0, 170);
const int BackBtnRadius = 96;

//! TopicsGPT implementation
class TopicsGPT
{
  lv_style_t _styleTextBig;
  lv_obj_t* _stepBack;
  lv_obj_t* _labelTopic;
  //! Timer
  unsigned long _timer = 0;
  //! Topic index
  std::vector<int> _topicIndex; // Topic indices, last one is the newest one
  int _selectedTopicIndex;
  bool _shown = true;

public:
 
  //! Constructor
  TopicsGPT()
  {
    // Step back button
    _stepBack = lv_img_create(lv_scr_act());
    lv_img_set_src(_stepBack, &stepBack);
    lv_obj_align(_stepBack, LV_ALIGN_CENTER, BackBtnPos.x, BackBtnPos.y);
    lv_img_set_antialias(_stepBack, false);

    // Big text style initialization
    lv_style_init(&_styleTextBig);
    lv_style_set_text_font(&_styleTextBig, &lv_font_montserrat_32);

    // Label representing the topic, aligned
    _labelTopic = lv_label_create(lv_scr_act());
    lv_obj_add_style(_labelTopic, &_styleTextBig, 0);
    lv_label_set_long_mode(_labelTopic, LV_LABEL_LONG_WRAP);
    lv_obj_align(_labelTopic, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_width(_labelTopic, TopicTextWidth);
    lv_obj_set_style_text_align(_labelTopic, LV_TEXT_ALIGN_CENTER, 0);

    srand(int(millis() % INT_MAX));

    int topicsCount = lenof(Topics);
    
    _topicIndex.push_back(rand() % topicsCount);
    _selectedTopicIndex = -1; // Make sure topic is selected next round
  }

  //! Destructor
  ~TopicsGPT()
  {
    lv_obj_del(_labelTopic);
    lv_obj_del(_stepBack);
  }

  //! Simulation step, invoked every rendering frame
  void Tick(int tp, bool pressed, const Vector2I& pos)
  {
    // Either step back or move to next topic (extend the timer)
    if (pressed)
    {
      // Position in centric coordinates
      Vector2I cPos = pos - Vector2I(240, 240);

      // Detect back button press, assume it is step forward otherwise
      if (BackBtnPos.Distance2(cPos) < Square(BackBtnRadius))
      {
        if (_topicIndex.size() > 1)
        {
          //_timer = millis() + 300;
          _topicIndex.pop_back();
        }  
      }
      else
      {
        _timer = millis() + 300;

        srand(int(millis() % INT_MAX));

        int topicsCount = lenof(Topics);

        // Select random topic
        _topicIndex.push_back(rand() % topicsCount);
      }
    }

    // Show or hide the step back button
    if (_topicIndex.size() > 1)
    {
      if (lv_obj_has_flag(_stepBack, LV_OBJ_FLAG_HIDDEN))
        lv_obj_clear_flag(_stepBack, LV_OBJ_FLAG_HIDDEN);
    }
    else
    {
      if (!lv_obj_has_flag(_stepBack, LV_OBJ_FLAG_HIDDEN))
        lv_obj_add_flag(_stepBack, LV_OBJ_FLAG_HIDDEN);
    }

    // Draw the dice or decrease the timer and hide the dice
    if (_timer <= millis())
    {
      // Show the topic
      if (!_shown)
      {
        lv_obj_clear_flag(_labelTopic, LV_OBJ_FLAG_HIDDEN);
        _shown = true;
      }
    }
    else
    {
      // Hide the topic
      if (_shown)
      {
        lv_obj_add_flag(_labelTopic, LV_OBJ_FLAG_HIDDEN);
        _shown = false;
      }
    }

    // Update topic
    if (_topicIndex.back() != _selectedTopicIndex)
    {
      // Get text to display
      const char* topicsText = Topics[_topicIndex.back()];

      // Set text to label
      lv_label_set_text(_labelTopic, topicsText);

      // Align text vertically
      lv_point_t size;
      lv_txt_get_size(&size, topicsText, &lv_font_montserrat_32, 0, 0, TopicTextWidth, LV_TEXT_FLAG_NONE);
      lv_obj_set_height(_labelTopic, size.y);

      // Remember selected topic ID
      _selectedTopicIndex = _topicIndex.back();
    }
  }
};


class AppTopicsGPT : public DDApp
{
  //! Topics rendering
  std::shared_ptr<TopicsGPT> _topicsGPT;


public:
  //! Constructor
  AppTopicsGPT(lv_obj_t* background) : DDApp(background)
  {
    // Hide background
    lv_obj_add_flag(_background, LV_OBJ_FLAG_HIDDEN);

    // Create the Topics
    _topicsGPT = std::shared_ptr<TopicsGPT>(new TopicsGPT());    
  }

  //! Destructor
  ~AppTopicsGPT()
  {
    // Show background
    lv_obj_clear_flag(_background, LV_OBJ_FLAG_HIDDEN);
  }

  //! Simulation step, invoked every rendering frame
  virtual void Tick(int tp, bool pressed, bool depressed, const Vector2I& pos)
  {
    // Process Topics
    _topicsGPT->Tick(tp, pressed, pos);
  }
};

LV_IMG_DECLARE(topicsGPT);

//! Application creator
class TopicsGPTCreator : public DDAppCreator
{
public:
  //! Implementation of DDAppCreator
  virtual std::string Name() const {return "TopicsGPT";}
  //! Implementation of DDAppCreator
  virtual const lv_img_dsc_t* Icon() const {return &topicsGPT;}
  //! Implementation of DDAppCreator
  virtual std::shared_ptr<DDApp> Create(lv_obj_t* background) {return std::make_shared<AppTopicsGPT>(background);}
};

//! Creator instance
TopicsGPTCreator tcgpt;