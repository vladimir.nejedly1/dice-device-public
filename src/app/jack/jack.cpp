// Copyright (c) 2023 Vladimir Nejedly, vladimir.nejedly@gmail.com
// Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license
// https://creativecommons.org/licenses/by-nc-sa/4.0/
// This software is provided "as is", without any warranty or condition.

#include <memory>
#include <random>
#include "Arduino.h"
#include "lvgl.h"
#include "..\..\menu\pin_config.h"
#include "..\..\menu\ddApp.h"
#include "..\..\lib\Vector2.h"

//! Parameters configured by the menu
struct ParametersJack
{
  //! Status of application - if it has started or is still in the menu
  bool _started = false;
  //! Number of players for indicator, 1 means there is no indicator
  int _playerCount = 1;
};

//! Menu dialog
class MenuJack
{
  //! Parameters stored in the application
  ParametersJack& _parameters;

  //!{ Styles
  lv_style_t _styleShadow;
  lv_style_t _styleTextMedium;
  lv_style_t _styleTextBig;
  lv_style_t _styleSelection;
  lv_style_t _styleSelectionItems;
  //!}

  //!{ Widgets
  lv_obj_t* _btnGo;
  lv_obj_t* _btnPlayerCount;
  //!}

  //!{ Events
  static void eventGo(lv_event_t* e)
  {
    ParametersJack* parameters = (ParametersJack*) e->user_data;
    lv_event_code_t code = lv_event_get_code(e);
    if (code == LV_EVENT_CLICKED)
    {
      parameters->_started = true;
    }
  }
  static void eventPlayerCount(lv_event_t* e)
  {
    ParametersJack* parameters = (ParametersJack*) e->user_data;
    lv_obj_t* obj = lv_event_get_target(e);
    uint32_t id = lv_btnmatrix_get_selected_btn(obj);
    parameters->_playerCount = id + 1;
  }
  //!}

public:

  //! Constructor
  MenuJack(ParametersJack& parameters) : _parameters(parameters)
  {
    // Shadow style initialization
    lv_style_init(&_styleShadow);
    lv_style_set_shadow_width(&_styleShadow, 50);
    lv_style_set_shadow_color(&_styleShadow, LV_COLOR_MAKE(0, 0, 0));
    lv_style_set_border_opa(&_styleShadow, LV_OPA_COVER);

    // Medium text style initialization
    lv_style_init(&_styleTextMedium);
    lv_style_set_text_font(&_styleTextMedium, &lv_font_montserrat_20);

    // Big text style initialization
    lv_style_init(&_styleTextBig);
    lv_style_set_text_font(&_styleTextBig, &lv_font_montserrat_32);

    // Selection, to be used by button matrix object
    lv_style_init(&_styleSelection);
    lv_style_set_pad_all(&_styleSelection, 0);
    lv_style_set_pad_gap(&_styleSelection, 0);
    lv_style_set_clip_corner(&_styleSelection, true);
    lv_style_set_border_width(&_styleSelection, 0);

    // Selection items, to be used by button matrix object for items
    lv_style_init(&_styleSelectionItems);
    lv_style_set_border_side(&_styleSelectionItems, LV_BORDER_SIDE_INTERNAL);
    lv_style_set_border_width(&_styleSelectionItems, 0);
    lv_style_set_radius(&_styleSelectionItems, 0);

    // Go button
    _btnGo = lv_btn_create(lv_scr_act());
    lv_obj_add_style(_btnGo, &_styleShadow, 0);
    lv_obj_add_style(_btnGo, &_styleTextBig, 0);
    lv_obj_align(_btnGo, LV_ALIGN_CENTER, 0, 180);
    lv_obj_set_ext_click_area(_btnGo, 50);
    lv_obj_t* labelGo = lv_label_create(_btnGo);
    lv_label_set_text(labelGo, "Go!");
    lv_obj_add_event_cb(_btnGo, eventGo, LV_EVENT_ALL, &_parameters);

    // Player count
    _btnPlayerCount = lv_btnmatrix_create(lv_scr_act());
    lv_obj_add_style(_btnPlayerCount, &_styleShadow, 0);
    lv_obj_add_style(_btnPlayerCount, &_styleTextMedium, 0);
    lv_obj_add_style(_btnPlayerCount, &_styleSelection, 0);
    lv_obj_add_style(_btnPlayerCount, &_styleSelectionItems, LV_PART_ITEMS);
    lv_obj_align(_btnPlayerCount, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_size(_btnPlayerCount, 204, 250);
    static const char * mapPlayerCount[] = {"No Indicator", "\n", "2 Players", "\n", "3 Players", "\n", "4 Players", "\n", "5 Players", "\n", "6 Players", ""};
    lv_btnmatrix_set_map(_btnPlayerCount, mapPlayerCount);
    lv_btnmatrix_set_btn_ctrl_all(_btnPlayerCount, LV_BTNMATRIX_CTRL_CHECKABLE);
    lv_btnmatrix_set_one_checked(_btnPlayerCount, true);
    lv_btnmatrix_set_btn_ctrl(_btnPlayerCount, 0, LV_BTNMATRIX_CTRL_CHECKED);
    lv_obj_add_event_cb(_btnPlayerCount, eventPlayerCount, LV_EVENT_VALUE_CHANGED, &_parameters);
  }

  //! Destructor
  ~MenuJack()
  {
    lv_obj_del(_btnPlayerCount);
    lv_obj_del(_btnGo);
  }
};

LV_IMG_DECLARE(compassArrow);
LV_IMG_DECLARE(compassBack);
LV_IMG_DECLARE(compassDisc);

//! Touch record
struct TouchRecord
{
  //! Touched point on screen
  Vector2I point;
  //! Time of the record
  uint32_t time;
};

//! Jack implementation
class Jack
{
  //! Maximum friction
  static constexpr float _frictionMax = 0.98f;
  //! Minimum friction
  static constexpr float _frictionMin = 0.99f;

  //! Buffer associated with canvas
  lv_color_t* _canvasBuffer;
  //! Canvas object
  lv_obj_t* _canvas;
  //! Arrow objects
  lv_obj_t* _arrow;
  //! Current rotation angle
  float _rotAngle;
  //! Current velocity
  float _rotVelocity;
  //! Current friction
  float _friction;
  //! History of last 2 touches, velocity is calculated from it
  TouchRecord _touchHistory[2];
  //! Number of players for indicator drawing, 1 means there is no indicator
  int _playerCount;
  //! Random generator
  std::default_random_engine _generator;
  //! Random distribution
  std::uniform_real_distribution<float> _distribution;

  //! Zero the touch history - prepare for capturing new history
  void InvalidateTouchHistory()
  {
    _touchHistory[0].point = Vector2I(0, 0);
    _touchHistory[0].time = 0;
    _touchHistory[1].point = Vector2I(0, 0);
    _touchHistory[1].time = 0;   
  }

public:

  //! Constructor
  Jack(int playerCount) :
    _rotAngle(0.0f),
    _rotVelocity(0.0f),
    _friction(Jack::_frictionMax),
    _playerCount(playerCount),
    _generator(std::random_device()()),
    _distribution(Jack::_frictionMin, Jack::_frictionMax)
  {
    // Initialize touch history
    InvalidateTouchHistory();

    // Canvas representing background
    _canvasBuffer = (lv_color_t *)heap_caps_malloc(EXAMPLE_LCD_H_RES * EXAMPLE_LCD_V_RES * sizeof(lv_color_t), MALLOC_CAP_SPIRAM);
    _canvas = lv_canvas_create(lv_scr_act());
    lv_canvas_set_buffer(_canvas, _canvasBuffer, EXAMPLE_LCD_H_RES, EXAMPLE_LCD_V_RES, LV_IMG_CF_TRUE_COLOR);

    // Simple draw descriptor
    lv_draw_img_dsc_t drawDscSimple;
    lv_draw_img_dsc_init(&drawDscSimple);

    // Draw compas border on canvas
    lv_canvas_draw_img(_canvas, 0, 0, &compassBack, &drawDscSimple);

    // Draw player identification colors
    {
      // Use multiply blending mode
      lv_draw_rect_dsc_t rect_dsc;
      lv_draw_rect_dsc_init(&rect_dsc);
      rect_dsc.blend_mode = LV_BLEND_MODE_MULTIPLY;

      // Colors to be used on border
      lv_color_t playerColor[] =
      {
        LV_COLOR_MAKE(255, 255, 255),
        LV_COLOR_MAKE(93, 165, 255),
        LV_COLOR_MAKE(197, 145, 203)
      };

      // In case of 2 players it is special, it is split to 2 halves
      if (_playerCount == 2)
      {
        rect_dsc.bg_color = playerColor[1];
        lv_point_t points[4];
        points[0].x = 240;
        points[0].y = 0;
        points[1].x = 480;
        points[1].y = 0;
        points[2].x = 480;
        points[2].y = 480;
        points[3].x = 240;
        points[3].y = 480;
        lv_canvas_draw_polygon(_canvas, points, 4, &rect_dsc);
      }

      // In case of 3 or more players draw pie triangle for each one. Note the following code is valid for 1 player (no indicator) too
      else
      {
        for (int i = 0; i < _playerCount; i++)
        {
          // Color of the pie. In case of even number of players just iterate between first 2, otherwise 3
          lv_color_t color;
          if (_playerCount % 2 == 0)
              color = playerColor[i % 2];
          else
              color = playerColor[i % 3];

          // Draw pie
          float angleA = 0.25 * Pi * 2 - (float)(i + 0) / _playerCount * 2 * Pi;
          float angleB = 0.25 * Pi * 2 - (float)(i + 1) / _playerCount * 2 * Pi;
          int pointAx = cos(angleA) * 3.0f * 240 + 240;
          int pointAy = sin(angleA) * 3.0f * 240 + 240;
          int pointBx = cos(angleB) * 3.0f * 240 + 240;
          int pointBy = sin(angleB) * 3.0f * 240 + 240;

          rect_dsc.bg_color = color;
          lv_point_t points[3];
          points[0].x = 240;
          points[0].y = 240;
          points[1].x = pointAx;
          points[1].y = pointAy;
          points[2].x = pointBx;
          points[2].y = pointBy;

          lv_canvas_draw_polygon(_canvas, points, 3, &rect_dsc);
        }
      }
    }

    // Draw front disc
    lv_canvas_draw_img(_canvas, 0, 0, &compassDisc, &drawDscSimple);

    // Arrow object
    _arrow = lv_img_create(lv_scr_act());
    lv_img_set_src(_arrow, &compassArrow);
    lv_obj_align(_arrow, LV_ALIGN_CENTER, 0, 0);
    lv_img_set_antialias(_arrow, false);
  }

  //! Destructor
  ~Jack()
  {
    lv_obj_del(_arrow);
    lv_obj_del(_canvas);
    heap_caps_free(_canvasBuffer);
  }

  //! Touch detection, invoked in a fast pace
  void Touch(int tp, const Vector2I& pos)
  {
    if (tp > 0)
    {
      // Distinguish between touching border or a central button. In first case update the history and control the arrow,
      // in second case just set the velocity
      const int centralButtonRadius = 100;
      Vector2I cPos = pos - 240;
      if (cPos.Size() > centralButtonRadius)
      {
        // Time at the moment of touch
        uint32_t touchTime = millis();

        // Difference comparing to previous time
        uint32_t touchTimeDelta = touchTime - _touchHistory[1].time;
 
        // Create record in history and update the rotation angle based on previous and new touch
        // Also check there was a non-zero time difference from the previous record (in case of multiple touches it can be zero),
        // otherwise it would lead to zero division later
        if (touchTimeDelta > 0)
        {
          // Move history by 1 item
          _touchHistory[0].point = _touchHistory[1].point;
          _touchHistory[0].time = _touchHistory[1].time;

          // Make new record in history
          _touchHistory[1].point = cPos;
          _touchHistory[1].time = touchTime;

          // If previous touch point make sense, get delta and update rotation angle
          if (_touchHistory[0].point.Size() > 0)
          {
            float angleDelta = _touchHistory[0].point.AngleTo(_touchHistory[1].point);
            _rotAngle = _rotAngle + angleDelta;
          }
        }

        // Zero the velocity when touching
        _rotVelocity = 0.0f;        
      }
      else
      {
        // Set velocity to some constant value
        const float spinVelocityLimit = 30.0f;
        _rotVelocity = spinVelocityLimit;

        // Set friction to random value between min and max
        _friction = _distribution(_generator);

        // User is touching button, set invalid value in history
        InvalidateTouchHistory();     
      }
    }
    else
    {
      // Calculate the velocity from history, only if the first item makes sense (we can be certain the second makes sense too)
      if (_touchHistory[0].point.Size() > 0)
      {
        // Set velocity
        float angleDelta = _touchHistory[0].point.AngleTo(_touchHistory[1].point);
        uint32_t touchTimeDelta = _touchHistory[1].time - _touchHistory[0].time;
        _rotVelocity = angleDelta / (touchTimeDelta / 1000.0f);

        // Set friction to random value between min and max
        _friction = _distribution(_generator);        
      }

      // User is not touching, set invalid value in history
      InvalidateTouchHistory();
    }
  }

  //! Simulation step, invoked every rendering frame
  void Tick(int tp, const Vector2I& pos)
  {
    // Apply velocity on rotation
    float deltaT = 1.0 / 30.0;
    const float velocityLimit = 13.0f;
    float satVelocity = std::max(std::min(_rotVelocity, velocityLimit), -velocityLimit);
    _rotAngle = _rotAngle + satVelocity * deltaT;
    _rotVelocity = _rotVelocity * _friction;

    // Update arrow position and orientation
    lv_obj_set_pos(_arrow, cos(_rotAngle) * 180, sin(_rotAngle) * 180);
    lv_img_set_angle(_arrow, (int)(_rotAngle / (Pi * 2) * 3600 + 900) % 3600);    
  }
};

//! Jack - wheel of fortune application
class AppJack : public DDApp
{
  //! Parameters of the application, set by initial Menu dialog, used by application
  ParametersJack _parameters;
  //! Menu dialog
  std::shared_ptr<MenuJack> _menu;
  //! Jack rendering
  std::shared_ptr<Jack> _jack;

public:
  //! Constructor
  AppJack(lv_obj_t* background) : DDApp(background)
  {
    // Application starts with dialog where user can adjust parameters
    _menu = std::shared_ptr<MenuJack>(new MenuJack(_parameters));
  }

  //! Destructor
  ~AppJack()
  {
    // Show background
    lv_obj_clear_flag(_background, LV_OBJ_FLAG_HIDDEN);
  }

  //! Touch detection, invoked in a fast pace
  virtual void Touch(int tp, const Vector2I& pos)
  {
    if (_jack)
      _jack->Touch(tp, pos);
  }

  //! Simulation step, invoked every rendering frame
  virtual void Tick(int tp, bool pressed, bool depressed, const Vector2I& pos)
  {
    // If application has started (means we are not in the dialog anymore), run
    if (_parameters._started)
    {
      // If dialog still exists, initialize run, destroy the dialog
      if (_menu != nullptr)
      {
        // Destroy the menu
        _menu.reset();

        // Hide background
        lv_obj_add_flag(_background, LV_OBJ_FLAG_HIDDEN);

        // Create the Jack
        _jack = std::shared_ptr<Jack>(new Jack(_parameters._playerCount));
      }

      // Process Jack
      _jack->Tick(tp, pos);
    }
  }
};

LV_IMG_DECLARE(jack);

//! Application creator
class JackCreator : public DDAppCreator
{
public:
  //! Implementation of DDAppCreator
  virtual std::string Name() const {return "Jack";}
  //! Implementation of DDAppCreator
  virtual const lv_img_dsc_t* Icon() const {return &jack;}
  //! Implementation of DDAppCreator
  virtual std::shared_ptr<DDApp> Create(lv_obj_t* background) {return std::make_shared<AppJack>(background);}
};

//! Creator instance
JackCreator jc;