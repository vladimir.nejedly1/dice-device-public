// Copyright (c) 2023 Vladimir Nejedly, vladimir.nejedly@gmail.com
// Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license
// https://creativecommons.org/licenses/by-nc-sa/4.0/
// This software is provided "as is", without any warranty or condition.

#include "Arduino.h"
#include "lvgl.h"
#include "..\..\lib\Common.h"
#include "..\..\lib\CirclePacking.h"
#include "..\..\lib\Color.h"
#include "..\..\menu\pin_config.h"
#include "..\..\menu\ddApp.h"
#include <memory>

//! Dice type
enum DiceType
{
  DT_D4,
  DT_D6,
  DT_D8,
  DT_D10,
  DT_D12,
  DT_D20,
  NDiceType,
};

//! Info needed for dice recreation
struct DiceDesc
{
  int _id;
  DiceType _type;
  bool _nightMode;
};

//! Generic Dice ancestor
class Dice
{
protected:
  //! Extension of dice dimension for collision purposes
  const int _collisionBorder = 8;
  //! ID of dice, used to set a color scheme of the particular dice
  int _id;
  //! Position of dice on screen
  Vector2I _pos;
  //! Diameter or width of dice
  int _size;
  //! Random number, at the beginning set to number of sides
  int _randomNum;
  //! Last selected number, for lazy update
  int _selectedNum;
  //! Night mode
  bool _nightMode;
  //! Number of sides of dice
  virtual int Sides() const {return 0;}
  //! Select number on dice. 0 has a special meaning, it hides the dice
  virtual void SelectNumber(int num) {}
  //! Update position of dice
  virtual void UpdatePos() {}
public:
  //! Constructor
  Dice(int id, const Vector2I& pos, int size, bool nightMode) : _id(id), _pos(pos), _size(size), _selectedNum(-1), _randomNum(-1), _nightMode(nightMode) {}
  //! Destructor
  virtual ~Dice() {}
  //! Return Dice type
  virtual DiceType GetType() const = 0;
  //! Shake the random number
  void Shake() {_randomNum = rand() % Sides() + 1;}
  //! Show dice
  void Show() {SelectNumber(_randomNum);}
  //! Hide dice
  void Hide() {SelectNumber(0);}
  //! Return if sice is currently shown
  bool IsShown() const {return _selectedNum > 0;}
  //! Set new position of dice
  void SetPosAndSize(const Vector2I& pos, int size) {_pos = pos; _size = size; UpdatePos();}
  //! Get position
  Vector2I GetPos() const {return _pos;}
  //! Setting a night mode
  void SetNightMode(bool nightMode) {_nightMode = nightMode; UpdatePos();}
  //! Get the night mode
  bool GetNightMode() const {return _nightMode;}
  //! Check if point is within radius of dice
  bool Intersects(const Vector2I& p) const {int radius = (_size + _collisionBorder * 2) / 2; return _pos.Distance2(p) <= (radius * radius);}
  //! Return dice descriptor
  DiceDesc GetDesc() {DiceDesc dd; dd._id = _id; dd._type = GetType(); dd._nightMode = _nightMode; return dd;}
};

// Color schemes
static lv_color_t ColorScheme[6] =
{
  LV_COLOR_MAKE(229, 121, 39),
  LV_COLOR_MAKE(198, 21, 31),
  LV_COLOR_MAKE(240, 226, 40),
  LV_COLOR_MAKE(32, 156, 191),
  LV_COLOR_MAKE(162, 32, 188),
  LV_COLOR_MAKE(49, 193, 32),
};

const lv_color_t ColorDayText = LV_COLOR_MAKE(220, 220, 220);

//! D6 dice
class D6 : public Dice
{
protected:
  //! Background square
  lv_obj_t* _background;
  //! 7 LED's with the following layout
  //! 0   3
  //! 1 6 4
  //! 2   5
  lv_obj_t* _led[7];
  //! Implementation of Dice
  virtual int Sides() const {return 6;}
  //! Implementation of Dice
  virtual void SelectNumber(int num)
  {
    // Finish in case we already have the number selected
    if (_selectedNum == num)
      return;

    // Symbol selection
    const int symbol[7][7] =
    {
      {0, 0, 0, 0, 0, 0, 0}, // 0 is used to switch the dice off
      {0, 0, 0, 0, 0, 0, 1},
      {1, 0, 0, 0, 0, 1, 0},
      {1, 0, 0, 0, 0, 1, 1},
      {1, 0, 1, 1, 0, 1, 0},
      {1, 0, 1, 1, 0, 1, 1},
      {1, 1, 1, 1, 1, 1, 0}
    };

    // Hide or unhide the individual dots
    for (int i = 0; i < 7; i++)
    {
      if (symbol[num][i])
        lv_obj_clear_flag(_led[i], LV_OBJ_FLAG_HIDDEN);
      else
        lv_obj_add_flag(_led[i], LV_OBJ_FLAG_HIDDEN);
    }

    // Hide or unhide background
    if (num > 0)
      lv_obj_clear_flag(_background, LV_OBJ_FLAG_HIDDEN);
    else
      lv_obj_add_flag(_background, LV_OBJ_FLAG_HIDDEN);    
      
    // The number is selected now
    _selectedNum = num;
  }
  //! Implementation of Dice
  virtual void UpdatePos()
  {
    // Size of inscribed cube
    int cubeSize = (int)sqrt(_size * _size * 0.5f);
    cubeSize += cubeSize / 10 / 2; // Round corners make possible to fit there bigger cube

    // Calculate layout dimensions based on size
    const int dotDistance = (int)(cubeSize / 3.5f);
    const int dotRadius = cubeSize / 10;

    lv_obj_set_size(_background, cubeSize, cubeSize);
    lv_obj_set_style_radius(_background, dotRadius, 0);

    // Multiply color
    lv_color32_t tColor32;
    lv_color32_t sColor32;
    if (_nightMode == false)
    {
      tColor32.full = lv_color_to32(LV_COLOR_MAKE(170, 170, 170));
      sColor32.full = lv_color_to32(ColorScheme[_id]);
    }
    else
    {
      tColor32.full = lv_color_to32(LV_COLOR_MAKE(112, 112, 112));
      sColor32.full = lv_color_to32(LV_COLOR_MAKE(64, 64, 64));
    }
    lv_color_t newColor = LV_COLOR_MAKE(
        LV_UDIV255(((uint16_t)tColor32.ch.red   * sColor32.ch.red)   ),
        LV_UDIV255(((uint16_t)tColor32.ch.green * sColor32.ch.green) ),
        LV_UDIV255(((uint16_t)tColor32.ch.blue  * sColor32.ch.blue)  ));
    lv_obj_set_style_bg_color(_background, newColor, 0);

    lv_obj_set_pos(_background, _pos.x, _pos.y);

    for (int i = 0; i < 7; i++)
    {
      lv_obj_set_size(_led[i], dotRadius * 2, dotRadius * 2);
      lv_obj_set_style_radius(_led[i], dotRadius, 0);
      //lv_obj_set_style_bg_color(_led[i], colorScheme[id], 0);
      lv_obj_set_style_border_width(_led[i], dotRadius, 0); // Draw dots as borders, to prevent artifacts when drawing without border
      if (_nightMode)
        lv_obj_set_style_border_color(_led[i], ColorScheme[_id], 0);
      else
        lv_obj_set_style_border_color(_led[i], ColorDayText, 0);
    }

    lv_obj_align(_led[0], LV_ALIGN_CENTER, _pos.x + dotDistance * -1, _pos.y + dotDistance * -1);
    lv_obj_align(_led[1], LV_ALIGN_CENTER, _pos.x + dotDistance * -1, _pos.y + dotDistance *  0);
    lv_obj_align(_led[2], LV_ALIGN_CENTER, _pos.x + dotDistance * -1, _pos.y + dotDistance *  1);
    lv_obj_align(_led[3], LV_ALIGN_CENTER, _pos.x + dotDistance *  1, _pos.y + dotDistance * -1);
    lv_obj_align(_led[4], LV_ALIGN_CENTER, _pos.x + dotDistance *  1, _pos.y + dotDistance *  0);
    lv_obj_align(_led[5], LV_ALIGN_CENTER, _pos.x + dotDistance *  1, _pos.y + dotDistance *  1);
    lv_obj_align(_led[6], LV_ALIGN_CENTER, _pos.x + dotDistance *  0, _pos.y + dotDistance *  0);
  }
public:
  //! Constructor
  D6(int id, const Vector2I& pos, int size, bool nightMode) : Dice(id, pos, size, nightMode)
  {
    // Size of inscribed cube
    int cubeSize = (int)sqrt(size * size * 0.5f);
    cubeSize += cubeSize / 10 / 2; // Round corners make possible to fit there bigger cube

    // Calculate layout dimensions based on size
    const int dotDistance = (int)(cubeSize / 3.5f);
    const int dotRadius = cubeSize / 10;

    // Create background
    _background = lv_obj_create(lv_scr_act());
    lv_obj_set_size(_background, cubeSize, cubeSize);
    lv_obj_set_style_radius(_background, dotRadius, 0);

    // Multiply color
    lv_color32_t tColor32;
    lv_color32_t sColor32;
    if (_nightMode == false)
    {
      tColor32.full = lv_color_to32(LV_COLOR_MAKE(170, 170, 170));
      sColor32.full = lv_color_to32(ColorScheme[_id]);
    }
    else
    {
      tColor32.full = lv_color_to32(LV_COLOR_MAKE(112, 112, 112));
      sColor32.full = lv_color_to32(LV_COLOR_MAKE(64, 64, 64));
    }
    lv_color_t newColor = LV_COLOR_MAKE(
        LV_UDIV255(((uint16_t)tColor32.ch.red   * sColor32.ch.red)   ),
        LV_UDIV255(((uint16_t)tColor32.ch.green * sColor32.ch.green) ),
        LV_UDIV255(((uint16_t)tColor32.ch.blue  * sColor32.ch.blue)  ));
    lv_obj_set_style_bg_color(_background, newColor, 0);

    lv_obj_set_style_border_width(_background, 0, 0);
    lv_obj_align(_background, LV_ALIGN_CENTER, pos.x, pos.y);

    // Create one led object for each dot and basic initialization
    for (int i = 0; i < 7; i++)
    {
      _led[i] = lv_obj_create(lv_scr_act());
      lv_obj_set_size(_led[i], dotRadius * 2, dotRadius * 2);
      lv_obj_set_style_radius(_led[i], dotRadius, 0);
      //lv_obj_set_style_bg_color(_led[i], colorScheme[id], 0);
      lv_obj_set_style_border_width(_led[i], dotRadius, 0); // Draw dots as borders, to prevent artifacts when drawing without border
      if (_nightMode)
        lv_obj_set_style_border_color(_led[i], ColorScheme[_id], 0);
      else
        lv_obj_set_style_border_color(_led[i], ColorDayText, 0);
    }

    // Initialize positions
    lv_obj_align(_led[0], LV_ALIGN_CENTER, pos.x + dotDistance * -1, pos.y + dotDistance * -1);
    lv_obj_align(_led[1], LV_ALIGN_CENTER, pos.x + dotDistance * -1, pos.y + dotDistance *  0);
    lv_obj_align(_led[2], LV_ALIGN_CENTER, pos.x + dotDistance * -1, pos.y + dotDistance *  1);
    lv_obj_align(_led[3], LV_ALIGN_CENTER, pos.x + dotDistance *  1, pos.y + dotDistance * -1);
    lv_obj_align(_led[4], LV_ALIGN_CENTER, pos.x + dotDistance *  1, pos.y + dotDistance *  0);
    lv_obj_align(_led[5], LV_ALIGN_CENTER, pos.x + dotDistance *  1, pos.y + dotDistance *  1);
    lv_obj_align(_led[6], LV_ALIGN_CENTER, pos.x + dotDistance *  0, pos.y + dotDistance *  0);

    // Initially we display the max number
    _randomNum = Sides();

    // Initial number
    SelectNumber(_randomNum);
  }

  //! Destructor
  virtual ~D6()
  {
    for (int i = 0; i < 7; i++)
      lv_obj_del(_led[i]);
    lv_obj_del(_background);
  }

  //! Implementation of dice
  virtual DiceType GetType() const {return DT_D6;}
};




//! Structure holding a triangle to draw
struct Triangle
{
  Vector2I pos[3];
  lv_color_t color;
};

//! DPoly Dice - ancestor of all polygonal dice
class DPoly : public Dice
{
protected:
  //! Buffer associated with arrow
  ColorAlpha* _backgroundBuffer = nullptr;
  //! Arrow object
  lv_obj_t* _background = nullptr;
  //! Big text style
  lv_style_t _styleTextBig;  
  //! Status text
  lv_obj_t* _numText = nullptr;
  //! Underscore under 6 and 9
  lv_obj_t* _undText = nullptr;
  //! Number of sides
  int _sides;
  //! Implementation of Dice
  virtual int Sides() const {return _sides;}

  //! Implementation of Dice
  virtual void SelectNumber(int num)
  {
    // Finish in case we already have the number selected
    if (_selectedNum == num)
      return;

    // Update text
    std::string numText = std::to_string(num);
    lv_label_set_text(_numText, numText.c_str());

    // Display underscrore under 6 and 9
    if ((num == 6) || (num == 9))
      lv_obj_clear_flag(_undText, LV_OBJ_FLAG_HIDDEN);
    else
      lv_obj_add_flag(_undText, LV_OBJ_FLAG_HIDDEN);

    // 0 is used to switch the dice off
    if (num == 0)
    {
      lv_obj_add_flag(_background, LV_OBJ_FLAG_HIDDEN);
      lv_obj_add_flag(_numText, LV_OBJ_FLAG_HIDDEN);
    }
    else
    {
      lv_obj_clear_flag(_background, LV_OBJ_FLAG_HIDDEN);
      lv_obj_clear_flag(_numText, LV_OBJ_FLAG_HIDDEN);
    }

    // The number is selected now
    _selectedNum = num;
  }

  //! Draw list of trianglea
  void DrawPoly(const Triangle* ts, int tsSize, int coordinatesDim)
  {
    // Clear background
    lv_canvas_fill_bg(_background, LV_COLOR_MAKE(0, 0, 0), LV_OPA_TRANSP);

    // Prepare descriptor
    lv_draw_rect_dsc_t rect_dsc;
    lv_draw_rect_dsc_init(&rect_dsc);
    lv_draw_line_dsc_t line_dsc;
    lv_draw_line_dsc_init(&line_dsc);

    // Draw triangles
    for (int i = 0; i < tsSize; i++)
    {
      // Get triangle
      const Triangle& t = ts[i];

      // Multiply color
      lv_color32_t tColor32;
      tColor32.full = lv_color_to32(t.color);
      lv_color32_t sColor32;
      if (_nightMode == false)
      {
        sColor32.full = lv_color_to32(ColorScheme[_id]);
      }
      else
      {
        sColor32.full = lv_color_to32(LV_COLOR_MAKE(64, 64, 64));
      }
      lv_color_t newColor = LV_COLOR_MAKE(
          LV_UDIV255(((uint16_t)tColor32.ch.red   * sColor32.ch.red)   ),
          LV_UDIV255(((uint16_t)tColor32.ch.green * sColor32.ch.green) ),
          LV_UDIV255(((uint16_t)tColor32.ch.blue  * sColor32.ch.blue)  ));
      rect_dsc.bg_color = newColor;
      
      // Draw triangle
      lv_point_t points[4];
      for (int j = 0; j < 3; j++)
      {
        points[j].x = t.pos[j].x * _size / coordinatesDim;
        points[j].y = t.pos[j].y * _size / coordinatesDim;
      }
      points[3].x = t.pos[0].x * _size / coordinatesDim;
      points[3].y = t.pos[0].y * _size / coordinatesDim;
      lv_canvas_draw_polygon(_background, points, 3, &rect_dsc);

      // Draw borders to not to have gaps between triangles
      line_dsc.color = rect_dsc.bg_color;
      line_dsc.width = 2;
      lv_canvas_draw_line(_background, points, 4, &line_dsc);
    }

    // text colour set
    if (_nightMode == false)
      lv_style_set_text_color(&_styleTextBig, ColorDayText);
    else
      lv_style_set_text_color(&_styleTextBig, ColorScheme[_id]);
  }

  //! Draw the shape of dice, implemented by descendants
  virtual void DrawShape() = 0;

  //! Implementation of Dice
  virtual void UpdatePos()
  {
    // Update background position
    lv_obj_set_pos(_background, _pos.x, _pos.y);

    // Update number position
    lv_obj_set_pos(_numText, _pos.x, _pos.y);
    lv_obj_set_pos(_undText, _pos.x, _pos.y + 10);

    // Allocate canvas with proper size
    Resize();

    // Draw shape of dice
    DrawShape();
  }

  //! Adapt to new size
  void Resize()
  {
    // This function is also called during initialization, the buffer does not have to exist. If it does, delete it
    if (_backgroundBuffer)
      heap_caps_free(_backgroundBuffer);

    // Create new buffer for new size and use it in the background object
    _backgroundBuffer = (ColorAlpha*)heap_caps_malloc(_size * _size * sizeof(ColorAlpha), MALLOC_CAP_SPIRAM);
    lv_canvas_set_buffer(_background, _backgroundBuffer, _size, _size, LV_IMG_CF_TRUE_COLOR_ALPHA);

    // Set font according to size
    if (_size < 80)
      lv_style_set_text_font(&_styleTextBig, &lv_font_montserrat_32);
    else
      lv_style_set_text_font(&_styleTextBig, &lv_font_montserrat_48);
  }

public:

  //! Constructor
  DPoly(int id, const Vector2I& pos, int size, bool nightMode, int sides) : Dice(id, pos, size, nightMode)
  {
    // Remember number of sides
    _sides = sides;

    // Initially we display the max number
    _randomNum = _sides;

    // Init background object
    _background = lv_canvas_create(lv_scr_act());
    lv_obj_align(_background, LV_ALIGN_CENTER, pos.x, pos.y);

    // Big text style initialization
    lv_style_init(&_styleTextBig);

    // Text representing the number
    _numText = lv_label_create(lv_scr_act());
    lv_obj_add_style(_numText, &_styleTextBig, 0);
    lv_obj_align(_numText, LV_ALIGN_CENTER, pos.x, pos.y);

    // Text representing the underscore
    _undText = lv_label_create(lv_scr_act());
    lv_obj_add_style(_undText, &_styleTextBig, 0);
    lv_obj_align(_undText, LV_ALIGN_CENTER, pos.x, pos.y + 10);
    lv_label_set_text(_undText, "_");

    // Initial number, update objects visibility and text
    SelectNumber(_randomNum);    

    // Allocate canvas with proper size
    Resize();
  }

  //! Destructor
  virtual ~DPoly()
  {
    // Delete text
    lv_obj_del(_undText);
    lv_obj_del(_numText);
    lv_obj_del(_background);
    heap_caps_free(_backgroundBuffer);
  }
};

//! DNum Dice - ancestor of all numeric dice based on image
class DNum : public Dice
{
protected:
  //! Background image - picture of dice shape
  lv_obj_t* _background;
  //! Big text style
  lv_style_t _styleTextBig;  
  //! Status text
  lv_obj_t* _numText;
  //! Underscore under 6 and 9
  lv_obj_t* _undText;
  //! Width of the source image
  int _imgWidth;
  //! Number of sides
  int _sides;
  //! Implementation of Dice
  virtual int Sides() const {return _sides;}
  //! Implementation of Dice
  virtual void SelectNumber(int num)
  {
    // Finish in case we already have the number selected
    if (_selectedNum == num)
      return;

    // Update text
    std::string numText = std::to_string(num);
    lv_label_set_text(_numText, numText.c_str());

    // Display underscrore under 6 and 9
    if ((num == 6) || (num == 9))
      lv_obj_clear_flag(_undText, LV_OBJ_FLAG_HIDDEN);
    else
      lv_obj_add_flag(_undText, LV_OBJ_FLAG_HIDDEN);

    // 0 is used to switch the dice off
    if (num == 0)
    {
      lv_obj_add_flag(_background, LV_OBJ_FLAG_HIDDEN);
      lv_obj_add_flag(_numText, LV_OBJ_FLAG_HIDDEN);
    }
    else
    {
      lv_obj_clear_flag(_background, LV_OBJ_FLAG_HIDDEN);
      lv_obj_clear_flag(_numText, LV_OBJ_FLAG_HIDDEN);
    }

    // The number is selected now
    _selectedNum = num;
  }
  //! Implementation of Dice
  virtual void UpdatePos()
  {
    lv_obj_set_pos(_background, _pos.x, _pos.y);
    lv_img_set_zoom(_background, _size * LV_IMG_ZOOM_NONE / _imgWidth);
    //lv_obj_refr_size(_background);
    lv_obj_set_pos(_numText, _pos.x, _pos.y);
    lv_obj_set_pos(_undText, _pos.x, _pos.y + 10);

    if (_size < 80)
      lv_style_set_text_font(&_styleTextBig, &lv_font_montserrat_32);
    else
      lv_style_set_text_font(&_styleTextBig, &lv_font_montserrat_48);    
  }
public:
  //! Constructor
  DNum(int id, const Vector2I& pos, int size, bool nightMode, const lv_img_dsc_t* imgSrc, int sides) : Dice(id, pos, size, nightMode)
  {
    _sides = sides;
    
    // Create background object
    _background = lv_img_create(lv_scr_act());
    lv_img_set_src(_background, imgSrc);

    _imgWidth = imgSrc->header.w;

    lv_obj_align(_background, LV_ALIGN_CENTER, pos.x, pos.y);
    lv_img_set_zoom(_background, size * LV_IMG_ZOOM_NONE / _imgWidth);
    //lv_obj_refr_size(_background);
    lv_img_set_antialias(_background, false);
    lv_obj_set_style_img_recolor_opa(_background, LV_OPA_MIN, 0); // LV_OPA_MIN is a special case, do color multiplication
    lv_obj_set_style_img_recolor(_background, ColorScheme[_id], 0);

    // Big text style initialization
    lv_style_init(&_styleTextBig);
    if (size < 80)
      lv_style_set_text_font(&_styleTextBig, &lv_font_montserrat_32);
    else
      lv_style_set_text_font(&_styleTextBig, &lv_font_montserrat_48);

    // Text representing the number
    _numText = lv_label_create(lv_scr_act());
    lv_obj_add_style(_numText, &_styleTextBig, 0);
    //lv_obj_set_style_text_color(_numText, color, 0);
    lv_obj_align(_numText, LV_ALIGN_CENTER, pos.x, pos.y);

    // Text representing the underscore
    _undText = lv_label_create(lv_scr_act());
    lv_obj_add_style(_undText, &_styleTextBig, 0);
    //lv_obj_set_style_text_color(_undText, color, 0);
    lv_obj_align(_undText, LV_ALIGN_CENTER, pos.x, pos.y + 10);
    lv_label_set_text(_undText, "_");

    // Initially we display the max number
    _randomNum = _sides;

    // Initial number
    SelectNumber(_randomNum);    
  }
  //! Destructor
  virtual ~DNum()
  {
    // Delete text
    lv_obj_del(_undText);
    lv_obj_del(_numText);
    lv_obj_del(_background);
  }
};

//! D4 Dice
class D4 : public DPoly
{
protected:

  //! Implementation of DPoly
  virtual void DrawShape()
  {
    // Define triangles
    const Triangle ts[] =
    {
      {{{1, 190}, {120, 12}, {239, 190}}, LV_COLOR_MAKE (174, 174, 174)},
      {{{1, 190}, {239, 190}, {120, 226}}, LV_COLOR_MAKE(114, 114, 114)},
    };

    // Draw triangles
    DrawPoly(ts, lenof(ts), 240);
  }

public:

  //! Constructor
  D4(int id, const Vector2I& pos, int size, bool nightMode) : DPoly(id, pos, size, nightMode, 4) {DrawShape();}
  //! Implementation of dice
  virtual DiceType GetType() const {return DT_D4;}
};

//! D4 Dice
class D8 : public DPoly
{
protected:

  //! Implementation of DPoly
  virtual void DrawShape()
  {
    // Define triangles
    const Triangle ts[] =
    {
      {{{17, 190}, {120, 1}, {223, 190}}, LV_COLOR_MAKE (174, 174, 174)},
      {{{17, 190}, {223, 190}, {120, 240}}, LV_COLOR_MAKE(130, 130, 130)},
      {{{17, 190}, {17, 60}, {120, 1}}, LV_COLOR_MAKE (158, 158, 158)},
      {{{120, 1}, {223, 60}, {223, 190}}, LV_COLOR_MAKE (114, 114, 114)},
    };

    // Draw triangles
    DrawPoly(ts, lenof(ts), 240);
  }

public:

  //! Constructor
  D8(int id, const Vector2I& pos, int size, bool nightMode) : DPoly(id, pos, size, nightMode, 8) {DrawShape();}
  //! Implementation of dice
  virtual DiceType GetType() const {return DT_D8;}
};

//D10 :O  
class D10 : public DPoly
{
protected:

  //! Implementation of DPoly
  virtual void DrawShape()
  {
    // Define triangles
    const Triangle ts[] =
    {
      {{{57, 154}, {120, 1}, {182, 154}}, LV_COLOR_MAKE (174, 174, 174)},
      {{{57, 154}, {182, 154}, {120, 191}}, LV_COLOR_MAKE(174, 174, 174)},
      {{{19, 154}, {19, 93}, {57, 154}}, LV_COLOR_MAKE (159, 159, 159)},
      {{{19, 93}, {120, 1}, {57, 154}}, LV_COLOR_MAKE (159, 159, 159)},
      {{{120, 1}, {220, 93}, {182, 154}}, LV_COLOR_MAKE (150, 150, 150)},
      {{{182, 154}, {220, 93}, {220, 154}}, LV_COLOR_MAKE(150, 150, 150)},
      {{{19, 154}, {57, 154}, {120, 237}}, LV_COLOR_MAKE (140, 140, 140)},
      {{{57, 154}, {120, 191}, {120, 237}}, LV_COLOR_MAKE (140, 140, 140)},
      {{{120, 191}, {182, 154}, {120, 237}}, LV_COLOR_MAKE (118, 118, 118)},
      {{{182, 154}, {220, 154}, {120, 237}}, LV_COLOR_MAKE (118, 118, 118)},
    };

    // Draw triangles
    DrawPoly(ts, lenof(ts), 240);
  }

public:

  //! Constructor
  D10(int id, const Vector2I& pos, int size, bool nightMode) : DPoly(id, pos, size, nightMode, 10) {DrawShape();}
  //! Implementation of dice
  virtual DiceType GetType() const {return DT_D10;}
};

//! D12 Dice
class D12 : public DPoly
{
protected:

  //! Implementation of DPoly
  virtual void DrawShape()
  {
    const Vector2I p01 = {50, 25};
    const Vector2I p02 = {120, 2};
    const Vector2I p03 = {190, 25};
    const Vector2I p04 = {230, 80};
    const Vector2I p05 = {230, 160};
    const Vector2I p06 = {190, 210};
    const Vector2I p07 = {120, 240};
    const Vector2I p08 = {50, 215};
    const Vector2I p09 = {8, 160};
    const Vector2I p10 = {10, 80};
    const Vector2I p11 = {50, 100};
    const Vector2I p12 = {120, 50};
    const Vector2I p13 = {190, 100};
    const Vector2I p14 = {160, 180};
    const Vector2I p15 = {80, 180};


    // Define triangles
    const Triangle ts[] =
    {
      {{{p15}, {p12}, {p14}}, LV_COLOR_MAKE (175, 175, 175)},
      {{{p12}, {p13}, {p14}}, LV_COLOR_MAKE(175, 175, 175)},
      {{{p11}, {p12}, {p15}}, LV_COLOR_MAKE (175, 175, 175)},
      {{{p02}, {p03}, {p12}}, LV_COLOR_MAKE (130, 130, 130)},
      {{{p12}, {p03}, {p13}}, LV_COLOR_MAKE (130, 130, 130)},
      {{{p03}, {p04}, {p13}}, LV_COLOR_MAKE(130, 130, 130)},
      {{{p13}, {p04}, {p05}}, LV_COLOR_MAKE (106, 106, 106)},
      {{{p13}, {p05}, {p14}}, LV_COLOR_MAKE (106, 106, 106)},
      {{{p14}, {p05}, {p06}}, LV_COLOR_MAKE (106, 106, 106)},
      {{{p07}, {p14}, {p06}}, LV_COLOR_MAKE (118, 118, 118)},
      // checkpoint 10
      {{{p15}, {p14}, {p07}}, LV_COLOR_MAKE (118, 118, 118)},
      {{{p15}, {p07}, {p08}}, LV_COLOR_MAKE(118, 118, 118)},
      {{{p09}, {p15}, {p08}}, LV_COLOR_MAKE (150, 150, 150)},
      {{{p09}, {p11}, {p15}}, LV_COLOR_MAKE (150, 150, 150)},
      {{{p10}, {p11}, {p09}}, LV_COLOR_MAKE (150, 150, 150)},
      {{{p10}, {p01}, {p11}}, LV_COLOR_MAKE(163, 163, 163)},
      {{{p01}, {p12}, {p11}}, LV_COLOR_MAKE (163, 163, 163)},
      {{p01, {p02}, {p12}}, LV_COLOR_MAKE (163, 163, 163)},
    };

    // Draw triangles
    DrawPoly(ts, lenof(ts), 240);
  }

public:

  //! Constructor
  D12(int id, const Vector2I& pos, int size, bool nightMode) : DPoly(id, pos, size, nightMode, 12) {DrawShape();}
  //! Implementation of dice
  virtual DiceType GetType() const {return DT_D12;}
};

//D20 :O  
class D20 : public DPoly
{
protected:

  //! Implementation of DPoly
  virtual void DrawShape()
  {
    // Define triangles
    const Triangle ts[] =
    {
      {{{57, 160}, {120, 50}, {182, 160}}, LV_COLOR_MAKE (181, 181, 181)},
      {{{57, 160}, {182, 160}, {120, 240}}, LV_COLOR_MAKE(132, 132, 132)},
      {{{19, 60}, {57, 160}, {18, 178}}, LV_COLOR_MAKE (136, 136, 136)},
      {{{19, 60}, {120, 50}, {57, 160}}, LV_COLOR_MAKE (169, 169, 169)},
      {{{19, 60}, {120, 0}, {120, 50}}, LV_COLOR_MAKE (162, 162, 162)},
      {{{120, 0}, {222, 60}, {120, 50}}, LV_COLOR_MAKE(145, 145, 145)},
      {{{120, 50}, {222, 60}, {182, 160}}, LV_COLOR_MAKE (152, 152, 152)},
      {{{182, 160}, {222, 60}, {222, 180}}, LV_COLOR_MAKE (114, 114, 114)},
      {{{120, 240}, {182, 160}, {222, 180}}, LV_COLOR_MAKE (100, 100, 100)},
      {{{18, 178}, {57, 160}, {120, 240}}, LV_COLOR_MAKE (114, 114, 114)},
    };

    // Draw triangles
    DrawPoly(ts, lenof(ts), 240);
  }

public:

  //! Constructor
  D20(int id, const Vector2I& pos, int size, bool nightMode) : DPoly(id, pos, size, nightMode, 20) {DrawShape();}
  //! Implementation of dice
  virtual DiceType GetType() const {return DT_D20;}
};

Dice* CreateDice(DiceType dt, int id, const Vector2I& pos, int size, bool nightMode)
{
  switch (dt)
  {
  case DT_D4:
    return new D4(id, pos, size, nightMode);
  case DT_D6:
    return new D6(id, pos, size, nightMode);
  case DT_D8:
    return new D8(id, pos, size, nightMode);
  case DT_D10:
    return new D10(id, pos, size, nightMode);
  case DT_D12:
    return new D12(id, pos, size, nightMode);
  case DT_D20:
    return new D20(id, pos, size, nightMode);
  }
  return nullptr;
}

//! Parameters configured by the menu
struct Parameters
{
  //! Status of application - if it has started or is still in the menu
  bool _started = false;
  //! Number of players for indicator, 1 means there is no indicator
  int _playerCount = 1;
};

//! Preview of dice, organized in the circle
class DicePreview
{
  //! Size of the preview area
  const int _sceenSize = 256;  
  //! Size of the dice preview area
  const int _noBorderSceenSize = 220;
  //! X offset of the preview area from center
  const int _xOffset = 0;
  //! Array of selected dice
  std::vector<std::shared_ptr<Dice>> _diceArray;
  //! Buffer associated with frame
  ColorAlpha* _frameBuffer = nullptr;
  //! Frame object
  lv_obj_t* _frame = nullptr;
  //! Flag to determine what mode if frame in, for lazy update
  bool _frameIsNight;
  //! Calculate optimal dice size
  int DiceSize(int diceCount) const
  {
    return (diceCount > 1) ? CirclePacking::OptimalCircleSize(diceCount, _noBorderSceenSize) : _sceenSize / 2;
  }
  //! Update frame
  void UpdateFrame(bool night)
  {
    // Clear the canvas
    lv_canvas_fill_bg(_frame, LV_COLOR_MAKE(0, 0, 0), LV_OPA_TRANSP);

    // Draw circle
    lv_draw_arc_dsc_t arc_dsc;
    lv_draw_arc_dsc_init(&arc_dsc);
    arc_dsc.color = LV_COLOR_MAKE(0, 0, 0);
    arc_dsc.width = night ? _sceenSize / 2 : 2;
    lv_canvas_draw_arc(_frame, _sceenSize / 2, _sceenSize / 2, _sceenSize / 2, 0, 360, &arc_dsc);
  }
  //! Find unused color, starting with index initIndex
  int FindAvailableColorIndex(int initColorIndex) const
  {
    int colorIndex = initColorIndex;
    do
    {
      // Go through existing dice and find one with the same color index
      int d = 0;
      for (; d < _diceArray.size(); d++)
      {
        DiceDesc dd = _diceArray[d]->GetDesc();
        if (dd._id == colorIndex)
          break;
      }

      // We didn't find matching color dice, use it
      if (d >= _diceArray.size())
        break;

      // Increase the colorIndex and try again
      colorIndex = (colorIndex < lenof(ColorScheme) - 1) ? colorIndex + 1 : 0;

    } while (colorIndex != initColorIndex);

    // Return the colorIndex
    return colorIndex;
  }

public:

  //! Constructor
  DicePreview()
  {
    // Create frame
    _frameBuffer = (ColorAlpha*)heap_caps_malloc(_sceenSize * _sceenSize * sizeof(ColorAlpha), MALLOC_CAP_SPIRAM);
    _frame = lv_canvas_create(lv_scr_act());
    lv_canvas_set_buffer(_frame, _frameBuffer, _sceenSize, _sceenSize, LV_IMG_CF_TRUE_COLOR_ALPHA);
    lv_obj_align(_frame, LV_ALIGN_CENTER, 0, 0);

    // Set frame
    UpdateFrame(false);
    _frameIsNight = false;
  }

  //! Destructor
  ~DicePreview()
  {
    // Delete arrow resources
    lv_obj_del(_frame);
    heap_caps_free(_frameBuffer);
  }

  //! Add dice to the end of the list, update positions
  void Add(DiceType dt, bool nightMode)
  {
    // The preview is full, quit
    if (_diceArray.size() >= 6)
      return;

    // Get new dice count
    int newDiceCount = _diceArray.size() + 1;

    // Prepare circle packing
    int circleSize = DiceSize(newDiceCount);
    CirclePacking cp(newDiceCount, circleSize, _noBorderSceenSize);

    // Update position and size of existing dice
    for (int i = 0; i < _diceArray.size(); i++)
      _diceArray[i]->SetPosAndSize(Vector2I(_xOffset, 0) + cp.GetPosition(i), circleSize);

    // Get the suitable ID (starting with the dice type, unique)
    int id = FindAvailableColorIndex((int)dt);

    // Add a new dice to the last position
    _diceArray.push_back(std::shared_ptr<Dice>(CreateDice(dt, id, Vector2I(_xOffset, 0) + cp.GetPosition(_diceArray.size()), circleSize, nightMode)));
  }

  //! Delete dice at given position
  void DeleteAt(const Vector2I& pos)
  {
    // Delete first item that intersects
    bool somethingWasDeleted = false;
    for (int i = 0; i < _diceArray.size(); i++)
    {
      if (_diceArray[i]->Intersects(pos))
      {
        _diceArray.erase(_diceArray.begin() + i);
        somethingWasDeleted = true;
        break;
      }
    }

    // Update position and size of dice
    if (somethingWasDeleted)
    {
      // Prepare circle packing
      int circleSize = DiceSize(_diceArray.size());
      CirclePacking cp(_diceArray.size(), circleSize, _noBorderSceenSize);

      // Update position and size of existing dice
      for (int i = 0; i < _diceArray.size(); i++)
        _diceArray[i]->SetPosAndSize(Vector2I(_xOffset, 0) + cp.GetPosition(i), circleSize);
    }
  }

  // Update night mode of exising dice
  void UpdateNightMode(bool nightMode)
  {
    // Update night mode of existing dice
    for (int i = 0; i < _diceArray.size(); i++)
    {
      if (_diceArray[i]->GetNightMode() != nightMode)
        _diceArray[i]->SetNightMode(nightMode);
    }

    // Update night mode of frame
    if (_frameIsNight != nightMode)
    {
      UpdateFrame(nightMode);
      _frameIsNight = nightMode;
    }
  }

  //! Return Dice Descriptors
  void GetDesc(std::vector<DiceDesc>& descArray) const
  {
    for (int i = 0; i < _diceArray.size(); i++)
    {
      DiceDesc dd = _diceArray[i]->GetDesc();
      descArray.push_back(dd);
    }
  }

  //! Return dice count
  int Count() const {return _diceArray.size();}
};

LV_IMG_DECLARE(day);
LV_IMG_DECLARE(night);

//! Menu dialog
class Menu
{
  //! Parameters stored in the application
  Parameters& _parameters;

  //!{ Styles
  lv_style_t _styleShadow;
  lv_style_t _styleTextMedium;
  lv_style_t _styleTextBig;
  lv_style_t _styleSelection;
  lv_style_t _styleSelectionItems;
  //!}

  //!{ Widgets
  lv_obj_t* _btnGo;
  lv_obj_t* _btnPlayerCount;
  lv_obj_t* _btnNightMode;
  //!}

  //! List of dice selectors
  std::vector<std::shared_ptr<Dice>> _diceSelector;

  //! Preview of selected dice
  DicePreview _dp;

  //!{ Events
  static void eventGo(lv_event_t* e)
  {
    Parameters* parameters = (Parameters*) e->user_data;
    lv_event_code_t code = lv_event_get_code(e);
    if (code == LV_EVENT_CLICKED)
    {
      parameters->_started = true;
    }
  }
  static void eventPlayerCount(lv_event_t* e)
  {
    Parameters* parameters = (Parameters*) e->user_data;
    lv_obj_t* obj = lv_event_get_target(e);
    uint32_t id = lv_btnmatrix_get_selected_btn(obj);
    parameters->_playerCount = id + 1;
  }
  //!}



public:

  //! Constructor
  Menu(Parameters& parameters) : _parameters(parameters)
  {
    // Shadow style initialization
    lv_style_init(&_styleShadow);
    lv_style_set_shadow_width(&_styleShadow, 50);
    lv_style_set_shadow_color(&_styleShadow, LV_COLOR_MAKE(0, 0, 0));
    lv_style_set_border_opa(&_styleShadow, LV_OPA_COVER);

    // Medium text style initialization
    lv_style_init(&_styleTextMedium);
    lv_style_set_text_font(&_styleTextMedium, &lv_font_montserrat_20);

    // Big text style initialization
    lv_style_init(&_styleTextBig);
    lv_style_set_text_font(&_styleTextBig, &lv_font_montserrat_32);

    // Selection, to be used by button matrix object
    lv_style_init(&_styleSelection);
    lv_style_set_pad_all(&_styleSelection, 0);
    lv_style_set_pad_gap(&_styleSelection, 0);
    lv_style_set_clip_corner(&_styleSelection, true);
    lv_style_set_border_width(&_styleSelection, 0);

    // Selection items, to be used by button matrix object for items
    lv_style_init(&_styleSelectionItems);
    lv_style_set_border_side(&_styleSelectionItems, LV_BORDER_SIDE_INTERNAL);
    lv_style_set_border_width(&_styleSelectionItems, 0);
    lv_style_set_radius(&_styleSelectionItems, 0);

    // Butons representing individual dice
    const int virtualDiceButtonCount = 12;
    const int noBorderSceenSize = 480 - 2 * 5;
    int diceButtonSize = CirclePacking::OptimalCircleSize(virtualDiceButtonCount, noBorderSceenSize);
    CirclePacking cp(virtualDiceButtonCount, diceButtonSize, noBorderSceenSize);
    _diceSelector.push_back(std::shared_ptr<Dice>(CreateDice(DT_D4, 0, cp.GetPosition(10), diceButtonSize, false)));
    _diceSelector.push_back(std::shared_ptr<Dice>(CreateDice(DT_D6, 1, cp.GetPosition(9), diceButtonSize, false)));
    _diceSelector.push_back(std::shared_ptr<Dice>(CreateDice(DT_D8, 2, cp.GetPosition(8), diceButtonSize, false)));
    _diceSelector.push_back(std::shared_ptr<Dice>(CreateDice(DT_D10, 3, cp.GetPosition(2), diceButtonSize, false)));
    _diceSelector.push_back(std::shared_ptr<Dice>(CreateDice(DT_D12, 4, cp.GetPosition(3), diceButtonSize, false)));
    _diceSelector.push_back(std::shared_ptr<Dice>(CreateDice(DT_D20, 5, cp.GetPosition(4), diceButtonSize, false)));

    // Go button
    _btnGo = lv_btn_create(lv_scr_act());
    lv_obj_add_style(_btnGo, &_styleShadow, 0);
    lv_obj_add_style(_btnGo, &_styleTextBig, 0);
    lv_obj_align(_btnGo, LV_ALIGN_CENTER, 0, 180);
    lv_obj_set_ext_click_area(_btnGo, 40);
    lv_obj_t* labelGo = lv_label_create(_btnGo);
    lv_label_set_text(labelGo, "Go!");
    lv_obj_add_event_cb(_btnGo, eventGo, LV_EVENT_ALL, &_parameters);
    lv_obj_add_state(_btnGo, LV_STATE_DISABLED);

    // Player count
    _btnPlayerCount = lv_btnmatrix_create(lv_scr_act());
    lv_obj_add_style(_btnPlayerCount, &_styleShadow, 0);
    lv_obj_add_style(_btnPlayerCount, &_styleTextMedium, 0);
    lv_obj_add_style(_btnPlayerCount, &_styleSelection, 0);
    lv_obj_add_style(_btnPlayerCount, &_styleSelectionItems, LV_PART_ITEMS);
    lv_obj_align(_btnPlayerCount, LV_ALIGN_CENTER, 0, -176);
    lv_obj_set_size(_btnPlayerCount, 240, 50);
    lv_obj_set_ext_click_area(_btnPlayerCount, 50);
    static const char * mapPlayerCount[] = {"No", "2", "3", "4", "5", ""};
    lv_btnmatrix_set_map(_btnPlayerCount, mapPlayerCount);
    lv_btnmatrix_set_btn_ctrl_all(_btnPlayerCount, LV_BTNMATRIX_CTRL_CHECKABLE);
    lv_btnmatrix_set_one_checked(_btnPlayerCount, true);
    lv_btnmatrix_set_btn_ctrl(_btnPlayerCount, 0, LV_BTNMATRIX_CTRL_CHECKED);
    lv_obj_add_event_cb(_btnPlayerCount, eventPlayerCount, LV_EVENT_VALUE_CHANGED, &_parameters);

    // Night mode button
    _btnNightMode = lv_imgbtn_create(lv_scr_act());
    lv_imgbtn_set_src(_btnNightMode, LV_IMGBTN_STATE_RELEASED, nullptr, &day, nullptr);
    lv_imgbtn_set_src(_btnNightMode, LV_IMGBTN_STATE_PRESSED, nullptr, &day, nullptr);
    lv_imgbtn_set_src(_btnNightMode, LV_IMGBTN_STATE_CHECKED_RELEASED, nullptr, &night, nullptr);
    lv_imgbtn_set_src(_btnNightMode, LV_IMGBTN_STATE_CHECKED_PRESSED, nullptr, &night, nullptr);
    Vector2I btnNightModePos = cp.GetPosition(7);
    lv_obj_align(_btnNightMode, LV_ALIGN_CENTER, btnNightModePos.x, btnNightModePos.y);
    lv_obj_add_flag(_btnNightMode, LV_OBJ_FLAG_CHECKABLE);
    lv_obj_set_width(_btnNightMode, day.header.w);
    lv_obj_set_height(_btnNightMode, day.header.h);
    lv_obj_set_ext_click_area(_btnPlayerCount, 20);
  }

  //! Destructor
  ~Menu()
  {
    lv_obj_del(_btnNightMode);
    lv_obj_del(_btnPlayerCount);
    lv_obj_del(_btnGo);
  }

  //! Tick
  void Tick(int tp, bool pressed, bool depressed, const Vector2I& pos)
  {
    if (pressed)
    {
      // Position in centric coordinates
      Vector2I cPos = pos - Vector2I(240, 240);

      // Check dice selector icons, if the are touched, create dice
      for (int i = 0; i < _diceSelector.size(); i++)
      {
        if (_diceSelector[i]->Intersects(cPos))
        {
          _dp.Add(_diceSelector[i]->GetType(), lv_obj_get_state(_btnNightMode) & LV_STATE_CHECKED);
          break;
        }
      }

      // Possibly delete of dice
      _dp.DeleteAt(cPos);

      // Depending on if there is some dice enable or disable Go button
      if (_dp.Count() > 0)
        lv_obj_clear_state(_btnGo, LV_STATE_DISABLED);
      else
        lv_obj_add_state(_btnGo, LV_STATE_DISABLED);
    }

    // Update night/day mode of dice in the preview
    _dp.UpdateNightMode(lv_obj_get_state(_btnNightMode) & LV_STATE_CHECKED);
    
  }

  void GetDesc(std::vector<DiceDesc>& descArray) const
  {
    _dp.GetDesc(descArray);
  }

  bool NightMode() const {return lv_obj_get_state(_btnNightMode) & LV_STATE_CHECKED;}
};

//! Dice application
class AppDice : public DDApp
{
  //! Parameters of the application, set by initial Menu dialog, used by application
  Parameters _parameters;
  //! Menu dialog
  std::shared_ptr<Menu> _menu;
  //! List of dice
  std::vector<std::shared_ptr<Dice>> _dice;
  //! Timer
  unsigned long _timer = 0;
  //! Next player index
  int _nextPlayer = 0;
  //! Buffer associated with arrow
  ColorAlpha* _arrowBuffer = nullptr;
  //! Arrow object
  lv_obj_t* _arrow = nullptr;

protected:

  //! Draw arrow for given player index
  void DrawArrow(int player)
  {
    float angle = (float)player / _parameters._playerCount * 2.0f * Pi - 0.5f * Pi;
    lv_obj_set_pos(_arrow, cos(angle) * 218, sin(angle) * 218);
    lv_img_set_angle(_arrow, (int)(angle / (Pi * 2.0f) * 3600 + 900) % 3600);
  }

public:

  //! Constructor
  AppDice(lv_obj_t* background) :
    DDApp(background)
  {
    // Application starts with dialog where user can adjust parameters
    _menu = std::shared_ptr<Menu>(new Menu(_parameters));
  }

  //! Destructor
  ~AppDice()
  {
    // Show background
    lv_obj_clear_flag(_background, LV_OBJ_FLAG_HIDDEN);

    // Delete arrow resources
    if (_arrow)
      lv_obj_del(_arrow);
    if (_arrowBuffer)
      heap_caps_free(_arrowBuffer);
  }

  //! Simulation step
  virtual void Tick(int tp, bool pressed, bool depressed, const Vector2I& pos)
  {
    // If application has started (means we are not in the dialog anymore), run
    if (_parameters._started)
    {
      // If dialog still exists, initialize run, destroy the dialog
      if (_menu != nullptr)
      {
        // Get Dice to show
        std::vector<DiceDesc> descArray;
        _menu->GetDesc(descArray);

        // Remember night mode
        bool nightMode = _menu->NightMode();

        // Destroy the menu
        _menu.reset();

        // Hide background
        if (nightMode)
          lv_obj_add_flag(_background, LV_OBJ_FLAG_HIDDEN);

        // Create indicator assets
        if (_parameters._playerCount > 1)
        {
          // Canvas representing arrow
          const int arrowSize = 32;
          _arrowBuffer = (ColorAlpha*)heap_caps_malloc(arrowSize * arrowSize * sizeof(ColorAlpha), MALLOC_CAP_SPIRAM);
          _arrow = lv_canvas_create(lv_scr_act());
          lv_canvas_set_buffer(_arrow, _arrowBuffer, arrowSize, arrowSize, LV_IMG_CF_TRUE_COLOR_ALPHA);
          lv_obj_align(_arrow, LV_ALIGN_CENTER, 0, 0);

          // Draw triangle on canvas
          lv_canvas_fill_bg(_arrow, LV_COLOR_MAKE(0, 0, 0), LV_OPA_TRANSP);
          lv_draw_rect_dsc_t rect_dsc;
          lv_draw_rect_dsc_init(&rect_dsc);
          rect_dsc.bg_color = LV_COLOR_MAKE(64, 64, 64);
          lv_point_t points[3];
          points[0].x = arrowSize / 2;
          points[0].y = 0;
          points[1].x = arrowSize;
          points[1].y = arrowSize / 2;
          points[2].x = 0;
          points[2].y = arrowSize / 2;
          lv_canvas_draw_polygon(_arrow, points, 3, &rect_dsc);

          // Draw arrow at specified position
          DrawArrow(_nextPlayer);
        }

        // Create Dice from descriptors
        int nDice = descArray.size();
        const int noBorderSceenSize = 480 - 2 * 25;
        int circleSize = CirclePacking::OptimalCircleSize(nDice, noBorderSceenSize);
        CirclePacking cp(nDice, circleSize, noBorderSceenSize);
        for (int i = 0; i < nDice; i++)
          _dice.push_back(std::shared_ptr<Dice>(CreateDice(descArray[i]._type, descArray[i]._id, cp.GetPosition(i), (nDice == 1) ? 240 : circleSize, descArray[i]._nightMode)));
      }

      // Touching, extend the timer
      if (tp > 0)
      {
        // Extend timer
        _timer = millis() + 300;

        // Shake the dice
        srand(int(millis() % INT_MAX));
        for (int i = 0; i < _dice.size(); i++)
          _dice[i]->Shake();
      }

      // Draw the dice or decrease the timer and hide the dice
      if (_timer <= millis())
      {
        // Show the dice
        bool allShown = true;
        for (int i = 0; i < _dice.size(); i++)
        {
          if (!_dice[i]->IsShown())
            allShown = false;
          _dice[i]->Show();
        }

        // Make sure whole screen is redrawn at once
        if (!allShown)
          lv_obj_invalidate(lv_scr_act());
      }
      else
      {
        // If number was shown and we are now going to hide it, update next player
        bool someDiceActive = false;
        for (int i = 0; i < _dice.size(); i++)
        {
          if (_dice[i]->IsShown())
          {
            someDiceActive = true;
            break;
          }
        }
        if (_arrow && someDiceActive)
        {
          // Update player index
          _nextPlayer = (_nextPlayer + 1) % _parameters._playerCount;

          // Draw arrow at specified position
          DrawArrow(_nextPlayer);
        }

        // Hide the dice
        bool allHidden = true;
        for (int i = 0; i < _dice.size(); i++)
        {
          if (_dice[i]->IsShown())
            allHidden = false;
          _dice[i]->Hide();
        }

        // Make sure whole screen is redrawn at once
        if (!allHidden)
          lv_obj_invalidate(lv_scr_act());        
      }
    }
    else
    {
      _menu->Tick(tp, pressed, depressed, pos);
    }
  }
};

LV_IMG_DECLARE(dice);

//! Application creator
class DiceCreator : public DDAppCreator
{
public:
  //! Implementation of DDAppCreator
  virtual std::string Name() const {return "Dice";}
  //! Implementation of DDAppCreator
  virtual const lv_img_dsc_t* Icon() const {return &dice;}
  //! Implementation of DDAppCreator
  virtual std::shared_ptr<DDApp> Create(lv_obj_t* background) {return std::make_shared<AppDice>(background);}
};

//! Creator instance
DiceCreator dc;

//! Automated application registering
//#define REGISTRY(appName)
