#include "lib\DDConfig.h"
#include "menu\XL9535_driver.h"
#include "esp_lcd_panel_io.h"
#include "esp_lcd_panel_ops.h"
#include "esp_lcd_panel_rgb.h"
#include "esp_lcd_panel_vendor.h"
#include "menu\ddApp.h"
#include "menu\ft3267.h"
#include "lvgl.h"
#include "menu\pin_config.h"
#include <Arduino.h>
#include <list>
#include "freertos\FreeRTOS.h"
#include "freertos\task.h"
#include "lib\ScopeLock.h"
#include "lib\Vector2.h"
#include "lib\CirclePacking.h"

#include "WiFi.h"
#include "esp_http_server.h"
#include <vector>

typedef struct
{
  uint8_t cmd;
  uint8_t data[16];
  uint8_t databytes; // No of data in data; bit 7 = delay after set; 0xFF = end of cmds.
} lcd_init_cmd_t;

DRAM_ATTR static const lcd_init_cmd_t st_init_cmds[] =
{
    {0xFF, {0x77, 0x01, 0x00, 0x00, 0x10}, 0x05},
    {0xC0, {0x3b, 0x00}, 0x02},
    {0xC1, {0x0b, 0x02}, 0x02},
    {0xC2, {0x07, 0x02}, 0x02},
    {0xCC, {0x10}, 0x01},
    {0xCD, {0x08}, 0x01}, //用565时屏蔽    666打开
    {0xb0, {0x00, 0x11, 0x16, 0x0e, 0x11, 0x06, 0x05, 0x09, 0x08, 0x21, 0x06, 0x13, 0x10, 0x29, 0x31, 0x18}, 0x10},
    {0xb1, {0x00, 0x11, 0x16, 0x0e, 0x11, 0x07, 0x05, 0x09, 0x09, 0x21, 0x05, 0x13, 0x11, 0x2a, 0x31, 0x18}, 0x10},
    {0xFF, {0x77, 0x01, 0x00, 0x00, 0x11}, 0x05},
    {0xb0, {0x6d}, 0x01},
    {0xb1, {0x37}, 0x01},
    {0xb2, {0x81}, 0x01},
    {0xb3, {0x80}, 0x01},
    {0xb5, {0x43}, 0x01},
    {0xb7, {0x85}, 0x01},
    {0xb8, {0x20}, 0x01},
    {0xc1, {0x78}, 0x01},
    {0xc2, {0x78}, 0x01},
    {0xc3, {0x8c}, 0x01},
    {0xd0, {0x88}, 0x01},
    {0xe0, {0x00, 0x00, 0x02}, 0x03},
    {0xe1, {0x03, 0xa0, 0x00, 0x00, 0x04, 0xa0, 0x00, 0x00, 0x00, 0x20, 0x20}, 0x0b},
    {0xe2, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 0x0d},
    {0xe3, {0x00, 0x00, 0x11, 0x00}, 0x04},
    {0xe4, {0x22, 0x00}, 0x02},
    {0xe5, {0x05, 0xec, 0xa0, 0xa0, 0x07, 0xee, 0xa0, 0xa0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 0x10},
    {0xe6, {0x00, 0x00, 0x11, 0x00}, 0x04},
    {0xe7, {0x22, 0x00}, 0x02},
    {0xe8, {0x06, 0xed, 0xa0, 0xa0, 0x08, 0xef, 0xa0, 0xa0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 0x10},
    {0xeb, {0x00, 0x00, 0x40, 0x40, 0x00, 0x00, 0x00}, 0x07},
    {0xed, {0xff, 0xff, 0xff, 0xba, 0x0a, 0xbf, 0x45, 0xff, 0xff, 0x54, 0xfb, 0xa0, 0xab, 0xff, 0xff, 0xff}, 0x10},
    {0xef, {0x10, 0x0d, 0x04, 0x08, 0x3f, 0x1f}, 0x06},
    {0xFF, {0x77, 0x01, 0x00, 0x00, 0x13}, 0x05},
    {0xef, {0x08}, 0x01},
    {0xFF, {0x77, 0x01, 0x00, 0x00, 0x00}, 0x05},
    {0x36, {0x08}, 0x01},
    {0x3a, {0x66}, 0x01},
    {0x11, {0x00}, 0x80},
    // {0xFF, {0x77, 0x01, 0x00, 0x00, 0x12}, 0x05},
    // {0xd1, {0x81}, 0x01},
    // {0xd2, {0x06}, 0x01},
    {0x29, {0x00}, 0x80},
    {0, {0}, 0xff}};

XL9535 xl;

// Flag to detect the touch interrupt was called
bool Touched = false;

bool touch_pin_get_int = false;

//! Interrupt routine to handle display touches
void IRAM_ATTR isr()
{
  Touched = true;
  touch_pin_get_int = true;
}

int VSync = 0;
void IRAM_ATTR vsync()
{
  VSync++;
}

void lcd_send_data(uint8_t data)
{
  uint8_t n;
  for (n = 0; n < 8; n++)
  {
    if (data & 0x80)
      xl.digitalWrite(LCD_SDA_PIN, 1);
    else
      xl.digitalWrite(LCD_SDA_PIN, 0);

    data <<= 1;
    xl.digitalWrite(LCD_CLK_PIN, 0);
    xl.digitalWrite(LCD_CLK_PIN, 1);
  }
}

void lcd_cmd(const uint8_t cmd)
{
  xl.digitalWrite(LCD_CS_PIN, 0);
  xl.digitalWrite(LCD_SDA_PIN, 0);
  xl.digitalWrite(LCD_CLK_PIN, 0);
  xl.digitalWrite(LCD_CLK_PIN, 1);
  lcd_send_data(cmd);
  xl.digitalWrite(LCD_CS_PIN, 1);
}

void lcd_data(const uint8_t *data, int len)
{
  uint32_t i = 0;
  if (len == 0)
    return; // no need to send anything
  do
  {
    xl.digitalWrite(LCD_CS_PIN, 0);
    xl.digitalWrite(LCD_SDA_PIN, 1);
    xl.digitalWrite(LCD_CLK_PIN, 0);
    xl.digitalWrite(LCD_CLK_PIN, 1);
    lcd_send_data(*(data + i));
    xl.digitalWrite(LCD_CS_PIN, 1);
    i++;
  } while (len--);
}

void tft_init(void)
{
  xl.digitalWrite(LCD_CS_PIN, 1);
  xl.digitalWrite(LCD_SDA_PIN, 1);
  xl.digitalWrite(LCD_CLK_PIN, 1);

  // Reset the display
  xl.digitalWrite(LCD_RST_PIN, 1);
  vTaskDelay(200 / portTICK_PERIOD_MS);
  xl.digitalWrite(LCD_RST_PIN, 0);
  vTaskDelay(200 / portTICK_PERIOD_MS);
  xl.digitalWrite(LCD_RST_PIN, 1);
  vTaskDelay(200 / portTICK_PERIOD_MS);
  int cmd = 0;
  while (st_init_cmds[cmd].databytes != 0xff)
  {
    lcd_cmd(st_init_cmds[cmd].cmd);
    lcd_data(st_init_cmds[cmd].data, st_init_cmds[cmd].databytes & 0x1F);
    if (st_init_cmds[cmd].databytes & 0x80)
    {
      vTaskDelay(100 / portTICK_PERIOD_MS);
    }
    cmd++;
  }
  Serial.println("Register setup complete");
}

static void example_lvgl_flush_cb(lv_disp_drv_t *drv, const lv_area_t *area, lv_color_t *color_map)
{
  esp_lcd_panel_handle_t panel_handle = (esp_lcd_panel_handle_t)drv->user_data;
  int offsetx1 = area->x1;
  int offsetx2 = area->x2;
  int offsety1 = area->y1;
  int offsety2 = area->y2;
  esp_lcd_panel_draw_bitmap(panel_handle, offsetx1, offsety1, offsetx2 + 1, offsety2 + 1, color_map);
  lv_disp_flush_ready(drv);
}

//! Object that is used to lock touch queues
Mutex TouchResourceMutex;

//! Structure holding one touch event
struct TouchEvent
{
  bool touch; // Touch or release
  Vector2I pos;
};

//! Touch history
class TouchHistory
{
private:
  //! History of touches
  std::list<TouchEvent> _touchHistory;

public:

  //! Push new item to history
  void Push(const TouchEvent& te)
  {
    // We are going to update touch resource, lock it
    ScopeLock lock(TouchResourceMutex);    

    // If there are some items, it is possible we will just update last
    if (_touchHistory.size() > 0)
    {
      // Get reference to last item of the history
      TouchEvent& teLast = _touchHistory.back();

      // Either add new item to touch queue, or update position of the last item
      if (teLast.touch != te.touch)
      {
        _touchHistory.push_back(te);
      }
      else
      {
        teLast.pos = te.pos;
      }
    }
    else
    {
      // Record item in the queue
      _touchHistory.push_back(te);
    }
  }

  //! Pull oldest item from history, return false in case history was empty
  bool Pull(TouchEvent& te)
  {
    // We are going to update touch resource, lock it
    ScopeLock lock(TouchResourceMutex);

    // History is empty, finish
    if (_touchHistory.size() <= 0)
      return false;

    // Pull first item from history
    te = _touchHistory.front();
    _touchHistory.pop_front();

    // We know there was some item
    return true;
  }
};

//!{ Touch Queues
TouchHistory TouchHistoryAf; // Application fast tick history
TouchHistory TouchHistoryAp; // Application tick history
TouchHistory TouchHistoryLv; // LVGL tick history
//!}

//! Callback reading input for LVGL
static void lv_touchpad_read(lv_indev_drv_t *indev_driver, lv_indev_data_t *data)
{
  // Process touching
  static bool dispLastFrameDown = false;
  static int dispLastX = 240;
  static int dispLastY = 240;
  TouchEvent te;
  if (!TouchHistoryLv.Pull(te))
  {
    te.touch = dispLastFrameDown;
    te.pos = Vector2I(dispLastX, dispLastY);
  }
  if (te.touch)
  {
    data->point.x = te.pos.x;
    data->point.y = te.pos.y;
    data->state = LV_INDEV_STATE_PR;
  }
  else
  {
    data->state = LV_INDEV_STATE_REL;
  }
}

void my_log_cb(const char * buf)
{
  Serial.println(buf);
}

//! Infinite loop running on a separate thread (second core), gathering inputs to queues
void TouchLoop(void *pvParameters)
{
  while (true)
  {
    // If display was touched, read the values and update the history
    if (Touched)
    {
      // Prevent reading next value without interrupt
      Touched = false;

      // Read new touch status
      uint8_t touchPointsNum = 0;
      uint16_t touchX = 0;
      uint16_t touchY = 0;    
      ft3267_read_pos(&touchPointsNum, &touchX, &touchY);

      // Get touching status
      bool touching = (touchPointsNum > 0);

      // Application fast history update
      TouchHistoryAf.Push({touching, Vector2I(touchX, touchY)});

      // Application history update
      TouchHistoryAp.Push({touching, Vector2I(touchX, touchY)});

      // LVGL history update
      TouchHistoryLv.Push({touching, Vector2I(touchX, touchY)});
    }

    // Wait 10 ms (this secures we read the input 100 times per second, which is above human capabilities)
    vTaskDelay(pdMS_TO_TICKS(10));
  }
}

#if DD_ENABLE_WIFI

// Encode image into BMP stream
std::vector<uint8_t> EncodeBMP(const lv_color_t* buffer, int width, int height)
{
    int padding = (4 - (width * 3) % 4) % 4;
    int size = (width * 3 + padding) * height;
    std::vector<uint8_t> bmpData(size + 54);

    uint8_t header[54] =
    {
        0x42, 0x4D,       // "BM"
        (uint8_t)(size), (uint8_t)(size >> 8), (uint8_t)(size >> 16), (uint8_t)(size >> 24), // size of BMP file
        0x00, 0x00, 0x00, 0x00, // reserved
        0x36, 0x00, 0x00, 0x00, // offset of pixel array
        0x28, 0x00, 0x00, 0x00, // size of BITMAPINFOHEADER
        (uint8_t)(width), (uint8_t)(width >> 8), (uint8_t)(width >> 16), (uint8_t)(width >> 24), // width of image
        (uint8_t)(height), (uint8_t)(height >> 8), (uint8_t)(height >> 16), (uint8_t)(height >> 24), // height of image
        0x01, 0x00,       // number of color planes
        0x18, 0x00,       // number of bits per pixel
        0x00, 0x00, 0x00, 0x00, // compression method (0 = uncompressed)
        (uint8_t)(size - 54), (uint8_t)((size - 54) >> 8), (uint8_t)((size - 54) >> 16), (uint8_t)((size - 54) >> 24), // size of pixel array
        0x13, 0x0B, 0x00, 0x00, // horizontal resolution (2835 pixels/meter)
        0x13, 0x0B, 0x00, 0x00, // vertical resolution (2835 pixels/meter)
        0x00, 0x00, 0x00, 0x00, // number of colors in the palette
        0x00, 0x00, 0x00, 0x00  // number of important colors
    };

    std::copy(std::begin(header), std::end(header), std::begin(bmpData));

    int offset = 54;
    for (int y = height - 1; y >= 0; y--)
    {
        for (int x = 0; x < width; x++)
        {
            lv_color32_t pixel;
            pixel.full = lv_color_to32(buffer[y * width + x]);
            bmpData[offset++] = pixel.ch.blue;
            bmpData[offset++] = pixel.ch.green;
            bmpData[offset++] = pixel.ch.red;
        }
        offset += padding;
    }

    return bmpData;
}

// Our URI handler function to be called during GET /uri request
esp_err_t get_handler(httpd_req_t *req)
{
  // Get background image
  lv_color_t* backgroundBuffer = (lv_color_t *)heap_caps_malloc(EXAMPLE_LCD_H_RES * EXAMPLE_LCD_V_RES * sizeof(lv_color_t), MALLOC_CAP_SPIRAM);
  lv_img_dsc_t backgroundDsc;
  lv_snapshot_take_to_buf(lv_scr_act(), LV_IMG_CF_TRUE_COLOR, &backgroundDsc, backgroundBuffer, EXAMPLE_LCD_H_RES * EXAMPLE_LCD_V_RES * sizeof(lv_color_t));

  // Canvas representing background with increased resolution for border
  lv_color_t* canvasBuffer = (lv_color_t *)heap_caps_malloc(512 * 512 * sizeof(lv_color_t), MALLOC_CAP_SPIRAM);
  lv_obj_t* canvas = lv_canvas_create(lv_scr_act());
  lv_canvas_set_buffer(canvas, canvasBuffer, 512, 512, LV_IMG_CF_TRUE_COLOR);
 
  // Simple draw descriptor
  lv_draw_img_dsc_t drawDscSimple;
  lv_draw_img_dsc_init(&drawDscSimple);

  // Draw background on canvas
  lv_canvas_draw_img(canvas, 16, 16, &backgroundDsc, &drawDscSimple);

  // Draw border
  for (int y = 0; y < 512; y++)
    for (int x = 0; x < 512; x++)
    {
      Vector2I p(x - 256, y - 256);
      int l = p.Size();
      if (l >= 256)
      {
        canvasBuffer[y * 512 + x] = LV_COLOR_MAKE(255, 255, 255);
      }
      else if (l >= 240)
      {
        canvasBuffer[y * 512 + x] = LV_COLOR_MAKE(0, 0, 0);
      }
    }
 
  // Encode screenshot into bmp stream
  std::vector<uint8_t> bmpStream = EncodeBMP(canvasBuffer, 512, 512);

  // Send image over http
  httpd_resp_set_type(req, "image/bmp");
  httpd_resp_send(req, (const char *)bmpStream.data(), bmpStream.size());

  // Free the memory used by the screenshot
  lv_obj_del(canvas);
  heap_caps_free(canvasBuffer);

  // Free the memory used by the background
  heap_caps_free(backgroundBuffer);

  return ESP_OK;
}

/* Our URI handler function to be called during POST /uri request */
esp_err_t post_handler(httpd_req_t *req)
{
    /* Destination buffer for content of HTTP POST request.
     * httpd_req_recv() accepts char* only, but content could
     * as well be any binary data (needs type casting).
     * In case of string data, null termination will be absent, and
     * content length would give length of string */
    char content[100];

    /* Truncate if content length larger than the buffer */
    size_t recv_size = std::min(req->content_len, sizeof(content));

    int ret = httpd_req_recv(req, content, recv_size);
    if (ret <= 0) {  /* 0 return value indicates connection closed */
        /* Check if timeout occurred */
        if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
            /* In case of timeout one can choose to retry calling
             * httpd_req_recv(), but to keep it simple, here we
             * respond with an HTTP 408 (Request Timeout) error */
            httpd_resp_send_408(req);
        }
        /* In case of error, returning ESP_FAIL will
         * ensure that the underlying socket is closed */
        return ESP_FAIL;
    }

    /* Send a simple response */
    const char resp[] = "URI POST Response";
    httpd_resp_send(req, resp, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

/* URI handler structure for GET /uri */
httpd_uri_t uri_get = {
    .uri      = "/screenshot",
    .method   = HTTP_GET,
    .handler  = get_handler,
    .user_ctx = NULL
};

/* URI handler structure for POST /uri */
httpd_uri_t uri_post = {
    .uri      = "/uri",
    .method   = HTTP_POST,
    .handler  = post_handler,
    .user_ctx = NULL
};

/* Function for starting the webserver */
httpd_handle_t start_webserver(void)
{
    /* Generate default configuration */
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    /* Empty handle to esp_http_server */
    httpd_handle_t server = NULL;

    /* Start the httpd server */
    if (httpd_start(&server, &config) == ESP_OK) {
        /* Register URI handlers */
        httpd_register_uri_handler(server, &uri_get);
        httpd_register_uri_handler(server, &uri_post);
    }
    /* If server failed to start, handle will be NULL */
    return server;
}

/* Function for stopping the webserver */
void stop_webserver(httpd_handle_t server)
{
    if (server) {
        /* Stop the httpd server */
        httpd_stop(server);
    }
}

#endif







#define BATTV_MAX    3.80    // maximum voltage of battery
#define BATTV_MIN    3.30    // what we regard as an empty battery
#define BATTV_CHG    4.20    // voltage detecting charging

//! Battery indicator
class Battery
{
  //! Style
  lv_style_t _style;
  //! Battery label
  lv_obj_t* _battery;
  //! When indicator should be deleted
  uint32_t _deleteTime;
  //! Life time of indicator
  static const int LifeTime = 2000;

public:

  //! Constructor
  Battery(float voltage)
  {
    // Style
    lv_style_init(&_style);
    lv_style_set_text_font(&_style, &lv_font_montserrat_32);
    lv_style_set_text_color(&_style, LV_COLOR_MAKE(32, 32, 32));

    // Label
    _battery = lv_label_create(lv_scr_act());
    lv_obj_add_style(_battery, &_style, 0);
    lv_obj_align(_battery, LV_ALIGN_CENTER, 0, 0);

    // Status
    float statusCoef = (voltage - BATTV_MIN) / (BATTV_MAX - BATTV_MIN);
    statusCoef = std::max(std::min(statusCoef, 1.0f), 0.01f); // Don't show less than 1%
#if DD_ENABLE_WIFI
    lv_label_set_text_fmt(_battery, "%d%%\n%s", std::lround(statusCoef * 100.0f), WiFi.localIP().toString().c_str());
#else
    if (voltage >= BATTV_CHG)
      lv_label_set_text_fmt(_battery, "charging...");
    else
      lv_label_set_text_fmt(_battery, "%d%%", std::lround(statusCoef * 100.0f));
#endif

    // Set of deletion
    Extend();
  }

  //! Destructor
  ~Battery()
  {
    lv_obj_del(_battery);
  }

  //! Simulation step, do animation, return false in case the indicator should be deleted
  bool Tick()
  {
    // Check if indicator should be deleted
    if (millis() >= _deleteTime)
      return false;

    // Continue with indicator
    return true;
  }

  //! Extend time for which the indicator is going to be displayed
  void Extend()
  {
    _deleteTime = millis() + Battery::LifeTime;
  }
};

//! Battery indicator
std::shared_ptr<Battery> GBattery;

//! Running application or null if we are in a menu
std::shared_ptr<DDApp> GApp;

LV_IMG_DECLARE(background);

lv_obj_t* objBackground;

//! Buttons associated with applications
std::vector<lv_obj_t*> Buttons;

// Application menu
void CreateMenu()
{


  static lv_style_t style_def;
  lv_style_init(&style_def);
  lv_style_set_text_color(&style_def, lv_color_white());

  // Add a shadow
  lv_style_set_radius(&style_def, 64);
  lv_style_set_shadow_width(&style_def, 50);
  lv_style_set_shadow_color(&style_def, LV_COLOR_MAKE(64, 64, 64));
  lv_style_set_border_opa(&style_def, LV_OPA_COVER);

  // Darken the button when pressed and make it wider
  static lv_style_t style_pr;
  lv_style_init(&style_pr);
  lv_style_set_img_recolor_opa(&style_pr, LV_OPA_30);
  //lv_style_set_img_recolor(&style_pr, lv_color_black());
  //lv_style_set_transform_width(&style_pr, 200);
  lv_style_set_shadow_width(&style_pr, 25);

  // Create application icons
  const int nApps = GetAppCreators().creators.size();
  const lv_img_dsc_t* firstIcon = GetAppCreators().creators[0]->Icon();
  CirclePacking cp(nApps, firstIcon->header.w);
  for (int i = 0; i < nApps; i++)
  {
    // Get position of icon
    Vector2I pos = cp.GetPosition(i);

    // Create icon
    lv_obj_t* b = lv_imgbtn_create(lv_scr_act());
    lv_imgbtn_set_src(b, LV_IMGBTN_STATE_RELEASED, nullptr, GetAppCreators().creators[i]->Icon(), nullptr);
    lv_obj_add_style(b, &style_def, 0);
    lv_obj_add_style(b, &style_pr, LV_STATE_USER_1);
    lv_obj_set_width(b, LV_SIZE_CONTENT);
    lv_obj_align(b, LV_ALIGN_CENTER, pos.x, pos.y);
    Buttons.push_back(b);
  }
}

void DestroyMenu()
{
  // Clear battery indicator
  GBattery.reset();

  // Clear Buttons
  for (auto b : Buttons)
    lv_obj_del(b);
  Buttons.clear();
}


//lv_obj_t* labelFPS;

void ui_begin()
{
  // Create background
  objBackground = lv_img_create(lv_scr_act());
  lv_img_set_src(objBackground, &background);
  lv_obj_align(objBackground, LV_ALIGN_CENTER, 0, 0);

  //app = CreateAppDice();
  CreateMenu();

  static lv_style_t style_btn_red;
  lv_style_init(&style_btn_red);
  lv_style_set_bg_color(&style_btn_red, lv_color_black());
  lv_style_set_bg_opa(&style_btn_red, LV_OPA_COVER);
  lv_obj_add_style(lv_scr_act(), &style_btn_red, 0);

/*
  labelFPS = lv_label_create(lv_scr_act());
  lv_obj_set_width(labelFPS, 50);
  lv_obj_set_style_text_align(labelFPS, LV_TEXT_ALIGN_CENTER, 0);
  lv_obj_set_style_text_color(labelFPS, LV_COLOR_MAKE(255, 0, 0), 0);
  lv_obj_align(labelFPS, LV_ALIGN_CENTER, 0, 0);
  lv_label_set_text(labelFPS, "FPS");
*/
}

bool IntersectsWithPoint(lv_obj_t* obj, int x, int y)
{
  lv_coord_t r = lv_obj_get_style_radius(obj, LV_PART_MAIN);
  lv_coord_t w = lv_obj_get_style_transform_width(obj, LV_PART_MAIN);
  lv_coord_t h = lv_obj_get_style_transform_height(obj, LV_PART_MAIN);

  // Enlarge area for collision checking
  const int margin = 20;
  w += margin;
  h += margin;

  lv_area_t coords;
  lv_area_copy(&coords, &obj->coords);
  coords.x1 -= w;
  coords.x2 += w;
  coords.y1 -= h;
  coords.y2 += h;
  lv_point_t p = {(lv_coord_t)x, (lv_coord_t)y};
  return _lv_area_is_point_on(&coords, &p, r);
}

//! Running average calculator for voltage status
class RunningAverage
{
  //! circular queue to store data points
  float* data;
  //! sum of data points in the queue
  float sum;
  //! index of the oldest data point in the queue
  int front;
  //! index of the newest data point in the queue
  int rear;
  //! size of the queue
  int size;
  //! number of data points in the queue
  int count;
  //! number of skipped samples
  int sampleSkipCount;
public:

  //! Constructor
  RunningAverage(int windowSize)
  {
    size = windowSize;
    data = new float[size];
    reset();
  }

  //! Destructor
  ~RunningAverage()
  {
    delete[] data;
  }

  //! Adding new data
  void addData(float newData)
  {
    // Record new data
    if (count == size)
    {
      sum -= data[front]; // remove the oldest data point from the sum
      front = (front + 1) % size; // move the front pointer to the next oldest data point
      count--;
    }
    rear = (rear + 1) % size; // move the rear pointer to the next empty slot
    data[rear] = newData; // add the new data point to the queue
    sum += newData; // add the new data point to the sum
    count++;
  }

  //! Reset the structure, forget the history
  void reset()
  {
    sum = 0.0f;
    front = 0;
    rear = -1;
    count = 0;
    sampleSkipCount = 1000000;
  }

  //! Getting the running average
  float getAverage() const
  {
    // Return 0 if the queue is empty
    if (count == 0)
      return 0.0f;

    // Calculate and return the running average
    return sum / count;
  }

  //! Calculating value of the average element (this is more smooth than just an average)
  float calculateAverageElementValue()
  {
    // Return 0 if the queue is empty
    if (count <= 0)
     return 0.0f;

    // Copy values to temp array
    float* sdata = new float[count];
    for (int i = 0; i < count; i++)
      sdata[i] = data[i];

    // Sort the array
    std::qsort(sdata, count, sizeof(sdata[0]), [](const void* x, const void* y)
      {
        const float arg1 = *static_cast<const float*>(x);
        const float arg2 = *static_cast<const float*>(y);
        const auto cmp = arg1 - arg2;
        if (cmp < 0)
          return -1;
        if (cmp > 0)
          return 1;
        return 0;
      }
    );

    // Get value of the average element
    float avgElementValue =  sdata[count / 2];

    // Delete the temp array
    delete[] sdata;

    // Return value of the average element
    return avgElementValue;
  }
};

//! Average voltage from last 64 seconds
RunningAverage VoltageAvg(64);

void RenderFrame(const TouchState& ts, float voltage)
{
  // Detectif we are chargning
  bool isCharging = (voltage >= BATTV_CHG);

  // Register new voltage value, only in case we are not charging, to not to distort history
  if (isCharging)
    VoltageAvg.reset();
  else
    VoltageAvg.addData(voltage);

  // Update state of buttons
  if (Buttons.size() > 0)
  {
    if (ts._dispTouching)
    {
      for (auto b : Buttons)
      {
        if (IntersectsWithPoint(b, ts._dispX, ts._dispY))
          lv_obj_add_state(b, LV_STATE_USER_1);
        else
          lv_obj_clear_state(b, LV_STATE_USER_1);
      }
    }
    else
    {
      for (auto b : Buttons)
        lv_obj_clear_state(b, LV_STATE_USER_1);
    }

    // Run application if user clicked on it
    if (ts._dispDepressed)
    {
      for (int i = 0; i < Buttons.size(); i++)
      {
        if (IntersectsWithPoint(Buttons[i], ts._dispX, ts._dispY))
        {
          // Destroy menu
          DestroyMenu();

          // Start application
          GApp = GetAppCreators().creators[i]->Create(objBackground);
          break;
        }
      }
    }

    // Update Battery status, when buton is pressed
    if (ts._btnDown)
    {
      // Update battery dialog
      if (GBattery)
        GBattery->Extend();
      else
        GBattery = std::make_shared<Battery>(isCharging ? voltage : VoltageAvg.calculateAverageElementValue());
    }
  }
  else // In this branch app must exist
  {
    // Application simulation step
    GApp->Tick(ts._dispTouching ? 1 : 0, ts._dispPressed, ts._dispDepressed, Vector2I(ts._dispX, ts._dispY));

    // If button is pressed, exit the application and create the menu again
    if (ts._btnDown)
    {
      // Exit application
      GApp.reset();

      // Recreate menu
      CreateMenu();
    }
  }

  // Simulate battery status diag, possible delete
  if (GBattery)
  {
    if (!GBattery->Tick())
      GBattery.reset();
  }



/*
  static uint32_t lastMS = 0;
  uint32_t newMS = millis();
  float fps = 1000.0f / (newMS - lastMS);
  lastMS = newMS;
  lv_label_set_text_fmt(labelFPS, "%.1f\n%.2f", fps, voltage);
  lv_obj_move_foreground(labelFPS);
*/

/*
  static uint32_t lastMS = 0;
  uint32_t newMS = millis();
  float fps = 1000.0f / (newMS - lastMS);
  lastMS = newMS;
  Serial.printf("%.1f\n", fps);
*/





}







void setup()
{
  static lv_disp_draw_buf_t disp_buf; // contains internal graphic buffer(s) called draw buffer(s)
  static lv_disp_drv_t disp_drv;      // contains callback functions
  static lv_indev_drv_t indev_drv;
  // put your setup code here, to run once:
  pinMode(BAT_VOLT_PIN, ANALOG);

  Wire.begin(IIC_SDA_PIN, IIC_SCL_PIN, (uint32_t)400000);
  Serial.begin(115200);
  //while(!Serial) {}
  //Serial.println("ahoj");
  xl.begin();
  uint8_t pin = (1 << PWR_EN_PIN) | (1 << LCD_CS_PIN) | (1 << TP_RES_PIN) | (1 << LCD_SDA_PIN) | (1 << LCD_CLK_PIN) |
                (1 << LCD_RST_PIN) | (1 << SD_CS_PIN);

  xl.pinMode8(0, pin, OUTPUT);
  xl.digitalWrite(PWR_EN_PIN, 1);
  pinMode(EXAMPLE_PIN_NUM_BK_LIGHT, OUTPUT);
  digitalWrite(EXAMPLE_PIN_NUM_BK_LIGHT, EXAMPLE_LCD_BK_LIGHT_ON_LEVEL);

  xl.digitalWrite(TP_RES_PIN, 0);
  delay(200);
  xl.digitalWrite(TP_RES_PIN, 1);
  ft3267_init(Wire);
  tft_init();
  esp_lcd_panel_handle_t panel_handle = NULL;
  esp_lcd_rgb_panel_config_t panel_config = {
      .clk_src = LCD_CLK_SRC_PLL160M,
      .timings =
          {
              .pclk_hz = EXAMPLE_LCD_PIXEL_CLOCK_HZ,
              .h_res = EXAMPLE_LCD_H_RES,
              .v_res = EXAMPLE_LCD_V_RES,
              // The following parameters should refer to LCD spec
              .hsync_pulse_width = 1,
              .hsync_back_porch = 30,
              .hsync_front_porch = 50,
              .vsync_pulse_width = 1,
              .vsync_back_porch = 30,
              .vsync_front_porch = 20,
              .flags =
                  {
                      .pclk_active_neg = 1,
                  },
          },
      .data_width = 16, // RGB565 in parallel mode, thus 16bit in width
      .psram_trans_align = 64,
      .hsync_gpio_num = EXAMPLE_PIN_NUM_HSYNC,
      .vsync_gpio_num = EXAMPLE_PIN_NUM_VSYNC,
      .de_gpio_num = EXAMPLE_PIN_NUM_DE,
      .pclk_gpio_num = EXAMPLE_PIN_NUM_PCLK,
      .data_gpio_nums =
          {
              // EXAMPLE_PIN_NUM_DATA0,
              EXAMPLE_PIN_NUM_DATA13,
              EXAMPLE_PIN_NUM_DATA14,
              EXAMPLE_PIN_NUM_DATA15,
              EXAMPLE_PIN_NUM_DATA16,
              EXAMPLE_PIN_NUM_DATA17,

              EXAMPLE_PIN_NUM_DATA6,
              EXAMPLE_PIN_NUM_DATA7,
              EXAMPLE_PIN_NUM_DATA8,
              EXAMPLE_PIN_NUM_DATA9,
              EXAMPLE_PIN_NUM_DATA10,
              EXAMPLE_PIN_NUM_DATA11,
              // EXAMPLE_PIN_NUM_DATA12,

              EXAMPLE_PIN_NUM_DATA1,
              EXAMPLE_PIN_NUM_DATA2,
              EXAMPLE_PIN_NUM_DATA3,
              EXAMPLE_PIN_NUM_DATA4,
              EXAMPLE_PIN_NUM_DATA5,
          },
      .disp_gpio_num = EXAMPLE_PIN_NUM_DISP_EN,
      .on_frame_trans_done = NULL,
      .user_ctx = NULL,
      .flags =
          {
              .fb_in_psram = 1, // allocate frame buffer in PSRAM
          },
  };
  ESP_ERROR_CHECK(esp_lcd_new_rgb_panel(&panel_config, &panel_handle));
  ESP_ERROR_CHECK(esp_lcd_panel_reset(panel_handle));
  ESP_ERROR_CHECK(esp_lcd_panel_init(panel_handle));

  //esp_lcd_panel_draw_bitmap(panel_handle, 0, 0, 480, 480, logo_img);
  //delay(5000);

  lv_init();
  // alloc draw buffers used by LVGL from PSRAM
  lv_color_t *buf1 =
      (lv_color_t *)heap_caps_malloc(EXAMPLE_LCD_H_RES * EXAMPLE_LCD_V_RES * sizeof(lv_color_t), MALLOC_CAP_SPIRAM);
  assert(buf1);
  lv_color_t *buf2 =
      (lv_color_t *)heap_caps_malloc(EXAMPLE_LCD_H_RES * EXAMPLE_LCD_V_RES * sizeof(lv_color_t), MALLOC_CAP_SPIRAM);
  assert(buf2);
  lv_disp_draw_buf_init(&disp_buf, buf1, buf2, EXAMPLE_LCD_H_RES * EXAMPLE_LCD_V_RES);

  Serial.println("Register display driver to LVGL");
  lv_disp_drv_init(&disp_drv);
  disp_drv.hor_res = EXAMPLE_LCD_H_RES;
  disp_drv.ver_res = EXAMPLE_LCD_V_RES;
  disp_drv.flush_cb = example_lvgl_flush_cb;
  disp_drv.draw_buf = &disp_buf;
  disp_drv.user_data = panel_handle;
  //disp_drv.antialiasing = 0;
  //disp_drv.full_refresh = true;
  //disp_drv.direct_mode = true;
  lv_disp_t *disp = lv_disp_drv_register(&disp_drv);

  lv_indev_drv_init(&indev_drv);
  indev_drv.type = LV_INDEV_TYPE_POINTER;
  indev_drv.read_cb = lv_touchpad_read;
  lv_indev_drv_register(&indev_drv);

  pinMode(TP_INT_PIN, INPUT_PULLUP);
  attachInterrupt(TP_INT_PIN, isr, FALLING);

  pinMode(BOOT_BTN_PIN, INPUT_PULLUP);

  // Catch VSync signal
  attachInterrupt(EXAMPLE_PIN_NUM_VSYNC, vsync, FALLING);

  Serial.println("Display LVGL Scatter Chart");

  ui_begin();

  lv_log_register_print_cb(my_log_cb);

  // Delete the original display refresh timer
  // We are going to call _lv_disp_refr_timer function manually, synced with VSync signal
  lv_timer_del(disp->refr_timer);
  disp->refr_timer = NULL;

  // Spawn task for touch loop
  xTaskCreate(TouchLoop, "Touch Loop Task", 2048, NULL, 1, NULL);

  // Disable screen scroll
  lv_obj_clear_flag(lv_scr_act(), LV_OBJ_FLAG_SCROLLABLE);

#if DD_ENABLE_WIFI
  String str;
  uint32_t last_m = millis();
  str = "connecting to wifi\r\n";
  str += "SSID : " WIFI_SSID "\r\n";
  str += "PASSWORD : " WIFI_PASSWORD "\r\n";
  Serial.print(str);

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  uint32_t timestamp = millis() + 20000;
  while (WiFi.status() != WL_CONNECTED)
  {
    if (timestamp < millis())
    {
        break;
    }
    Serial.print(".");
    sleep(1);
  }

  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.printf("\r\n-- wifi connect success! --\r\n");
    Serial.printf("It takes %d milliseconds\r\n", millis() - last_m);
    Serial.print("IP: "); Serial.println(WiFi.localIP());
    Serial.print("HostName: "); Serial.println(WiFi.getHostname());
    Serial.print("Use http://"); Serial.print(WiFi.localIP()); Serial.println("/screenshot to take a screenshot");
  }
  else
  {
    Serial.printf("\r\n-- wifi connect failed! --\r\n");
  }

  // Start web server
  if (WiFi.status() == WL_CONNECTED)
  {
    start_webserver();
  }
#endif
}


void loop()
{



  // put your main code here, to run repeatedly:
  static uint32_t Millis;
  static float Voltage = -1.0f;
  //delay(2);

  lv_timer_handler();




  if (millis() - Millis > 50) {
    Voltage = (analogRead(BAT_VOLT_PIN) * 2 * 3.3) / 4096;
    Millis = millis();
  }






  // Process touching
  TouchEvent te;
  if (TouchHistoryAf.Pull(te))
  {
    if (GApp)
      GApp->Touch(te.touch ? 1 : 0, te.pos);    
  }

  // Frame rendering synchronized with VSync signal
  const int frameStep = 1;
  if (VSync >= frameStep)
  {
    // Zero VSync counter so that interrupt can immediately start counting again. This way it is robust when frameStep > 1
    // and one frame takes more than 1 vsync signal
    VSync = 0;

    // Get touch from the queue at the tick pace
    // In case the queue is empty, it does not have to indicate there is no touch, but that we were calling pull
    // before another push - in such case just use the last known values
    static bool dispLastFrameDown = false;
    static int dispLastX = 240;
    static int dispLastY = 240;
    TouchEvent te;
    if (!TouchHistoryAp.Pull(te))
    {
      te.touch = dispLastFrameDown;
      te.pos = Vector2I(dispLastX, dispLastY);
    }

    // Initialize the touch state
    TouchState ts;
    {
      // Update the pressed / depressed status
      bool dispPressed = false;
      bool dispDepressed = false;
      if (te.touch != dispLastFrameDown)
      {
        if (te.touch)
          dispPressed = true;
        else
          dispDepressed = true;
        dispLastFrameDown = te.touch;
      }

      // Update the last pressed position
      if (te.touch)
      {
        dispLastX = te.pos.x;
        dispLastY = te.pos.y;
      }

      // Update button status
      static bool btnLastFrameDown = false;
      bool btnDown = (digitalRead(BOOT_BTN_PIN) == 0);
      bool btnPressed = false;
      bool btnDepressed = false; 
      if (btnDown != btnLastFrameDown)
      {
        if (btnDown)
          btnPressed = true;
        else
          btnDepressed = true;
        btnLastFrameDown = btnDown;
      }
      
      // Initialize the touch state
      ts._dispTouching = te.touch;
      ts._dispPressed = dispPressed;
      ts._dispDepressed = dispDepressed;
      ts._dispX = dispLastX;
      ts._dispY = dispLastY;
      ts._btnDown = btnDown;
      ts._btnPressed = btnPressed;
      ts._btnDepressed = btnDepressed;
    }

    // Frame rendering with touch event
    RenderFrame(ts, Voltage);

    // Refresh screen
    _lv_disp_refr_timer(NULL);
  }
}




