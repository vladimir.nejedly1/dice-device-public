// Copyright (c) 2023 Vladimir Nejedly, vladimir.nejedly@gmail.com
// Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license
// https://creativecommons.org/licenses/by-nc-sa/4.0/
// This software is provided "as is", without any warranty or condition.

#pragma once

#include <string>
#include <vector>
#include <memory>
#include "lvgl.h"
#include "..\lib\Color.h"
#include "..\lib\Vector2.h"

//! State of touching of display and buttons
struct TouchState
{
  //! Display is being touched
  bool _dispTouching;
  //! Display was touched just this frame
  bool _dispPressed;
  //! Display was untouched just this frame
  bool _dispDepressed;
  //! X coordinate of where display was last touched
  int _dispX;
  //! Y coordinate of where display was last touched
  int _dispY;
  //! Button is being pressed
  bool _btnDown;
  //! Button was pressed just this frame
  bool _btnPressed;
  //! Button was depressed just this frame
  bool _btnDepressed;
};

//! Ancestor of all Dice Device Applications
class DDApp
{
protected:
  //! Background object, inherited from the main menu. Application can modify it, hide it, but upon exit it needs to restore
  //! it to original state
  lv_obj_t* _background;
public:
  //! Constructor
  DDApp(lv_obj_t* background) : _background(background) {}
  //! Touch detection, invoked in a fast pace. pos is in screen coordinates
  virtual void Touch(int tp, const Vector2I& pos) {}
  //! Simulation step
  virtual void Tick(int tp, bool pressed, bool depressed, const Vector2I& pos) = 0;
};

//! Ancestor of all application creators
class DDAppCreator
{
  //! Buffer associated with canvas representing the icon
  mutable ColorAlpha* _iconCanvasBuffer = nullptr;
  //! Object representin the icon
  mutable lv_obj_t* _iconCanvas = nullptr;
public:
  //! Constructor, performs self-registering in creators structure
  DDAppCreator();
  //! Destructor
  ~DDAppCreator();
  //! Application name
  virtual std::string Name() const = 0;
  //! Application icon, create it from the first letter of the application name
  virtual const lv_img_dsc_t* Icon() const;
  //! Application creation
  virtual std::shared_ptr<DDApp> Create(lv_obj_t* background) = 0;
};

//! Structure holding creators
struct AppCreators
{
  //! Container where all app creators are registered
  std::vector<DDAppCreator*> creators;
};

//! Access to global creators structure
AppCreators& GetAppCreators();