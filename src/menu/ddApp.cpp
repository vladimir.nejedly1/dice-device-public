#include "Arduino.h"
#include "ddApp.h"

DDAppCreator::DDAppCreator()
{
  GetAppCreators().creators.push_back(this);
}

DDAppCreator::~DDAppCreator()
{
  if (_iconCanvas)
    lv_obj_del(_iconCanvas);
  if (_iconCanvasBuffer)
    heap_caps_free(_iconCanvasBuffer);  
}

LV_IMG_DECLARE(button);

const lv_img_dsc_t* DDAppCreator::Icon() const
{
  // Create icon when calling the function for the first time
  if (_iconCanvasBuffer == nullptr)
  {
    // Canvas representing background
    _iconCanvasBuffer = (ColorAlpha *)heap_caps_malloc(button.header.h * button.header.w * sizeof(ColorAlpha), MALLOC_CAP_SPIRAM);
    _iconCanvas = lv_canvas_create(lv_scr_act());
    lv_obj_add_flag(_iconCanvas, LV_OBJ_FLAG_HIDDEN);
    lv_canvas_set_buffer(_iconCanvas, _iconCanvasBuffer, button.header.h, button.header.w, LV_IMG_CF_TRUE_COLOR_ALPHA);

    // Clear the canvas
    lv_canvas_fill_bg(_iconCanvas, LV_COLOR_MAKE(0, 0, 0), LV_OPA_TRANSP);

    // Simple draw descriptor
    lv_draw_img_dsc_t drawDscSimple;
    lv_draw_img_dsc_init(&drawDscSimple);

    // Draw button on canvas
    lv_canvas_draw_img(_iconCanvas, 0, 0, &button, &drawDscSimple);

    // Draw text - first letter of application
    const int textWidth = 48;
    lv_draw_label_dsc_t dd;
    lv_draw_label_dsc_init(&dd);
    dd.font = &lv_font_montserrat_48;
    dd.ofs_x = -textWidth/2;
    dd.ofs_y = -dd.font->line_height/2;
    dd.color = LV_COLOR_MAKE(0, 0, 0);
    dd.align = LV_TEXT_ALIGN_CENTER;
    char firstChar = (Name().length() > 0) ? Name()[0] : 'X';
    char capitalizedChar = std::toupper(firstChar);
    char text[3];
    sprintf(text, "%c", capitalizedChar);
    lv_canvas_draw_text(_iconCanvas, button.header.w / 2, button.header.h / 2, textWidth, &dd, text);    
  }

  // Return descriptor
  return (lv_img_dsc_t*)lv_img_get_src(_iconCanvas);
}

// Function to access global creators structure. In order that it works in application start time and prevents the
// "static initialization fiasco", it follows the "construct on first use" idiom.
// More can be found in https://isocpp.org/wiki/faq/ctors#static-init-order
AppCreators& GetAppCreators()
{
  static AppCreators* c = new AppCreators();
  return *c;
}

